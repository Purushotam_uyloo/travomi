//
//  LandingPageViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/31/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "LandingPageViewController.h"

@interface LandingPageViewController ()
#import "2014LandingPageViewController.h"



#define KAUTHURL @"https://api.instagram.com/oauth/authorize/"
#define kAPIURl @"https://api.instagram.com/v1/users/"
#define KCLIENTID @"2d63aabea6fb44c587d283d515e1e106"
#define kREDIRECTURI @"jaago://"



@end


@implementation LandingPageViewController
{
    NSMutableArray *images;
    NSString *picture;
}


#pragma mark NSURLConnection Delegate Methods


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"in landing page now");
    NSLog(@"user: %@", [PFUser currentUser]);
    
}

#pragma mark NSURLConenction Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"Didthis");
    _responseData = [[NSMutableData alloc] init];
}

- (void) connection:(NSURLConnection *) connection didReceiveData:(NSData *)data
{
    NSLog(@"Didthis2");
    [_responseData appendData:data];
}
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    NSLog(@"Didthis3");
    return nil;
}



-(void) connectionDidFinishLoading:(NSURLConnection *)connection {
    images = [[NSMutableArray alloc] init];
    
    NSLog(@"Succeeded! Received %d bytes of data",[_responseData length]);
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:_responseData options:kNilOptions error:&error];
    NSArray *firstPic = [[[json objectForKey:@"data"]valueForKey:@"images"]valueForKey:@"low_resolution"];
    NSLog(@"/n/n String = %@", firstPic);
    
    int count = 0;
    
    for (NSDictionary *testDict in firstPic) {
        picture = [testDict objectForKey:@"url"];
        NSLog(@" %@",picture);
        NSLog(@"%i, objects: %i",count, [images count]);
        
        [images addObject:picture];
        
        count++;
        
    }
    NSURL *url;
    int ndx;
    
    NSLog(@"%i",[images count]);
    
    for (ndx = 0; ndx < 2; ndx++ ) {
        
        NSLog(@"hello");
        url = [NSURL URLWithString:[images objectAtIndex:ndx]];
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        UIImage *img = [[UIImage alloc] initWithData:imageData];
        [image setImage:img];
    }
    
    _responseData = nil;
    NSLog(@"hurray");
    
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    _responseData = nil;
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
