//
//  HotelPickerViewController.h
//  Jaago
//
//  Created by WangYinghao on 3/2/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelPickerViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UISegmentedControl *top;
@property (strong, nonatomic) IBOutlet UIButton *below;
@property (strong, nonatomic) IBOutlet UIView *settingsView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *settingsButton;
@property (strong, nonatomic) IBOutlet UITableView *hotelTable;
@property UIImage* navigationImage;
@property NSString* islandName;
@property (strong, nonatomic) NSArray* hotelList;
@property NSString* type;
- (IBAction)homePressed:(id)sender;
- (IBAction)islandsPressed:(id)sender;
- (IBAction)cameraPressed:(id)sender;
- (IBAction)profilePressed:(id)sender;
- (IBAction)settingPressed:(id)sender;

@end
