//
//  ImageProperties.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/14/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "VideoModel.h"
#import "VineVideoModel.h"
//this is the header for all the properties needed for each image when being displayed.

@interface ImageProperties : NSObject

@property NSDictionary * googleSearchResults;
@property NSString* videoType;
//Vine video
@property VineVideoModel* vineVideo;
//Parse user video
@property BOOL isUserVideo;
@property PFFile *videoFile;
@property NSData *videoData;
//Youtube video model
@property VideoModel* videos;
@property BOOL isVideo;
// the url the image is coming from.
@property NSURL *url;
@property NSURL *videourl;

// if it is an instagram picture or a jaago hosted picture
@property BOOL instagramPic;
// what the image should will show.
@property UIImage *image;
@property NSString *caption;
@property NSString *user;
@property PFFile *imageFile;
//show loading image if not downloaded yet if downloaded show real image.
@property BOOL downloaded;
@property NSString *id;
@property Boolean travomiPost;
//parse user and photo data if not instagram image. ß
@property PFUser *pfUser;
@property NSString *photoID;

- (BOOL) isUserContent;

@property NSString *hotspottype;

//preset URLs
- (NSString *) expediaURL;
- (NSString *) travelocityURL;
- (NSString *) hotelscomURL;
- (NSString *) bookingcomURL;
- (NSString *) viatorURL;
- (NSString *) opentableURL;


@end
