/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLHotelEndpointCommonResponse.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   hotelEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLHotelEndpointCommonResponse (0 custom class methods, 6 custom properties)

#import "GTLHotelEndpointCommonResponse.h"

// ----------------------------------------------------------------------------
//
//   GTLHotelEndpointCommonResponse
//

@implementation GTLHotelEndpointCommonResponse
@dynamic errorId, errorMessage, identifier, sId, status, uid;

+ (NSDictionary *)propertyToJSONKeyMap {
  NSDictionary *map = @{
    @"identifier" : @"id"
  };
  return map;
}

@end
