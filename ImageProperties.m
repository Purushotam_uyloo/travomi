//
//  ImageProperties.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/14/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "ImageProperties.h"

@implementation ImageProperties

- (BOOL) isUserContent{
    return self.isUserVideo || self.photoID.length>0;
}


- (NSString *) islandName{
    //load all island lists
    NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"HawaiiTags" ofType:@"txt"];
    NSString *fileContents1 = [NSString stringWithContentsOfFile:filePath1
                                             encoding:NSUTF8StringEncoding
                                                error:nil];
    NSString *filePath2 = [[NSBundle mainBundle] pathForResource:@"KauaiTags" ofType:@"txt"];
    NSString *fileContents2 = [NSString stringWithContentsOfFile:filePath2
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];
    NSString *filePath3 = [[NSBundle mainBundle] pathForResource:@"MauiTags" ofType:@"txt"];
    NSString *fileContents3 = [NSString stringWithContentsOfFile:filePath3
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];
    if ([fileContents1 containsString:[self.caption substringFromIndex:1]]) {
        return @"Hawaii";
    } else if ([fileContents2 containsString:[self.caption substringFromIndex:1]]){
        return @"Kauai";
    } else if ([fileContents3 containsString:[self.caption substringFromIndex:1]]){
        return @"Maui";
    } else{
        return @"Oahu";
    }
    
}

- (NSString *) expediaURL{
    if ([[self islandName] isEqualToString:@"Hawaii"]){
        return @"https://www.expedia.com/Destinations-In-Hawaii.d213.Hotel-Destinations";
    } else if([[self islandName] isEqualToString:@"Kauai"]){
        return @"https://www.expedia.com/Kauai-Island-Hotels.d180075.Travel-Guide-Hotels";
    } else if ([[self islandName] isEqualToString:@"Maui"]){
        return @"https://www.expedia.com/Maui-Island-Hotels.d180073.Travel-Guide-Hotels";
    } else {
        return @"https://www.expedia.com/Oahu-Island-Hotels.d180077.Travel-Guide-Hotels";
    }
}

- (NSString *) travelocityURL{
    if ([[self islandName] isEqualToString:@"Hawaii"]){
        return @"https://www.travelocity.com/Hawaii-Hotels.d180074.Travel-Guide-Hotels";
    } else if([[self islandName] isEqualToString:@"Kauai"]){
        return @"https://www.travelocity.com/Kauai-Island-Hotels.d180075.Travel-Guide-Hotels";
    } else if ([[self islandName] isEqualToString:@"Maui"]){
        return @"https://www.travelocity.com/Maui-Island-Hotels.d180073.Travel-Guide-Hotels";
    } else {
        return @"https://www.travelocity.com/Oahu-Island-Hotels.d180077.Travel-Guide-Hotels";
    }
}
- (NSString *) hotelscomURL{
    if ([[self islandName] isEqualToString:@"Hawaii"]){
        return @"http://www.hotels.com/sa12/hotels-in-hawaii-united-states-of-america/";
    } else if([[self islandName] isEqualToString:@"Kauai"]){
        return @"http://www.hotels.com/de1633052/hotels-kauai-island-hawaii/";
    } else if ([[self islandName] isEqualToString:@"Maui"]){
        return @"http://www.hotels.com/de1475774/hotels-maui-island-hawaii/";
    } else {
        return @"http://www.hotels.com/de1633050/hotels-oahu-island-hawaii/";
    }
}
- (NSString *) bookingcomURL{
    if ([[self islandName] isEqualToString:@"Hawaii"]){
        return @"http://www.booking.com/region/us/hawaii.html";
    } else if([[self islandName] isEqualToString:@"Kauai"]){
        return @"http://www.booking.com/region/us/kauai-hawaii.html";
    } else if ([[self islandName] isEqualToString:@"Maui"]){
        return @"http://www.booking.com/region/us/maui-hawaii.html";
    } else {
        return @"http://www.booking.com/region/us/oahu-hawaii.html";
    }
}

- (NSString *) viatorURL{
    if ([[self islandName] isEqualToString:@"Hawaii"]){
        return @"http://www.viator.com/Hawaii/d278-ttd";
    } else if([[self islandName] isEqualToString:@"Kauai"]){
        return @"http://www.viator.com/Kauai/d670-ttd";
    } else if ([[self islandName] isEqualToString:@"Maui"]){
        return @"http://www.viator.com/Maui/d671-ttd";
    } else {
        return @"http://www.viator.com/Oahu/d672-ttd";
    }
}

- (NSString *) opentableURL{
    if ([[self islandName] isEqualToString:@"Hawaii"]){
        return @"http://www.opentable.com/best-hawaii-big-island-restaurants?topic=Best%20Overall";
    } else if([[self islandName] isEqualToString:@"Kauai"]){
        return @"http://www.opentable.com/best-kauai-restaurants?topic=Best%20Overall";
    } else if ([[self islandName] isEqualToString:@"Maui"]){
        return @"http://www.opentable.com/best-maui-restaurants?topic=Best%20Overall";
    } else {
        return @"http://www.opentable.com/best-oahu-restaurants?topic=Best%20Overall";
    }
}


@end
