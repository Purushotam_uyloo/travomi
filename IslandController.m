//
//  IslandController.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/5/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "IslandController.h"
#import "islandPickerCell.h"
#import "HotelPickerViewController.h"
#import <Parse/Parse.h>

#import "RootNavigationController.h"
#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
#import "HomePageViewController.h"
#import "GTLServiceHotelEndpoint.h"
#import "GTLQueryHotelEndpoint.h"
#import "GTMHTTPFetcher.h"
#import "GTLService.h"
#import "GTLHotelEndpointHotelCollection.h"
#import "GTLHotelEndpointHotel.h"
#import "GTLHotelEndpointImage.h"
#import "HotelDetailViewController.h"
#import "FeedsViewController.h"
#import "UIImageView+WebCache.h"


@interface IslandController ()
{
    NSInteger tagValue;
    __weak IBOutlet UIView *introductionView;
}
@end

NSMutableDictionary *dict;

@implementation IslandController
@synthesize islandTable;
static GTLServiceHotelEndpoint *service = nil;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    HotelArray = [[NSMutableArray alloc]init];
    
    
    
    if (!service) {
        service = [[GTLServiceHotelEndpoint alloc] init];
        service.retryEnabled = YES;
        
    }
    
    
    RootNavigationController * rootNVC = (RootNavigationController *)self.navigationController;
    
    UITabBar *tabBar = self.tabBarController.tabBar;
    [self.tabBarController.tabBar setBarTintColor: [UIColor blackColor]];
    
    
    
    UITabBarItem *item0 = [tabBar.items objectAtIndex:0];
    UITabBarItem *item2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *item3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *item4 = [tabBar.items objectAtIndex:3];
    
    
    UIImage* unselectedImage0 = [[UIImage imageNamed:@"home_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage0 = [[UIImage imageNamed:@"home_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

    UIImage* unselectedImage2 = [[UIImage imageNamed:@"camera_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage2 = [[UIImage imageNamed:@"camera_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* unselectedImage3 = [[UIImage imageNamed:@"user_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage3 = [[UIImage imageNamed:@"user_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* unselectedImage4 = [[UIImage imageNamed:@"settings_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage4 = [[UIImage imageNamed:@"settings_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item0.selectedImage = selectedImage0;
    item0.image = unselectedImage0;
    item0.imageInsets = UIEdgeInsetsMake(6, -3, -6, 3);
    item0.title = @"";

    item2.selectedImage = selectedImage2;
    item2.image = unselectedImage2;
    item2.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    item2.title = @"";
    
    
    item3.selectedImage = selectedImage3;
    item3.image = unselectedImage3;
    item3.imageInsets = UIEdgeInsetsMake(6, 8, -6, -8);
    item3.title = @"";
    
    
    item4.selectedImage = selectedImage4;
    item4.image = unselectedImage4;
    item4.imageInsets = UIEdgeInsetsMake(6, 3, -6, -3);
    item4.title = @"";


    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Montserrat-Regular" size:15.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = size;

    
    introductionView.hidden = true;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"showPage"]) {
        introductionView.hidden = false;
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(openscratchpad:)
                                                 name:@"scratchpad" object:nil];
   
    
}

- (void)openscratchpad:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification object];
    
    NSLog(@"%@",info);
    
    [self performSelector:@selector(PushHoteldetail:) withObject:[info valueForKey:@"name"] afterDelay:0.7];
 
    

    
    
}

-(void)PushHoteldetail:(NSString *)HotelName
{
    for(int i=0;i < [HotelArray count];i++)
    {
        GTLHotelEndpointHotel * wrapper = (GTLHotelEndpointHotel *)[HotelArray objectAtIndex:i];
        
        if([wrapper.name isEqualToString:HotelName])
        {
            HotelDetailViewController * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"detail"];
            page.DataArray = [HotelArray objectAtIndex: i];
            
            NSMutableArray *FeedsArray = [[NSMutableArray alloc]init];


            NSArray * Images = (NSArray *)wrapper.gallery;

            
            for(int i=0 ; i < Images.count ; i++)
            {
                GTLHotelEndpointImage * wrapper = (GTLHotelEndpointImage *)[Images objectAtIndex:i];
                
                ImageProperties * prop =[[ImageProperties alloc]init];
                prop.url = [NSURL URLWithString:wrapper.sURL];
                prop.id = prop.id;
                prop.isVideo = false;
                [FeedsArray addObject:prop];
                
            }

            page.ImagesArray = [[NSMutableArray alloc]initWithArray:FeedsArray];
            
            
            [self.navigationController pushViewController:page animated:YES];
        }
        
        
        
    }
}


-(void)viewWillAppear:(BOOL)animated
{
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"hotel"];
    NSMutableArray *HotelArr = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    
    [HotelArray removeAllObjects];
    if ([HotelArr count]>0)
    {
        for(int i=0 ; i < [HotelArr count];i++)
        {
            GTLHotelEndpointHotel * wrapper = (GTLHotelEndpointHotel *)[HotelArr objectAtIndex:i];
            [HotelArray addObject:wrapper];

        }
        
    }
    [self.islandTable reloadData];
    [self CallAPI];
    
}

-(void)CallAPI
{
    if([HotelArray count]==0)
    {
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    }
    

    GTLQueryHotelEndpoint *query = [GTLQueryHotelEndpoint queryForGetHotelList];
    [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLHotelEndpointHotelCollection *object, NSError *error) {
        
        if(error == nil)
        {
       
            NSArray *items = [object items];
            NSMutableArray * tempArr =[[NSMutableArray alloc]init];
            
            [tempArr addObjectsFromArray:items];
            if([HotelArray count]==0)
            {
                [HotelArray addObjectsFromArray:items];
                [islandTable reloadData];
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                
                
            }
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:tempArr];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hotel"];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"hotel"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            
        }
        else
        {
            if([HotelArray count]==0)
            {
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                [self CallAPI];
            }
            

        }
        
        // Do something with items.
    }];
    
    
}


-(IBAction)btnCheckboxAction:(id)sender{
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 0) {
        [btn setImage:[UIImage imageNamed:@"checked_checkbox.png"] forState:UIControlStateNormal];
        btn.tag = 1;
        tagValue = 1;
    }
    else{
        [btn setImage:[UIImage imageNamed:@"unchecked_checkbox.png"] forState:UIControlStateNormal];
        btn.tag = 0;
        tagValue = 0;
    }
}

-(IBAction)btnOkAction:(id)sender{
    if (tagValue == 1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:true forKey:@"showPage"];
        [defaults synchronize];
    }
    [introductionView setHidden:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [HotelArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    islandPickerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"islandCell" forIndexPath:indexPath];
    
    GTLHotelEndpointHotel * wrapper = (GTLHotelEndpointHotel *)[HotelArray objectAtIndex:indexPath.row];
    
    NSArray * Images = (NSArray *)wrapper.gallery;
    
    GTLHotelEndpointImage * ImageWrapper = (GTLHotelEndpointImage *)[Images objectAtIndex:0];
    NSString *image1 =[NSString stringWithFormat:@"%@",ImageWrapper.name];
    
    if (![image1 isEqualToString:@""])
    {
        [cell.mainImage sd_setImageWithURL:[NSURL URLWithString:ImageWrapper.sURL]
                        placeholderImage:[UIImage imageNamed:@"no_img_sq.png"]];

    }
    else
    {
        UIImage *imgno = [UIImage imageNamed:@"no_img_sq.png"];
        
        [cell.mainImage setImage:imgno];
    }
    
    
    
    
    cell.textField.text = wrapper.address;
    cell.subTextField.text = wrapper.name;
    cell.shadowImage.image = [UIImage imageNamed:@"pic_bottom_shade.png"];

    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
     GTLHotelEndpointHotel * wrapper = (GTLHotelEndpointHotel *)[HotelArray objectAtIndex:indexPath.row];
    
    FeedsViewController * feedspage = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"feeds"];
    feedspage.HotelId = wrapper.identifier.stringValue;
    feedspage.DataArr = [HotelArray objectAtIndex: indexPath.row];
    feedspage.title = wrapper.name;
    [self.navigationController pushViewController:feedspage animated:YES];

    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *marr = [[NSMutableArray alloc]init];
     [marr addObject:wrapper.name];
    [marr addObjectsFromArray:[defaults objectForKey:@"savedHotels"]];
    
    //adding extra
    
    NSArray *distintStrings = [marr valueForKeyPath:@"@distinctUnionOfObjects.self"];
    [defaults setObject:distintStrings forKey:@"savedHotels"];
    [defaults synchronize];
    
    
//    GTLHotelEndpointHotel * wrapper = (GTLHotelEndpointHotel *)[HotelArray objectAtIndex:indexPath.row];
//    [marr addObject:wrapper.name];
//    
//    NSArray *distintStrings = [marr valueForKeyPath:@"@distinctUnionOfObjects.self"];
//    [defaults setObject:distintStrings forKey:@"savedHotels"];
//    [defaults synchronize];
//
//    HotelDetailViewController * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"detail"];
//    page.DataArray = [HotelArray objectAtIndex: indexPath.row];
//    [self.navigationController pushViewController:page animated:YES];
    
    
}


- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}



#pragma mark Segues

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  //    NSIndexPath *indexPath = self.islandTable.indexPathForSelectedRow;
//    HomePageViewController *controller = [segue destinationViewController];
//    controller.selectedRow = [NSString stringWithFormat:@"%ld",indexPath.row];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSMutableArray *marr = [[NSMutableArray alloc]init];
//    [marr addObjectsFromArray:[defaults objectForKey:@"savedHotels"]];
//
//    if (indexPath.row == 0) {
//        [marr addObject:@"Dolphin Cove Motel"];
//    }
//    else{
//        [marr addObject:@"Courtesy Inn"];
//    }
//    NSArray *distintStrings = [marr valueForKeyPath:@"@distinctUnionOfObjects.self"];
//
//    [defaults setObject:distintStrings forKey:@"savedHotels"];
//    [defaults synchronize];
    
    /*
    NSLog(@"DOESNT GET IN HERE");
    NSCharacterSet *newlineCharSet = [NSCharacterSet newlineCharacterSet];
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"KauaiTags" ofType:@"txt"];
    NSString* fileContents = [NSString stringWithContentsOfFile:filePath
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    if(indexPath.row == 0) {
        NSArray *kauaiPicked = [[NSArray alloc] init];
        filePath = [[NSBundle mainBundle] pathForResource:@"KauaiTags" ofType:@"txt"];
        fileContents = [NSString stringWithContentsOfFile:filePath
                                                           encoding:NSUTF8StringEncoding
                                                              error:nil];
        kauaiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
        controller.islandName = @"Kauai";
        controller.hotelList = kauaiPicked;
        controller.type = @"hotel";


    }
    else if (indexPath.row == 1){
        NSArray *oahuPicked = [[NSArray alloc] init];
        filePath = [[NSBundle mainBundle] pathForResource:@"OahuTags" ofType:@"txt"];
        fileContents = [NSString stringWithContentsOfFile:filePath
                                                 encoding:NSUTF8StringEncoding
                                                    error:nil];
        oahuPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
        controller.islandName = @"Oahu";
        controller.hotelList = oahuPicked;
        controller.type = @"hotel";

    }
    else if (indexPath.row == 2){
        NSArray *mauiPicked = [[NSArray alloc] init];
        filePath = [[NSBundle mainBundle] pathForResource:@"MauiTags" ofType:@"txt"];
        fileContents = [NSString stringWithContentsOfFile:filePath
                                                 encoding:NSUTF8StringEncoding
                                                    error:nil];
        mauiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
        controller.islandName = @"Maui";
        controller.hotelList = mauiPicked;
        controller.type = @"hotel";

        
    }
    else if (indexPath.row == 3){
        NSArray *hawaiiPicked = [[NSArray alloc] init];
        filePath = [[NSBundle mainBundle] pathForResource:@"HawaiiTags" ofType:@"txt"];
        fileContents = [NSString stringWithContentsOfFile:filePath
                                                 encoding:NSUTF8StringEncoding
                                                    error:nil];
        hawaiiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
        controller.islandName = @"Hawaii";
        controller.hotelList = hawaiiPicked;
        controller.type = @"hotel";

        
    }
    NSLog(@"Here, %@", controller.islandName);
    NSLog(@" %@", controller.hotelList);
     */
    
    


    
    
    HotelDetailViewController * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"detail"];
    [self.navigationController pushViewController:page animated:NO];

}
@end
