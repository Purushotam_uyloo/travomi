/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLHotelEndpointReportImage.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   hotelEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLHotelEndpointReportImage (0 custom class methods, 4 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLObject.h"
#else
  #import "GTLObject.h"
#endif

// ----------------------------------------------------------------------------
//
//   GTLHotelEndpointReportImage
//

@interface GTLHotelEndpointReportImage : GTLObject
@property (nonatomic, copy) NSString *comment;

// identifier property maps to 'id' in JSON (to avoid Objective C's 'id').
@property (nonatomic, retain) NSNumber *identifier;  // longLongValue

@property (nonatomic, retain) NSNumber *imageID;  // longLongValue
@property (nonatomic, retain) NSNumber *userId;  // longLongValue
@end
