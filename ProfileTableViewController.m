//
//  ProfileTableViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/5/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//


// view controller for the users profile

#import "ProfileTableViewController.h"
#import "2014TableViewCell.h"
#import "ProfileResultViewController.h"
#import "ImageProperties.h"



#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
#import "CommentsController.h"

@interface ProfileTableViewController ()
{
    NSMutableArray *images;
    NSMutableArray *imageCaptions;
    NSMutableArray *photoObjects;
    NSMutableArray *photoIds;
}
@end

@implementation ProfileTableViewController

@synthesize tableView;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];

    [super viewDidLoad];
    
    images = [[NSMutableArray alloc]init];
    imageCaptions = [[NSMutableArray alloc] init];
    photoIds = [[NSMutableArray alloc]init];
    
    
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    
    NSLog(@"currentUser = %@", [PFUser currentUser]);
    [photoObjectQuery whereKey:@"creator" equalTo:[PFUser currentUser]];
    
    [photoObjectQuery whereKey:@"active" equalTo:@1];
    
    [photoObjectQuery orderByDescending:@"createdAt"];
    
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            for(PFObject *hello in objects){
                NSLog(@"hello: %@", hello );
                
                ImageProperties *property = [[ImageProperties alloc]init];
                property.id = [hello objectId];
                NSLog(@"property.id: %@", property.id);
                [photoIds addObject:property];

            }
            [tableView reloadData];
            
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 157;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([photoIds count] %2 == 1) {
        return [photoIds count]/2 + 1;

    }
    else if ([photoIds count] %2 == 0) {
        NSLog(@"even count");

        return [photoIds count]/2;
    }
    else {
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ImageViewCell";
    ImageViewCell4 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[ImageViewCell4 alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // math to have 2 images per row.
    ImageProperties *cellProperties1;
    ImageProperties *cellProperties2;

    
    if ([photoIds count] > indexPath.row*2) {
        cellProperties1 = ((ImageProperties * )photoIds[indexPath.row*2]);
    }
    else {
        cellProperties1 = nil;
    }
        
    if ([photoIds count] > (indexPath.row*2+1)) {
        cellProperties2 = ((ImageProperties * )photoIds[indexPath.row*2+1]);
    }
    else {
        cellProperties2 = nil;

    }


    if (cellProperties1.image) {
        cell.pictureView1.image = cellProperties1.image;
    }
    else {
        if (cellProperties1) {
            cell.pictureView1.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                [self downloadImageWithObject:(&cellProperties1) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                    if (succeeded) {
                        NSLog(@"lets do it");
                        cell.pictureView1.image = image.image;
                        cellProperties1.user = image.user;
                        cellProperties1.caption = image.caption;
                        cellProperties1.image = image.image;
                        cellProperties1.pfUser = image.pfUser;
                        NSLog(@"Regurgitate %@, %@", cellProperties1.user, cellProperties1.caption);
                    }
                    
                    
                }];
        }
        
    }
    if (cellProperties2.image) {
        cell.pictureView2.image = cellProperties2.image;
    }
    else {
        if (cellProperties2) {

            cell.pictureView2.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
            [self downloadImageWithObject:(&cellProperties2) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                if (succeeded) {
                    NSLog(@"lets do it");
                    cell.pictureView2.image = image.image;
                    cellProperties2.user = image.user;
                    cellProperties2.caption = image.caption;
                    cellProperties2.image = image.image;
                    cellProperties2.pfUser = image.pfUser;
                    NSLog(@"Regurgitate %@, %@", cellProperties2.user, cellProperties2.caption);
                }
                
                
            }];
        }
        
        
    }

    return cell;
}


- (void) downloadImageWithObject:(ImageProperties **)id completionBlock:(void (^)(BOOL succeeded, ImageProperties *image)) completionBlock
{
    NSLog(@"ran download function");
    NSLog(@"%@",(*id).id);
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    [photoObjectQuery includeKey:@"creator"];
    [photoObjectQuery getObjectInBackgroundWithId:(*id).id block:^(PFObject *object, NSError *error) {
        if (!error) {
            ImageProperties *imageSpecs = [[ImageProperties alloc] init];
            NSLog(@"user %@", object [@"creator"][@"username"]);
            imageSpecs.user = object[@"creator"][@"username"];
            imageSpecs.pfUser = object[@"creator"];
            imageSpecs.caption = object[@"caption"];
            imageSpecs.imageFile = object[@"image"];
            
            
            [object[@"image"] getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    NSLog(@"Noerrors");
                    imageSpecs.image = [UIImage imageWithData:imageData];
                    
                    completionBlock (YES,imageSpecs);
                    
                }
                else {
                    NSLog(@"Error downloading image");
                    completionBlock (NO,nil);
                    
                    
                }
            }];
            
        }
        else {
            NSLog(@"Error first query");
            completionBlock (NO,nil);
            
            
        }
        
    }];
    
    
}

- (IBAction)unwindAfterDelete:(UIStoryboardSegue *)unwindSegue
{
    ProfileResultViewController *controller = unwindSegue.sourceViewController;
    NSInteger path = controller.number;
    [photoIds removeObjectAtIndex:path];
    [tableView reloadData];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqual:@"profileresult"]){
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        ProfileResultViewController *controller = [segue destinationViewController];
        NSInteger path = tableView.indexPathForSelectedRow.row;
        controller.number = path;
        controller.selectedImageProperties = [photoIds objectAtIndex:path];
    }else if ([segue.identifier isEqualToString:@"left"]){
        CommentsController *comment = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *hitIndex = [self.tableView indexPathForRowAtPoint:hitPoint];
        NSLog(@"hitIndex.row*2: %ld", hitIndex.row*2);
        comment.property = [photoIds objectAtIndex:hitIndex.row*2];
        
        NSLog(@"hello");
    }else if ([segue.identifier isEqualToString:@"right"]) {
        CommentsController *comment = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.tableView];
        NSIndexPath *hitIndex = [self.tableView indexPathForRowAtPoint:hitPoint];
        NSLog(@"hitIndex.row*2+1: %ld", hitIndex.row*2+1);

        comment.property = [photoIds objectAtIndex:hitIndex.row*2+1];
    }
    
}


@end
