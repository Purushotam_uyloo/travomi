//
//  VineVideoModel.h
//  Jaago
//
//  Created by WangYinghao on 3/25/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VineVideoModel : NSObject
@property (strong,nonatomic) NSString *title;
@property (strong, nonatomic) NSURL *videoThumb;
@property (strong, nonatomic) NSURL *videoURL;

@end
