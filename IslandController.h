//
//  IslandController.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/5/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface IslandController : UIViewController<UITableViewDataSource, UITableViewDelegate,MBProgressHUDDelegate> {
    NSString *island;
    NSMutableArray * HotelArray;
    MBProgressHUD * HUD;
    
}
@property (weak, nonatomic) IBOutlet UITableView *islandTable;


@end
