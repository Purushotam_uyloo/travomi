//
//  RecycleBinTableViewController.m
//  Jaago
//
//  Created by WangYinghao on 2/24/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "RecycleBinTableViewController.h"
#import "ProfileTableViewController.h"
#import "2014TableViewCell.h"
#import "RecycleBinResultViewController.h"
#import "ImageProperties.h"

@interface RecycleBinTableViewController ()
{
    NSMutableArray *images;
    NSMutableArray *imageCaptions;
    NSMutableArray *photoObjects;
    NSMutableArray *photoIds;
}

@end

@implementation RecycleBinTableViewController

@synthesize tableView;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    
    [super viewDidLoad];
    
    images = [[NSMutableArray alloc]init];
    imageCaptions = [[NSMutableArray alloc] init];
    photoIds = [[NSMutableArray alloc]init];
    
    
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    
    NSLog(@"currentUser = %@", [PFUser currentUser]);
    [photoObjectQuery whereKey:@"creator" equalTo:[PFUser currentUser]];
    
    [photoObjectQuery whereKey:@"active" equalTo:@0];
    
    [photoObjectQuery orderByDescending:@"createdAt"];
    
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            for(PFObject *hello in objects){
                
                
                ImageProperties *property = [[ImageProperties alloc]init];
                property.id = [hello objectId];
                [photoIds addObject:property];
                
            }
            [tableView reloadData];
            
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [photoIds count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"1234567890";
    _014TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[_014TableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    ImageProperties *cellProperties = ((ImageProperties *)photoIds[indexPath.row]);
    if (cellProperties.image) {
        cell.pictureView.image = cellProperties.image;
    }
    else {
        cell.pictureView.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
        [self downloadImageWithObject:(&cellProperties) completionBlock:^(BOOL succeeded, ImageProperties *image) {
            if (succeeded) {
                NSLog(@"lets do it");
                cell.pictureView.image = image.image;
                cellProperties.user = image.user;
                cellProperties.caption = image.caption;
                cellProperties.image = image.image;
                cellProperties.pfUser = image.pfUser;
                NSLog(@"Regurgitate %@, %@", cellProperties.user, cellProperties.caption);
            }
            
            
        }];
        
        
    }
    return cell;
}


- (void) downloadImageWithObject:(ImageProperties **)id completionBlock:(void (^)(BOOL succeeded, ImageProperties *image)) completionBlock
{
    NSLog(@"ran download function");
    NSLog(@"%@",(*id).id);
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    [photoObjectQuery includeKey:@"creator"];
    [photoObjectQuery getObjectInBackgroundWithId:(*id).id block:^(PFObject *object, NSError *error) {
        if (!error) {
            ImageProperties *imageSpecs = [[ImageProperties alloc] init];
            NSLog(@"user %@", object [@"creator"][@"username"]);
            imageSpecs.user = object[@"creator"][@"username"];
            imageSpecs.pfUser = object[@"creator"];
            imageSpecs.caption = object[@"caption"];
            imageSpecs.imageFile = object[@"image"];
            
            
            [object[@"image"] getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    NSLog(@"Noerrors");
                    imageSpecs.image = [UIImage imageWithData:imageData];
                    
                    completionBlock (YES,imageSpecs);
                    
                }
                else {
                    NSLog(@"Error downloading image");
                    completionBlock (NO,nil);
                    
                    
                }
            }];
            
        }
        else {
            NSLog(@"Error first query");
            completionBlock (NO,nil);
            
            
        }
        
    }];
    
    
}

- (IBAction)unwindAfterDelete:(UIStoryboardSegue *)unwindSegue
{
    RecycleBinResultViewController *controller = unwindSegue.sourceViewController;
    NSInteger path = controller.number;
    [photoIds removeObjectAtIndex:path];
    [tableView reloadData];
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqual:@"recyclebinresult"]){
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        RecycleBinResultViewController *controller = [segue destinationViewController];
        NSInteger path = tableView.indexPathForSelectedRow.row;
        controller.number = path;
        controller.selectedImageProperties = [photoIds objectAtIndex:path];
    }else{
        
    }
    
}


@end
