//
//  CommentViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/29/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "CommentViewController.h"
#import <Parse/Parse.h>
#import "CommentViewCell.h"

#define COMMENT @"comment"
#define LIKE @"like"
#define FOLLOW @"follow"

@interface CommentViewController ()
{
    NSMutableArray *commentArray;
}
@end

@implementation CommentViewController
@synthesize photoID, photoProperties, addCommentView, commentTable, scrollView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.commentTable.allowsMultipleSelectionDuringEditing = NO;

    NSLog(@"in viewDidLoad");
    commentArray = [[NSMutableArray alloc]init];

    if ([commentArray count] == 0) {
        NSLog(@"Beforefunction");
        PFQuery *tableQuery = [self queryForTable];
        NSLog(@"before query");
        
        [tableQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (PFObject *object in objects) {
                    
                    [commentArray addObject:object];
                }
                [commentTable reloadData];
            }
            else {
                NSLog(@"Ran into an error");
            }
        }];

    }
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


-(void)dismissKeyboard
{
    [addCommentView resignFirstResponder];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.scrollView setContentOffset:CGPointMake(0, kbSize.height) animated:YES];
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (IBAction)textViewDidBeginEditing:(UITextView *)sender {
    
    [sender setText:@""];
    
    sender.delegate = self;
    
}

// set all of the comments into a tableview to organize them all

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        PFObject *object = [commentArray objectAtIndex:indexPath.row];
        
        [object deleteInBackground];
        
        [commentArray removeObjectAtIndex:indexPath.row];
        [commentTable reloadData];
    }
}


- (IBAction)doneEditing:(UITextView *)sender {
    [addCommentView resignFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [commentArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CommentCell";
    CommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[CommentViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:   CellIdentifier];
    }

    PFObject *object = [commentArray objectAtIndex:indexPath.row];
    
    cell.comment.text = object[@"comment"];

    //NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    //[dateFormater setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssz"];
    //NSString *date = [dateFormater stringFromDate:object.createdAt];
    //cell.date.text = date;
    
    //NSLog(@"cell.date %@", cell.date);
    return cell;
}
- (PFQuery *) queryForTable {
	PFQuery *tableQuery = [PFQuery queryWithClassName:@"Notification"];

	[tableQuery whereKey:@"notificationType" equalTo:COMMENT];
	[tableQuery whereKey:@"photoID" equalTo:photoProperties.id];

	//[tableQuery includeKey:@"creator"];
	[tableQuery orderByAscending:@"createdAt"];
    NSLog(@"End of function");
	return tableQuery;
}
- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]){
        
        NSString *trimmedComment = [addCommentView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if (trimmedComment.length != 0) {
            // Create the comment activity object
            PFObject *comment = [PFObject objectWithClassName:@"Notification"];
            [comment setValue:trimmedComment forKey:@"comment"]; // Set comment text
            [comment setValue:[PFUser currentUser] forKey:@"creator"];
            [comment setValue:photoProperties.pfUser forKey:@"recipient"];
            [comment setValue:COMMENT forKey:@"notificationType"];
            [comment setValue:photoProperties.id forKey:@"photoID"];
            
            
            PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
            [ACL setPublicReadAccess:YES];
            comment.ACL = ACL;
            
            [comment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    [commentArray addObject:comment];
                    [commentTable reloadData];
                }
                else {
                    NSLog(@"Error in submitting the comment");
                }
            }];
            
        }
        [textView setText:@""];

        [textView resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}

- (BOOL)textViewShouldReturn:(UITextView *)textField {
    // Trim the comment text
    NSString *trimmedComment = [addCommentView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (trimmedComment.length != 0) {
        // Create the comment activity object
        PFObject *comment = [PFObject objectWithClassName:@"Notification"];
        [comment setValue:trimmedComment forKey:@"comment"]; // Set comment text
        [comment setValue:[PFUser currentUser] forKey:@"creator"];
        [comment setValue:photoProperties.pfUser forKey:@"recipient"];
        [comment setValue:COMMENT forKey:@"notificationType"];
        [comment setValue:photoProperties.id forKey:@"photoID"];
        
        
        PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
        [ACL setPublicReadAccess:YES];
        comment.ACL = ACL;

        [comment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [commentArray addObject:comment];
                [commentTable reloadData];
            }
            else {
                NSLog(@"Error in submitting the comment");
            }
        }];

    }
    [textField setText:@""];
    return [textField resignFirstResponder];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


// this acttion deletes the comment and updates the table again.
- (IBAction)deleteButtonPressed:(UIButton *)sender {
}
@end
