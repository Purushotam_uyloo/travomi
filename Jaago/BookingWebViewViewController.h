//
//  BookingWebViewViewController.h
//  Travomi
//
//  Created by WangYinghao on 8/24/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingWebViewViewController : UIViewController<UIWebViewDelegate>

@property NSString * aURLStringToOpen;
@property NSString * bookingEngineName;
@property NSString * searchKeyWord;

@end
