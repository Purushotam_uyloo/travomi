//
//  PickIslandController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/19/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "PickIslandController.h"
#import "HomePageViewController.h"
#import "HotelPickerViewController.h"

@interface PickIslandController ()

@end

@implementation PickIslandController
{
    NSArray *mauiPicked;
    NSArray *hawaiiPicked;
    NSArray *oahuPicked;
    NSArray *kauaiPicked;
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_bar.png"] forBarMetrics:UIBarMetricsDefault];
    //[self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:252.0/255.0 green:103.0/255.0 blue:61.0/255.0 alpha:1.0/255.0]] ;
    PFUser *test = [PFUser currentUser];
    if (!test) { // No user logged in
        // Create the log in view controller
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        [logInViewController setDelegate:self]; // Set ourselves as the delegate
        [logInViewController.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hlogo.png"]]];
        [logInViewController setFields:PFLogInFieldsUsernameAndPassword| PFLogInFieldsSignUpButton| PFLogInFieldsPasswordForgotten|PFLogInFieldsLogInButton];
        
        // Create the sign up view controller
        PFSignUpViewController *signUp = [[PFSignUpViewController alloc] init];
        [signUp setFields: PFSignUpFieldsUsernameAndPassword | PFSignUpFieldsEmail |PFSignUpFieldsSignUpButton | PFSignUpFieldsDismissButton];
        [signUp.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hlogo.png"]]];
        [signUp setDelegate:self]; // Set ourselves as the delegate
        
        // Assign our sign up controller to be displayed from the login controller
        [logInViewController setSignUpController:signUp];
        
        // Present the log in view controller
        [self presentViewController:logInViewController animated:YES completion:NULL];
        
        
        
        
        //[self performSegueWithIdentifier:@"NormaltoLoginView" sender:self];
        
        
    }
    else {
        if(![[test objectForKey:@"emailVerified"] boolValue]){
            [test refresh];
            if(![[test objectForKey:@"emailVerified"] boolValue]){
                [PFUser logOut];
                [self viewDidAppear:NO];
                [[[UIAlertView alloc] initWithTitle:@"Please Verify Email" message:@"Check your email to Verify your Email Address before continuing" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show ];
            }
            
        }
        if (![[test objectForKey:@"firstTime"] boolValue] && [[test objectForKey:@"testing"]boolValue]) {
            NSLog(@"Nothing here");
            [self performSegueWithIdentifier:@"InitialSocialMedia" sender:self];

            
        }
        else {
            NSLog(@"DO NOTHING");
        }
        
        //[self dismiss:logInViewController];
    }
}

#pragma mark - PFLogInViewControllerDelegate

// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length && password.length) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.

- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    NSLog(@"User dismissed the logInViewController");
}


#pragma mark - PFSignUpViewControllerDelegate

// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    
    // loop through all of the submitted data
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        if (!field || !field.length) { // check completion
            informationComplete = NO;
            break;
        }
    }
    
    // Display an alert if a field wasn't completed
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    NSLog(@"User dismissed the signUpViewController");
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"gets here");
    mauiPicked = [[NSArray alloc] init];
    hawaiiPicked = [[NSArray alloc] init];
    oahuPicked = [[NSArray alloc] init];
    kauaiPicked = [[NSArray alloc] init];
    
    NSCharacterSet *newlineCharSet = [NSCharacterSet newlineCharacterSet];
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"MauiTags" ofType:@"txt"];
    NSString* fileContents = [NSString stringWithContentsOfFile:filePath
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    
    mauiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    filePath = [[NSBundle mainBundle] pathForResource:@"KauaiTags" ofType:@"txt"];
    fileContents = [NSString stringWithContentsOfFile:filePath
                                             encoding:NSUTF8StringEncoding
                                                error:nil];
    
    kauaiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    filePath = [[NSBundle mainBundle] pathForResource:@"OahuTags" ofType:@"txt"];
 
    
    
    oahuPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    filePath = [[NSBundle mainBundle] pathForResource:@"HawaiiTags" ofType:@"txt"];
    fileContents = [NSString stringWithContentsOfFile:filePath
                                             encoding:NSUTF8StringEncoding
                                                error:nil];
    
    hawaiiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    NSLog(@"mauiPicked %@",mauiPicked);
    NSLog(@"kauaiPicked %@",kauaiPicked);
    NSLog(@"oahuPicked %@",oahuPicked);
    NSLog(@"hawaiiPicked %@",hawaiiPicked);
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Hawaii"]) {
        HotelPickerViewController *controller = [segue destinationViewController];
        controller.navigationImage = [UIImage imageNamed:@"Hawaii.jpeg"];
        controller.islandName = @"Hawaii";
        controller.hotelList = hawaiiPicked;
        
        
    }
    if ([segue.identifier isEqualToString:@"Oahu"]) {
        HotelPickerViewController *controller = [segue destinationViewController];
        controller.navigationImage = [UIImage imageNamed:@"Oahu.jpg"];
        controller.islandName = @"Oahu";
        controller.hotelList = oahuPicked;
        
    }
    if ([segue.identifier isEqualToString:@"Maui"]) {
        HotelPickerViewController *controller = [segue destinationViewController];
        controller.navigationImage = [UIImage imageNamed:@"maui.jpg"];
        controller.islandName = @"Maui";
        controller.hotelList = mauiPicked;
    }
    if ([segue.identifier isEqualToString:@"Kauai"]) {
        HotelPickerViewController *controller = [segue destinationViewController];
        controller.navigationImage = [UIImage imageNamed:@"Kauai.jpg"];
        controller.islandName = @"Kauai";
        controller.hotelList = kauaiPicked;
        
    }
    
}


@end
