//
//  HotelDetailViewController.h
//  Travomi
//
//  Created by admin on 2/3/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhoto.h"
#import "MWPhotoBrowser.h"
#import <CoreLocation/CoreLocation.h>
#import "TTTAttributedLabel.h"
#import "MBProgressHUD.h"

@class _014AppDelegate;

@interface HotelDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MWPhotoBrowserDelegate,CLLocationManagerDelegate,TTTAttributedLabelDelegate,MBProgressHUDDelegate>
{
    IBOutlet UITableView * tableview;
    BOOL IsAboutMoreClicked;
    NSMutableArray * Photos;
    CAGradientLayer *grad;
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentloc;
    
    _014AppDelegate * _delegate;
    UIView *transparentview;
    NSMutableData *responseData;
    BOOL locationBOOL;
    NSMutableArray *tempArray;
    NSArray* videos;
    
    NSMutableArray * TwitterArray;
    MBProgressHUD * HUD;
    
}
@property(nonatomic) int selectedIndex;
@property(nonatomic,retain)NSArray * DataArray;
@property(nonatomic,retain)NSMutableArray * ImagesArray;


@end
