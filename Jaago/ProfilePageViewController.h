//
//  ProfilePageViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/4/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilePageViewController : UITableViewController

@property (strong,nonatomic) NSMutableArray *images;

@end
