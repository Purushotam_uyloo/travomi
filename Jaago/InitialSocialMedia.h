//
//  InitialSocialMedia.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 9/11/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InitialSocialMedia : UIViewController {


IBOutlet UISwitch *facebookSwitch;
IBOutlet UISwitch *twitterSwitch;
IBOutlet UISwitch *tumblrSwitch;

}
- (IBAction)switchFacebookSwitch:(id)sender;
- (IBAction)switchTwitterSwitch:(id)sender;
- (IBAction)switchTumblrSwitch:(id)sender;
- (IBAction)dismiss:(id)sender;


@end
