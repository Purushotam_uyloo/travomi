//
//  CustomSignupViewController.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/9/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "CustomSignupViewController.h"
#import "HomePageViewController.h"
#import "GTLQueryCustomerLogin.h"
#import "GTLServiceCustomerLogin.h"
#import "GTLCustomerLoginCommonResponse.h"


@interface CustomSignupViewController ()

@end

@implementation CustomSignupViewController

@synthesize userNameField, termsButton, passwordField, emailField, scrollView, checkBox, createAccountbutton;
@synthesize Datadict;
@synthesize IsFromFacebook;
static GTLServiceCustomerLogin *service = nil;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!service) {
        service = [[GTLServiceCustomerLogin alloc] init];
        service.retryEnabled = YES;
        
    }
    
    if(IsFromFacebook == true)
    {
        userNameField.text = [Datadict valueForKey:@"username"];
        emailField.text = [Datadict valueForKey:@"email"];
        
        
    }
    
    [createAccountbutton setEnabled:NO];
    // Do any additional setup after loading the view.


}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    termsButton.layer.borderWidth = .5f;
    termsButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, userNameField.frame.size.height - 1, userNameField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    
    CALayer *bottomBorder1 = [CALayer layer];
    bottomBorder1.frame = CGRectMake(0.0f, userNameField.frame.size.height - 1, userNameField.frame.size.width, 1.0f);
    bottomBorder1.backgroundColor = [UIColor lightGrayColor].CGColor;
    
    CALayer *bottomBorder2 = [CALayer layer];
    bottomBorder2.frame = CGRectMake(0.0f, userNameField.frame.size.height - 1, userNameField.frame.size.width, 1.0f);
    bottomBorder2.backgroundColor = [UIColor lightGrayColor].CGColor;
    
    [userNameField.layer addSublayer:bottomBorder];
    [passwordField.layer addSublayer:bottomBorder1];
    [emailField.layer addSublayer:bottomBorder2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)signupAction:(id)sender {
    NSLog(@"user: %@", userNameField.text);
    NSLog(@"password: %@", passwordField.text);
    NSLog(@"email: %@", emailField.text);
    
    
    
    if ([userNameField.text isEqualToString:@""] ||[passwordField.text isEqualToString:@""] ||[emailField.text isEqualToString:@""] ) {
        NSLog(@"do nothing!!!!");
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Signing Up" message:@"Please fill out all the fields" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
        [alertView show];
    }
    else if (checkBox.isSelected == false) {

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Signing Up" message:@"Please Select terms and condition" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
        [alertView show];
    }
    /*else if (![passwordField.text isEqualToString:repeatPasswordField.text] ) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Password Mismatch" message:@"Please have both password fields match and try again" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
        [alertView show];
    }*/
    else {

        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        HUD.delegate = self;
        [HUD show:YES];
        
        GTLQueryCustomerLogin *query = [GTLQueryCustomerLogin queryForSignUpWithCUsername:userNameField.text cEmail:emailField.text cLoginType:@"Travomi" cPassword:passwordField.text];
        
        [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLCustomerLoginCommonResponse *object, NSError *error) {
            
            if(error == nil)
            {
                
                GTLCustomerLoginCommonResponse * resp = object;
                
                if([resp.status.stringValue isEqualToString:@"0"])
                {
                    UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Travomi" message:resp.errorMessage delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                }
                else{
                    
                    UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Travomi" message:@"A verification link has been sent over your mail address.Please verify that link" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alert show];
                    
                    [self dismissViewControllerAnimated:YES completion:nil];

                }


                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                
                
            }
            else
            {
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
            }
            
            // Do something with items.
        }];
    }
    
    
    
}

-(void) textFieldDidBeginEditing:(UITextField *)textField {
    
    CGPoint scrollPoint;
    
    if (textField == userNameField) {
        scrollPoint = CGPointMake(0, textField.frame.origin.y + 100);

    }
    else if (textField == emailField){
        scrollPoint = CGPointMake(0, textField.frame.origin.y + 57);
        
    }
    else if (textField == passwordField){
        scrollPoint = CGPointMake(0, textField.frame.origin.y + 14);

    }
    
    /*else if (textField == repeatPasswordField){
        scrollPoint = CGPointMake(0, textField.frame.origin.y - 29);
    }*/
    else {
        scrollPoint = CGPointMake(0, textField.frame.origin.y);
    }

    
    [scrollView setContentOffset:scrollPoint animated:YES];


    
}

-(void) textFieldDidEndEditing:(UITextField *)textField {
//    _controllerView.frame = CGRectMake(_controllerView.frame.origin.x,
//                                       _controllerView.frame.origin.y,
//                                       _controllerView.frame.size.width,
//                                       _controllerView.frame.size.height + 215 - 50);
}

- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
    [self textFieldDidBeginEditing:nil];
    
}
- (IBAction)toTermsofUse:(id)sender {
    
}

- (IBAction)checkboxPressed:(id)sender {
    [checkBox setSelected:!checkBox.isSelected];
    if (checkBox.isSelected) {
        [createAccountbutton setEnabled:YES];
    }
    else {
        [createAccountbutton setEnabled:NO];
    }
}
@end
