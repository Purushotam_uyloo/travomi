//
//  BookNowTableViewCell.h
//  Travomi
//
//  Created by admin on 2/5/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookNowTableViewCell : UITableViewCell
{
    IBOutlet UIButton * BookNowbtn;
}

@property(nonatomic,retain)  IBOutlet UIButton * BookNowbtn;

@end
