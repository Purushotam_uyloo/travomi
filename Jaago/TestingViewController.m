//
//  TestingViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/28/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

//OLD TESTING CONTROLLER
//doesnt do anything now with the project should delete.

// delete stuff after.

//

#import "TestingViewController.h"
#import <Parse/Parse.h>

@interface TestingViewController ()

@property (nonatomic, assign) UIBackgroundTaskIdentifier fileUploadBackgroundTaskId;
@property (nonatomic, assign) UIBackgroundTaskIdentifier photoPostBackgroundTaskId;

@end

@implementation TestingViewController

@synthesize captionTextView, scrollView, photoImage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    // Do any additional setup after loading the view.
    
    [photoImage setImage:[UIImage imageNamed:@"JaagoImage.png"]];
}

-(void)dismissKeyboard
{
    [captionTextView resignFirstResponder];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.scrollView setContentOffset:CGPointMake(0, kbSize.height) animated:YES];
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (IBAction)textFieldDidBeginEditing:(UITextField *)sender {
    sender.delegate = self;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [textField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)post:(UIButton *)sender {
    NSArray *words = [captionTextView.text componentsSeparatedByString:@" " ];
    NSMutableArray *tags = [[NSMutableArray alloc]init];
    for (NSString *word in words) {
        if([word hasPrefix:@"#"]){
            NSString *stringTime;
            stringTime = [word substringFromIndex:1];
            stringTime =[stringTime lowercaseStringWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]];
            [tags addObject:stringTime];
        }
    }
    NSData *photoData = UIImageJPEGRepresentation(photoImage.image, 0.8f);
    PFFile *photoFile = [PFFile fileWithData:photoData];
    self.fileUploadBackgroundTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
    }];
    [photoFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded){
            [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
        }
        else{
            [[UIApplication sharedApplication] endBackgroundTask:self.fileUploadBackgroundTaskId];
        }
    }];
    
    if (!photoFile) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo"
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Dismiss",nil];
        [alert show];
        return;
    }
    
    NSString *trimmedComment = [captionTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    PFObject *photo = [PFObject objectWithClassName:@"Photo"];
    [photo setObject:[PFUser currentUser] forKey:@"creator"];
    [photo setObject:photoFile forKey:@"image"];
    [photo setObject:tags forKey:@"tags"];
    

    if (trimmedComment.length != 0) {
        [photo setObject:trimmedComment forKey:@"caption"];
    }

    PFACL *photoACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [photoACL setPublicReadAccess:YES];
    photo.ACL = photoACL;
    [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
            [alert show];
        }

        
    }];
    
    NSArray *uniquearray = [[NSSet setWithArray:tags] allObjects];

    NSLog(@"NewArray %@", uniquearray);
    
    for (NSString *word in uniquearray) {
        PFQuery *query = [PFQuery queryWithClassName:@"Hashtag"];
        [query whereKey:@"title" equalTo:word];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            if ([objects count]) {
                NSLog(@"Items");
                for(PFObject *object in objects){
                    [query getObjectInBackgroundWithId:object.objectId block:^(PFObject *hashtag, NSError *error) {
                        [hashtag[@"list"] addObject:photo];
                        [hashtag saveInBackground];
                    }];
                }
                
                
                
            }
            else {
                NSLog(@"No Items");
                PFObject *hashtags = [PFObject objectWithClassName:@"Hashtag"];
                [hashtags setObject:word forKey:@"title"];
                NSMutableArray *list = [[NSMutableArray alloc]init];
                [list addObject:photo];
                [hashtags setObject:list forKey:@"list"];
                [hashtags saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    
                    
                }];
                
            }
        
        }];
        
[self dismissViewControllerAnimated:YES completion:nil];
    }

   // PFObject *tagArray = [PFObject objectWithClassName:@"Hashtag"];
    // too much work today - query into parse if they dont have the tag make it, if they do alrready then chill on save a value to it.

    
    
    NSLog(@"%@", tags);
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
}


@end
