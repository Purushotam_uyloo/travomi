//
//  CustomTerms.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 11/12/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTerms : UIViewController
- (IBAction)donePressed:(id)sender;

@end
