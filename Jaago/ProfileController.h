//
//  ProfileController.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/16/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray * ProfileArray;
    NSTimer *myTimerName;
    UITableView * tableview;
}
@property(nonatomic,retain)IBOutlet UITableView * tableview;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@end
