//
//  LandingPageResultViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/1/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProperties.h"
#import "VideoModel.h"

@interface LandingPageResultViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property UIImage *tableImage;
@property NSString *tableCaption;
@property NSString *tableUser;
@property ImageProperties *selectedImageProperties;
@property NSString *controllerTitle;
@property NSString *selectedImage;
/*@property (strong, nonatomic) IBOutlet UIButton *bookButton;


@property (strong, nonatomic) IBOutlet UILabel *nonUserLabel;
@property (strong, nonatomic) IBOutlet UINavigationItem *navigationItem;
@property (strong, nonatomic) IBOutlet UIButton *user;
@property (strong, nonatomic) IBOutlet UITextView *caption;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *viewCommentsButton;*/
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UITableView *segueTable;
- (IBAction)reportButtonPressed:(id)sender;
- (IBAction)flagPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *reportView;

@property (strong, nonatomic) IBOutlet UIButton *videoButton;

@end
