//
//  SettingsViewController.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/22/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "SettingsViewController.h"


#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Montserrat-Regular" size:15.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = size;
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.settingsTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* CellIdentifier = @"Cell";
    
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userdata"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    switch (indexPath.row) {
        cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

        case (0):
            
            cell.textLabel.text = @"Social Media Settings";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"share_normal.png"];
            break;
        case (1):
            cell.textLabel.text = @"About";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"about_normal.png"];
            break;
//        case (2):
//            cell.textLabel.text = @"Help";
//            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"help_normal.png"];
            break;
        case (2):
            cell.textLabel.text = @"Terms of Use";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"terms_normal.png"];
            break;
        case (3):
            cell.textLabel.text = @"Privacy";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"privacy_normal.png"];
            break;
        case (4):
            cell.textLabel.text = @"Report Problem";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"report_normal.png"];
            break;
        case (5):
            cell.textLabel.text = @"Rate Us";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"rate_normal.png"];
            break;
            
        case (6):
            cell.textLabel.text = @"Refer to friend";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];
            
            cell.imageView.image = [UIImage imageNamed:@"rate_normal.png"];
            break;
            
        case (7):
            cell.textLabel.text = @"ScratchPad";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];
            
            cell.imageView.image = [UIImage imageNamed:@"rate_normal.png"];
            break;
            
        case (8):
            if (!dictionary) {
                cell.textLabel.text = @"Signin/Signup";
            }
            else {
                cell.textLabel.text = @"Signout";
            }
            
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];
            
            cell.imageView.image = [UIImage imageNamed:@"user_normal1.png"];
            break;
        default:
            cell.textLabel.text = @"Shouldn't get here";
    }

    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userdata"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    
    switch (indexPath.row) {

        case (0):
            [self performSegueWithIdentifier:@"socialmedia" sender:self];
            break;
        case (1):
            [self performSegueWithIdentifier:@"about" sender:self];
            break;
//        case (2):
//            [self performSegueWithIdentifier:@"help" sender:self];
            break;
        case (2):
            [self performSegueWithIdentifier:@"terms" sender:self];
            break;
        case (3):
            [self performSegueWithIdentifier:@"privacy" sender:self];
            break;
        case (4):
            [self performSegueWithIdentifier:@"problems" sender:self];
            break;
        case (5):
            [self performSegueWithIdentifier:@"rate" sender:self];
            break;
        case (6):
            [self OpenServerurl];
            break;
        case (7):
            [self performSegueWithIdentifier:@"ShowScratchPad" sender:self];
            break;
        case (8):
            if (dictionary) {

                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Travomi" message:@"Do you want to signout ?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
                [alert show];
                
                
            }
            else {
                [self performSegueWithIdentifier:@"customLogIn" sender:self];
            }
            
            break;
        default:
            break;
    }
}

-(void)OpenServerurl
{
   // [[UIApplication sharedApplication]openURL:]
    
    NSString *url=@"https://itunes.apple.com/us/app/travomi/id996799456?ls=1&mt=8";
    NSString * title =[NSString stringWithFormat:@"Please download and check Travomi app :%@",url];
    NSArray* dataToShare = @[title];
    UIActivityViewController* activityViewController =[[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop];
    [self presentViewController:activityViewController animated:YES completion:^{}];
}

- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"proflie"]) {
        
    }
    else if ([segue.identifier isEqualToString:@"socialmedia"]) {
        
    }else if ([segue.identifier isEqualToString:@"about"]) {
        
    }else if ([segue.identifier isEqualToString:@"help"]) {
        
    }else if ([segue.identifier isEqualToString:@"terms"]) {
        
    }else if ([segue.identifier isEqualToString:@"privacy"]) {
        
    }else if ([segue.identifier isEqualToString:@"problems"]) {
        
    }else if ([segue.identifier isEqualToString:@"rate"]) {
        
    }

    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userdata"];
        UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
        
        [[tabBarController.viewControllers objectAtIndex:0] popToRootViewControllerAnimated:YES];
        [tabBarController setSelectedIndex:0];
    }
}

@end
