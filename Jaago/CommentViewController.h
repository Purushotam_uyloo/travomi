//
//  CommentViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/29/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProperties.h"

// this is the interface for the comment for each picture (if the picture was associated with jaago)
@interface CommentViewController : UIViewController <UITextViewDelegate>
- (IBAction)deleteButtonPressed:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@property NSString *photoID;
@property (strong, nonatomic) IBOutlet UITextView *addCommentView;
@property ImageProperties *photoProperties;
@property (strong, nonatomic) IBOutlet UITableView *commentTable;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
