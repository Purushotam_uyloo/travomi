//
//  ImageViewCell4.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 9/15/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "ImageViewCell4.h"

@implementation ImageViewCell4


@synthesize pictureView1, pictureView2, pictureView3, pictureView4, button1, button2, button3, button4;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
