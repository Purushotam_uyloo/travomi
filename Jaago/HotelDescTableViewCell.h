//
//  HotelDescTableViewCell.h
//  Travomi
//
//  Created by admin on 2/5/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelDescTableViewCell : UITableViewCell
{
    
}
@property(nonatomic,retain)IBOutlet UIButton * MapButton;

@property(nonatomic,retain)IBOutlet UIView * backImgview;
@property(nonatomic,retain) IBOutlet UILabel * HotelNameLbl,*HotelDescLbl;

@end
