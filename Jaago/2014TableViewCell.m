//
//  2014TableViewCell.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/21/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "2014TableViewCell.h"

@implementation _014TableViewCell

@synthesize imageView;
@synthesize number;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
