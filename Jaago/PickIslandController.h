//
//  PickIslandController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/19/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface PickIslandController : UITableViewController < PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@end
