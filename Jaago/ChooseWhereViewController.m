//
//  ChooseWhereViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/21/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "ChooseWhereViewController.h"
#import "ImageReviewViewController.h"
#import "CustomLogInViewController.h"


#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
#import <Parse/Parse.h>

@interface ChooseWhereViewController ()

@end
NSString *type;
@implementation ChooseWhereViewController

-(void)camera:(UIButton *)sender {
    
    type = @"camera";
    NSLog(@"ChooseWhereViewController: now set type to %@",type);
    [self performSegueWithIdentifier:@"toEditPicture"sender:self];


}
-(void)existing:(UIButton *)sender {
    type = @"existing";
    NSLog(@"ChooseWhereViewController: now set type to %@",type);
    [self performSegueWithIdentifier:@"toEditPicture"sender:self];
    

}

- (void)video:(UIButton *)sender {
    type = @"video";
    NSLog(@"ChooseWhereViewController: now set type to %@",type);
    [self performSegueWithIdentifier:@"toEditPicture"sender:self];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear ChooseWhereViewController");

}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"viewDidAppear ChooseWhereViewController");
    
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userdata"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    
    if (dictionary == nil) {
        CustomLogInViewController *loginViewController = (CustomLogInViewController *)[[UIStoryboard storyboardWithName:@"Main_Storyboard" bundle:nil] instantiateViewControllerWithIdentifier:@"loginViewController"];
        [self presentViewController:loginViewController animated:YES completion:nil];
    }

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"viewDidLoad ChooseWhereViewController");
    
    
    

    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Montserrat-Regular" size:15.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];

    self.navigationController.navigationBar.titleTextAttributes = size;

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    //self.navigationController.title = @"Camera";

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ImageReviewViewController *controller = [segue destinationViewController];
    controller.type = type;
    NSLog(@"in prepareforSEGUE, %@", type);

    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
