//
//  WebVideoViewController.m
//  YTBrowser
//
//  Created by Marin Todorov on 09/01/2013.
//  Copyright (c) 2013 Underplot ltd. All rights reserved.
//

#import "WebVideoViewController.h"

@implementation WebVideoViewController
{
    IBOutlet UIWebView* webView;
    
}
@synthesize videotype,youtubevideo,uservideodata,vineVideo;
static const NSString *ItemStatusContext;


-(void)viewDidAppear:(BOOL)animated
{
    if([videotype isEqual:@"Youtube"]){
        self.playerView.hidden=YES;
        VideoLink* link = self.youtubevideo.link[0];
        
        NSString* videoId = nil;
        NSArray *queryComponents = [link.href.query componentsSeparatedByString:@"&"];
        for (NSString* pair in queryComponents) {
            NSArray* pairComponents = [pair componentsSeparatedByString:@"="];
            if ([pairComponents[0] isEqualToString:@"v"]) {
                videoId = pairComponents[1];
                break;
            }
        }
        
        if (!videoId) {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Video ID not found in video URL" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil]show];
            return;
        }
        
        NSLog(@"Embed video id: %@", videoId);
        
        NSString *htmlString = @"<html><head>\
        <meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = 320\"/></head>\
        <body style=\"background:#000;margin-top:0px;margin-left:0px\">\
        <iframe id=\"ytplayer\" type=\"text/html\" width=\"320\" height=\"240\"\
        src=\"http://www.youtube.com/embed/%@?autoplay=1\"\
        frameborder=\"0\"/>\
        </body></html>";
        
        htmlString = [NSString stringWithFormat:htmlString, videoId];
        
        [webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:@"http://www.youtube.com"]];
    }else if([videotype isEqualToString:@"Vine"]){
        self.playerView.hidden=YES;
        [webView loadRequest:[NSURLRequest requestWithURL:vineVideo.videoURL]];

    }else{
        NSLog(@"playing %@ video",videotype);
        NSString *tempFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"tmp.mov"];
        
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager createFileAtPath:tempFilePath contents:uservideodata attributes:nil];
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:tempFilePath] options:nil];
        
        [self playAsset:asset];
        
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [manager removeItemAtPath:tempFilePath error:nil];
        });
    }
}

- (void)playAsset:(AVURLAsset*) asset {
    NSLog(@"begin local loading");
    
    
        NSString *tracksKey = @"tracks";
        // Define this constant for the key-value observation context.
        [asset loadValuesAsynchronouslyForKeys:@[tracksKey] completionHandler:
         ^{
             // The completion block goes here.
             // Completion handler block.
             dispatch_async(dispatch_get_main_queue(),
                            ^{
                                
                                NSError *error;
                                AVKeyValueStatus status = [asset statusOfValueForKey:tracksKey error:&error];
                                
                                if (status == AVKeyValueStatusLoaded) {
                                    self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
                                    
                                    [[NSNotificationCenter defaultCenter] addObserver:self
                                                                             selector:@selector(playerItemDidReachEnd:)
                                                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                                                               object:self.playerItem];
                                    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
                                    
                                    [self.playerView setPlayer:self.player];
                                    for(AVAssetTrack * track in self.playerItem.asset.tracks){
                                        NSLog(@"track info:%@",track.formatDescriptions.description);
                                    }
                                    [self.player play];
                                    
                                    
                                }
                                else {
                                    // You should deal with the error appropriately.
                                    NSLog(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
                                }
                            });
             
         }];
        
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [self.player seekToTime:kCMTimeZero];
}



@end
