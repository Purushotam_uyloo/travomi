//
//  _014AppDelegate.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/8/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface _014AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
