//
//  InitialSocialMedia.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 9/11/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "InitialSocialMedia.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <Parse/Parse.h>

@interface InitialSocialMedia ()

@property (nonatomic, strong) ACAccountStore *accountStore;


@end

@implementation InitialSocialMedia
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated {
    
}
- (void)viewDidLoad
{
    // first time it loaded the page it sets the switches based on the parse user field for jaago
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString * FacebookKey = [defaults valueForKey:@"facebook"];
    if(FacebookKey==nil || [FacebookKey isEqualToString:@"No"])
    {
        facebookSwitch.on = NO;
    }
    else
    {
        facebookSwitch.on = YES;
    }
    
    NSString * twitterKey = [defaults valueForKey:@"twitter"];
    if(twitterKey==nil || [twitterKey isEqualToString:@"No"])
    {
        twitterSwitch.on = NO;
    }
    else
    {
        twitterSwitch.on = YES;
    }
    
    NSString * tumblrKey = [defaults valueForKey:@"tumblr"];
    if(tumblrKey==nil || [tumblrKey isEqualToString:@"No"])
    {
        tumblrSwitch.on = NO;
    }
    else
    {
        tumblrSwitch.on = YES;
    }
    
    
    
    
    
    
    
    self.accountStore = [[ACAccountStore alloc]init];
    
    
//    // Do any additional setup after loading the view.
//    if([[[PFUser currentUser] objectForKey:@"facebook"] isEqual: @YES]) {
//        facebookSwitch.on = YES;
//    }
//    else {
//        facebookSwitch.on = NO;
//        
//    }
//    if([[[PFUser currentUser] objectForKey:@"twitter"] isEqual: @YES]) {
//        twitterSwitch.on = YES;
//    }
//    else {
//        twitterSwitch.on = NO;
//        
//    }
//    if([[[PFUser currentUser] objectForKey:@"tumblr"] isEqual: @YES]) {
//        tumblrSwitch.on = YES;
//    }
//    else {
//        tumblrSwitch.on = NO;
//        
//    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)switchFacebookSwitch:(id)sender {
    
    if (facebookSwitch.on) {
        NSLog(@"facebook switch on");
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"Yes" forKey:@"facebook"];
        [defaults synchronize];
        

        
    }
    else {
        NSLog(@"facebook switch off");
        //        [[PFUser currentUser] setObject:@NO forKey:@"facebook"];
        //        [[PFUser currentUser] saveInBackground];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"No" forKey:@"facebook"];
        [defaults synchronize];
        
        
    }
    
//    if (facebookSwitch.on) {
//        NSLog(@"facebook switch on");
//        // If the session state is any of the two "open" states when the button is clicked
//        if (FBSession.activeSession.state == FBSessionStateOpen
//            || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
//            
//            
//            
//            [[PFUser currentUser] setObject:@NO forKey:@"facebook"];
//            [[PFUser currentUser] saveInBackground];
//            // Close the session and remove the access token from the cache
//            // The session state handler (in the app delegate) will be called automatically
//            [FBSession.activeSession closeAndClearTokenInformation];
//            
//            // If the session state is not any of the two "open" states when the button is clicked
//        } else {
//            // Open a session showing the user the login UI
//            // You must ALWAYS ask for public_profile permissions when opening a session
//            [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"]
//                                               allowLoginUI:YES
//                                          completionHandler:
//             ^(FBSession *session, FBSessionState state, NSError *error) {
//                 
//            }];
//        }
//        
//        
//    }
//    else {
//        NSLog(@"facebook switch off");
//        [[PFUser currentUser] setObject:@NO forKey:@"facebook"];
//        [[PFUser currentUser] saveInBackground];
//        
//        
//    }

}
- (IBAction)switchTwitterSwitch:(id)sender {
    NSArray *accounts = [self.accountStore accountsWithAccountType:[self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter]];
    
    if (twitterSwitch.on) {
        if ([self checkTwitterAuth] == YES && [accounts count] != 0) {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:@"Yes" forKey:@"twitter"];
            [defaults synchronize];

        }
    }
    else {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"No" forKey:@"twitter"];
        [defaults synchronize];
        
    }
}
- (IBAction)switchTumblrSwitch:(id)sender {
    if (tumblrSwitch.on) {
        NSLog(@"tumblr switch on");
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"Yes" forKey:@"tumblr"];
        [defaults synchronize];
    }
    else {
        NSLog(@"tumblr switch off");
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"No" forKey:@"tumblr"];
        [defaults synchronize];
        
    }
}

- (IBAction)dismiss:(id)sender {
    
    
    [[PFUser currentUser] setObject:@YES forKey:@"firstTime"];
    [[PFUser currentUser] saveInBackground];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(BOOL)checkTwitterAuth {
    __block BOOL flag = YES;
    ACAccountType *twitterType =
    [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:twitterType  options:nil completion:^(BOOL granted, NSError *error) { //completion handling is on indeterminate queue so I force main queue inside
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted) {
                NSLog(@"here");
                NSArray *accounts = [self.accountStore accountsWithAccountType:twitterType];
                
                if ([accounts count] == 0) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log into Twitter" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
                    [alert show];
                    twitterSwitch.on = NO;
                    flag = false;
                    
                }
                else
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setValue:@"Yes" forKey:@"twitter"];
                    [defaults synchronize];
                }
                
            }
            else {
                NSLog(@"not granted");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Grant Travomi Permissions to Twitter" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
                [alert show];
                twitterSwitch.on = NO;
                flag = NO;
                
                
            }
        }
                       );
        
    }];
    return flag;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
