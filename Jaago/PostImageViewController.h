//
//  PostImageViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/26/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
#import "UserVideo.h"

@interface PostImageViewController : UIViewController <UITextViewDelegate, UIAlertViewDelegate, MBProgressHUDDelegate, UIPickerViewDelegate, UIPickerViewDataSource,CLLocationManagerDelegate>
{
    MBProgressHUD *HUD;
    CLLocationManager *manager;
    NSString * HotelId;
}

@property UIImage *image;
@property UserVideo *video;
@property NSString *type;

- (IBAction)selectLocationButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextView *caption;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *receivedPhoto;
@property (strong, nonatomic) IBOutlet UIButton *location;

@property (weak, nonatomic) IBOutlet UIButton *islandLocation;
@property (weak, nonatomic) IBOutlet UIButton *hotel;

//@property (strong, nonatomic) IBOutlet NSMutableArray *islands;
- (IBAction)postButtonPressed:(UIButton *)sender;



@end
