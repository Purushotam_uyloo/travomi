//
//  islandPickerCell.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/12/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface islandPickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UIImageView *shadowImage;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *subTextField;

@end
