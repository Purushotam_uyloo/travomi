//
//  DestinationPickerCellTableViewCell.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/20/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DestinationPickerCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *island;
@property (weak, nonatomic) IBOutlet UITextField *hotel;
@property (weak, nonatomic) IBOutlet UITextField *favorites;
@property (weak, nonatomic) IBOutlet UIImageView *hotelImage;

@end
