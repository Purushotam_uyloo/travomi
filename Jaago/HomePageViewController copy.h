//
//  HomePageViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/21/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


// initial starting page view controller past picking the island
@interface HomePageViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,// PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>
{
    NSMutableData *responseData;
}

@property NSString *islandPicked;
@property NSArray *islandHotels;
@property UIImage *navigationImage;

@property (strong, nonatomic) IBOutlet UISearchBar *theSearchBar;
@property (strong, nonatomic) IBOutlet UITableView *realTableView;
@property (strong, nonatomic) IBOutlet UITableView *searchHotelsTableView;
@property (strong, nonatomic) NSMutableArray *images;
- (IBAction)logoutButtonPressed:(id)sender;
- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)profileButtonPressed:(id)sender;


@end
