//
//  ProfilePageViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/4/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "ProfilePageViewController.h"
#import <Parse/Parse.h>
#import "2014TableViewCell.h"

@interface ProfilePageViewController ()
{
}
@end


@implementation ProfilePageViewController
@synthesize images;
/*- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}*/



- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"hithithithithit");
    images = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    PFQuery *profilePhotoQuery = [PFQuery queryWithClassName:@"Photo"];
    [profilePhotoQuery whereKey:@"creator" equalTo:[PFUser currentUser]];
    [profilePhotoQuery orderByDescending:@"createdAt"];
    [profilePhotoQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            for(PFObject *hello in objects){
                PFFile *imageFile = hello[@"image"];
                NSLog(@"what is the caption to this thing?: %@",hello[@"image"]);
                [imageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                    if (!error) {
                        NSLog(@"got here yay");
                        [images addObject:[UIImage imageWithData:imageData]];
                        [self.tableView reloadData];
                    }
                }];
            }
        }
    }];

   
    
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"in number of rows %i",[images count]);
    
    return [images count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"in cellforrowatindexPath of rows");

    static NSString *CellIdentifier = @"2014TableViewCell";
    _014TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell.pictureView setImage:[images objectAtIndex:indexPath.row]];
    
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
