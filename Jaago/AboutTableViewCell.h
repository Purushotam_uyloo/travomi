//
//  AboutTableViewCell.h
//  Travomi
//
//  Created by admin on 2/4/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface AboutTableViewCell : UITableViewCell
{
    
   // IBOutlet UILabel * HotelDescLbl;

    
}
@property(nonatomic,retain)IBOutlet UIButton * Morebtn;
@property(nonatomic,retain)IBOutlet UIView * backImgview;
@property(nonatomic,retain) IBOutlet TTTAttributedLabel *HotelDescLbl;
@property(nonatomic,retain) IBOutlet UILabel * HotelNameLbl;

@end
