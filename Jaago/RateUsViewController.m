//
//  RateUsViewController.m
//  Travomi
//
//  Created by WangYinghao on 11/24/15.
//  Copyright © 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "RateUsViewController.h"

@interface RateUsViewController ()

@end

@implementation RateUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *iTunesLink = @"https://itunes.apple.com/us/app/travomi/id996799456?ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
