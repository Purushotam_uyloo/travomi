//
//  CommentsController.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/21/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProperties.h"

@interface CommentsController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *test;
@property (weak, nonatomic) IBOutlet UITableView *commentTable;
@property (strong, nonatomic) ImageProperties *property;
@property (nonatomic, retain) NSMutableArray *textArray;
@property (strong, nonatomic) IBOutlet UIView *optionsView;
@property (strong, nonatomic) IBOutlet UIView *commentView;
@property (strong, nonatomic) IBOutlet UITextView *commentField;
@property Boolean hideDelete;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
- (IBAction)postPressed:(id)sender;
- (IBAction)deletePressed:(id)sender;
- (IBAction)addCommentPressed:(id)sender;
- (IBAction)optionsPressed:(id)sender;

@end
