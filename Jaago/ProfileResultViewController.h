//
//  ProfileResultViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/11/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProperties.h"


@interface ProfileResultViewController : UIViewController<UINavigationControllerDelegate, UITextFieldDelegate>
@property UIImage *selectedImage;
@property NSString *selectedImageCaption;
@property NSInteger number;
@property ImageProperties *selectedImageProperties;


@property (strong, nonatomic) IBOutlet UIButton *likeButton;

@property (strong, nonatomic) IBOutlet UIButton *unlikeButton;
@property (strong, nonatomic) IBOutlet UIImageView *picture;
@property (strong, nonatomic) IBOutlet UITextView *caption;
- (IBAction)comments:(UIButton *)sender;
- (IBAction)likePressed:(UIButton *)sender;
- (IBAction)removePhoto:(UIButton *)sender;
- (IBAction)unlikePressed:(UIButton *)sender;


@end

