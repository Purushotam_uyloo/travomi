//
//  CustomSignupViewController.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/9/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "MBProgressHUD.h"

@interface CustomSignupViewController : UIViewController <UITextFieldDelegate,MBProgressHUDDelegate>
{
    MBProgressHUD * HUD;
}
@property(strong,nonatomic)NSDictionary * Datadict;
@property(nonatomic)BOOL  IsFromFacebook;


@property (strong, nonatomic) IBOutlet UIView *controllerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *termsButton;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UIButton *checkBox;
- (IBAction)toTermsofUse:(id)sender;
- (IBAction)checkboxPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *createAccountbutton;

@end
