//
//  BookNowTableViewCell.m
//  Travomi
//
//  Created by admin on 2/5/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "BookNowTableViewCell.h"

@implementation BookNowTableViewCell
@synthesize BookNowbtn;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    BookNowbtn.layer.borderWidth = 1.0;
    BookNowbtn.layer.borderColor = [UIColor orangeColor].CGColor;
    
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
