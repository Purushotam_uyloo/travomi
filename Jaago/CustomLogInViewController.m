//
//  CustomLogInViewController.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/9/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "CustomLogInViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HomePageViewController.h"
#import <ParseFacebookUtilsV4/ParseFacebookUtilsV4.h>
#import "GTLQueryCustomerLogin.h"
#import "GTLServiceCustomerLogin.h"
#import "GTLCustomerLoginUserLoginResponse.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "CustomSignupViewController.h"


@interface CustomLogInViewController ()

@end

@implementation CustomLogInViewController

@synthesize scrollView, usernameField, passwordField;
static GTLServiceCustomerLogin *service = nil;

- (void)viewDidLoad {

    
    facebookdict = [[NSMutableDictionary alloc]init];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (!service) {
        service = [[GTLServiceCustomerLogin alloc] init];
        service.retryEnabled = YES;
        
    }

    [scrollView setContentOffset:CGPointZero animated:YES];

    

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:true];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    UIActivityIndicatorView *actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    actInd.frame = CGRectMake(0, 0, 24, 24);
    actInd.center = self.view.center;
    actInd.hidesWhenStopped = true;
    //self.view.addSubview(actInd);   didnt work but is at 24:36 of tutorial
    [self.view addSubview:actInd ];
    
    _forgotButton.layer.borderWidth = .5f;
    _forgotButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, usernameField.frame.size.height - 1, usernameField.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
    [usernameField.layer addSublayer:bottomBorder];
    
    
    [scrollView setContentOffset:CGPointZero animated:YES];
    
    if ([PFUser currentUser] != nil) {
        NSLog(@"Already logged in!");
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)loginAction:(id)sender {
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    GTLQueryCustomerLogin *query = [GTLQueryCustomerLogin queryForLoginWithRLoginType:@"Travomi" username:self.usernameField.text];
    [query setPassword:passwordField.text];
    
    [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLCustomerLoginUserLoginResponse *object, NSError *error) {
        
        if(error == nil)
        {
            
            GTLCustomerLoginUserLoginResponse * resp = object;
            
            if([resp.status.stringValue isEqualToString:@"1"])
            {
            NSMutableDictionary * dict = resp.user;
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dict];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"userdata"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [HUD hide:YES];
            [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Travomi" message:resp.errorMessage delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                [alert show];
                
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
            }
            
            
            
        }
        else
        {
            [HUD hide:YES];
            [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
        }
        
        // Do something with items.
    }];
  
  }




    

- (IBAction)signupAction:(id)sender {

}

#pragma KEYBOARD STUFF


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    UITextField * alertTextField = [alertView textFieldAtIndex:0];
    if([title isEqualToString:@"Ok"])
    {
        
        [PFUser requestPasswordResetForEmail:alertTextField.text];
        //self performSequeWithIdentifi
        
    }
    // do whatever you want to do with this UITextField.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"does it even get int the textFieldShouldReturn");
    if (textField == usernameField) {
        return [textField resignFirstResponder];
    }
    else if (textField == passwordField) {
        return [textField resignFirstResponder];
    }
    else {
        return [textField resignFirstResponder];
    }
    
}




-(void) textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint;
    if (textField == usernameField) {
        scrollPoint = CGPointMake(0, textField.frame.origin.y+139);

    }
    else if (textField == passwordField){
        scrollPoint = CGPointMake(0, textField.frame.origin.y+100);
    }
    else {
        scrollPoint = CGPointMake(0, textField.frame.origin.y);
    }
    [scrollView setContentOffset:scrollPoint animated:YES];
    
    /*_scrollView.frame = CGRectMake(_scrollView.frame.origin.x,
                                       _scrollView.frame.origin.y,
                                       _scrollView.frame.size.width,
                                       _scrollView.frame.size.height - 215 + 50);*/
}

-(void) textFieldDidEndEditing:(UITextField *)textField {
    
    [scrollView setContentOffset:CGPointZero animated:YES];

    /*_scrollView.frame = CGRectMake(_scrollView.frame.origin.x,
                                       _scrollView.frame.origin.y,
                                       _scrollView.frame.size.width,
                                       _scrollView.frame.size.height + 215 - 50);*/
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
    [self textFieldDidBeginEditing:nil];
    
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSLog(@"in keyboardwasshown");
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.scrollView setContentOffset:CGPointMake(0, kbSize.height) animated:YES];
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSLog(@"in keyboardwillbehidden");
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    //TODO: this is a hack
    if ([self.presentingViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController *) self.presentingViewController;
        [tabBarController setSelectedIndex:0];
    }
}


- (IBAction)forgot:(id)sender {

    [self performSegueWithIdentifier:@"ShowForgotPassword" sender:nil];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Enter Email" message:@"Please enter the email address for your account" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
//    alertView.tag = 2;
//    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
//    [alertView show];
}


#pragma mark - Login with Facebook

- (IBAction)loginWithFacebook {
  
    
    [facebookdict removeAllObjects];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error %@",error.description);
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"%@",result);
             
             NSLog(@"Logged in");
             if ([result.grantedPermissions containsObject:@"email"])
             {
                 NSLog(@"result is:%@",result);
                 [self fetchUserInfo];
             }
         }
     }];
    
    
    
    
//    NSArray * permissionArray = [[NSArray alloc] initWithObjects:@"public_profile", @"email" , nil ];
//    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionArray block:^(PFUser *user, NSError *error){
//        if (error == nil) {
//            //skip the email verification if user use facebook to login
//            [user setObject:@true forKey:@"emailVerified"];
//            [user save];
//            if (user.isNew) {
//                
//                NSLog(@"New User created, info: %@", user.description);
//            } else {
//                NSLog(@"Returning user");
//            }
//        } else {
//            NSLog(@"Error while logging in with Facebook!");
//        }
//        
//    }];
    
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday, bio ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
               //  NSLog(@"resultis:%@",result);
                 
                 NSDictionary * dict = (NSDictionary *)result;
                 NSLog(@"%@",[dict valueForKey:@"first_name"]);
                 [facebookdict setValue:[dict valueForKey:@"email"] forKey:@"email"];
                 [facebookdict setValue:[dict valueForKey:@"first_name"] forKey:@"username"];
                 [facebookdict setValue:[dict valueForKey:@"name"] forKey:@"name"];
                 [facebookdict setValue:[dict valueForKey:@"id"] forKey:@"id"];
                 [self CallLoginApi:[dict valueForKey:@"email"]];


             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
        
    }
    
}

-(void)CallLoginApi:(NSString *)username
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    GTLQueryCustomerLogin *query = [GTLQueryCustomerLogin queryForLoginWithRLoginType:@"Facebook" username:username];
    [query setRThirdPartyID:[facebookdict valueForKey:@"id"]];
    
    [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLCustomerLoginUserLoginResponse *object, NSError *error) {
        
        if(error == nil)
        {
            
            GTLCustomerLoginUserLoginResponse * resp = object;
            
            if([resp.status.stringValue isEqualToString:@"1"])
            {
                NSMutableDictionary * dict = resp.user;
                
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dict];
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"userdata"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:@"No" forKey:@"facebook"];
                [defaults setValue:@"No" forKey:@"twitter"];
                [defaults setValue:@"No" forKey:@"tumblr"];
                [defaults synchronize];


  
                
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else
            {
                
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                
                if([resp.errorId.stringValue isEqualToString:@"3"])
                {
//                    CustomSignupViewController * signuppage = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"signin"];
//                    signuppage.Datadict = facebookdict;
//                    signuppage.IsFromFacebook = true;
//                    [self presentViewController:signuppage animated:true completion:nil];
//                   // [self.navigationController pushViewController:signuppage animated:YES];
                    
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_Storyboard" bundle:nil];
                    CustomSignupViewController *vc = (CustomSignupViewController *) [storyboard instantiateViewControllerWithIdentifier:@"signin"];
                    vc.Datadict = facebookdict;
                    vc.IsFromFacebook = true;
                    
                    [self showDetailViewController:vc sender:self];

                   // [self.navigationController showViewController:vc sender:nil];
                    
                }


            }
            
            
            
        }
        else
        {
            
            UIAlertView * alert  = [[UIAlertView alloc]initWithTitle:@"Travomi" message:error.description delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            
            [HUD hide:YES];
            [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
        }
        
        // Do something with items.
    }];

}



@end
