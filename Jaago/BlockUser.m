//
//  BlockUser.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 11/15/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "BlockUser.h"
#import "UserBlocked.h"

@interface BlockUser ()

@end

@implementation BlockUser

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"toBlockedUser"]){
        //UserBlocked* controller = segue.destinationViewController;
        
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}




- (IBAction)cancelPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)itemPressed:(id)sender {
    [self performSegueWithIdentifier:@"toBlockedUser" sender:self];

}
@end
