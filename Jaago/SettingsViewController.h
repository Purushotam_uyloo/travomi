//
//  SettingsViewController.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/22/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *settingsTable;

@end
