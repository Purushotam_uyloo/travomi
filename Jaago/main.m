
//
//  main.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/8/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "_014AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([_014AppDelegate class]));
    }
}
