//
//  PostImageViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/26/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "PostImageViewController.h"
#import <Parse/Parse.h>
#import "HomePageViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "RootNavigationController.h"


#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
#import "GTLHotelEndpointHotel.h"
#import "GTLCustomerLoginCustomerProfile.h"


#define GOOGLEAPIKEY @"AIzaSyC5LTp0l2m74UDj7SoKP81t2XGGjAq2X4M"

@interface PostImageViewController ()
{
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
}

@property(nonatomic,strong)IBOutlet UILabel *TaggedLbl;
@property (nonatomic, strong) ACAccountStore *accountStore;
@property (nonatomic, strong) UITextField *pickerViewTextField;

@property(nonatomic,strong)IBOutlet UIButton * TagHotelBtn ,* PostBtn;

//
@property (nonatomic, strong) UITextField *pickerViewTextField2;

@end

//Dolphin Cove motel (lat - 35.1398269 and long - -120.6432269)
//Courtesy Inn (lat - 35.6145169 and long - -121.14487429999997)

@implementation PostImageViewController
{
    NSMutableArray *tags;
    NSMutableArray *islands;
    
    NSMutableArray *hotelId;

    
    NSArray *mauiPicked;
    NSArray *hawaiiPicked;
    NSArray *oahuPicked;
    NSArray *kauaiPicked;
    NSDictionary *hotelNames;
    NSString *postTag;
}
@synthesize caption,image, scrollView, receivedPhoto, location, video, type, hotel, islandLocation;
@synthesize TagHotelBtn,PostBtn;

@synthesize TaggedLbl;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.accountStore = [[ACAccountStore alloc]init];
    [self.scrollView setContentOffset:CGPointMake(0, 65) animated:YES];

    
    [TagHotelBtn setBackgroundColor:[UIColor clearColor]];
    [PostBtn setBackgroundColor:[UIColor clearColor]];
    
    [TagHotelBtn.layer setBorderWidth:1.0];
    [TagHotelBtn.layer setBorderColor:[UIColor orangeColor].CGColor];
    
    [PostBtn.layer setBorderWidth:1.0];
    [PostBtn.layer setBorderColor:[UIColor orangeColor].CGColor];
    
    [TagHotelBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [PostBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];

    
    
    
    
    
    // Do any additional setup after loading the view.
    
    
    //initializes the arrays in which to store the photos generated from instagram.
    RootNavigationController * rootNVC = (RootNavigationController *)self.navigationController;
    hotelNames = rootNVC.hotelNames;
    
    mauiPicked = [[NSArray alloc] init];
    hawaiiPicked = [[NSArray alloc] init];
    oahuPicked = [[NSArray alloc] init];
    kauaiPicked = [[NSArray alloc] init];
    postTag = @"NEVERGETTHIS";
    
    //sets the title for the screen based on what was picked and to set what island the picture should go under.
    NSCharacterSet *newlineCharSet = [NSCharacterSet newlineCharacterSet];
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"MauiTags" ofType:@"txt"];
    NSString* fileContents = [NSString stringWithContentsOfFile:filePath
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    
    mauiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    filePath = [[NSBundle mainBundle] pathForResource:@"KauaiTags" ofType:@"txt"];
    fileContents = [NSString stringWithContentsOfFile:filePath
                                             encoding:NSUTF8StringEncoding
                                                error:nil];
    
    kauaiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    filePath = [[NSBundle mainBundle] pathForResource:@"OahuTags" ofType:@"txt"];
    fileContents = [NSString stringWithContentsOfFile:filePath
                                             encoding:NSUTF8StringEncoding
                                                error:nil];
    
    oahuPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    filePath = [[NSBundle mainBundle] pathForResource:@"HawaiiTags" ofType:@"txt"];
    fileContents = [NSString stringWithContentsOfFile:filePath
                                             encoding:NSUTF8StringEncoding
                                                error:nil];
    
    hawaiiPicked = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];

    
    
    islands = [[NSMutableArray alloc] init];
    hotelId = [[NSMutableArray alloc] init];
    
//    [islands addObject:@"Maui"];
//    [islands addObject:@"Hawaii"];
//    [islands addObject:@"Oahu"];
//    [islands addObject:@"Kauai"];
    
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"hotel"];
    NSMutableArray *hotels = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    
    for(int i=0 ; i < [hotels count];i++)
    {
        GTLHotelEndpointHotel * wrapper = (GTLHotelEndpointHotel *)[hotels objectAtIndex:i];
        [islands addObject:wrapper.name];
        [hotelId addObject:[NSString stringWithFormat:@"%@",wrapper.identifier]];
        
        
    }
    
    
    
    // set the frame to zero
    self.pickerViewTextField = [[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.pickerViewTextField];
    
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    // set change the inputView (default is keyboard) to UIPickerView
    self.pickerViewTextField.inputView = pickerView;
    
    //new
    self.pickerViewTextField2 = [[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.pickerViewTextField2];
    
    UIPickerView *pickerView2 = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    pickerView2.showsSelectionIndicator = YES;
    pickerView2.dataSource = self;
    pickerView2.delegate = self;
    self.pickerViewTextField2.inputView = pickerView2;
    
    
    
    
    
    
    
    // add a toolbar with Cancel & Done button
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched:)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
    
    // the middle button is to make the Done button align to right
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    self.pickerViewTextField.inputAccessoryView = toolBar;
    
    //new
    self.pickerViewTextField2.inputAccessoryView = toolBar;
    
    
    [receivedPhoto setImage:image];
    tags = [[NSMutableArray alloc]init];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];

}

-(void)viewDidAppear:(BOOL)animated{
   // [self getCurrentLocation];
}
- (void)getCurrentLocation {
    if ([CLLocationManager locationServicesEnabled])
    {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        // Requesting for authorization
        
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
    }
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //    NSLog(@"didUpdateToLocation: %@", newLocation);
    if (currentLocation == NULL) {
        currentLocation = newLocation;
        [self findAddressFromCoordinates:currentLocation];
    }
    [locationManager stopUpdatingLocation];
}

-(void)findAddressFromCoordinates:(CLLocation*)location{
//https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
    NSString* strAPI = [NSString stringWithFormat: @"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&key=%@", location.coordinate.latitude, location.coordinate.longitude, GOOGLEAPIKEY];
    [JSONHTTPClient getJSONFromURLWithString: strAPI
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      NSLog(@"Got JSON from web: %@", json);                                      
                                      NSArray * results = json[@"results"];
                                  }];

}


-(void)dismissKeyboard
{
    [caption resignFirstResponder];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    //CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //[self.scrollView setContentOffset:CGPointMake(0, kbSize.height) animated:YES];
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
}
- (IBAction)textViewDidBeginEditing:(UITextView *)sender {
    if ([caption.text  isEqual: @"Type Caption here."]) {
     caption.text = @"";
     }
    
//    [sender setText:@""];
    
    sender.delegate = self;
    
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    NSLog(@"In New METHOD");
    BOOL travomi = NO;
    
    NSArray *words = [caption.text componentsSeparatedByString:@" " ];
    [tags removeAllObjects];
    for (NSString *word in words) {
        if([word hasPrefix:@"#"]){
            NSString *tagString = [word substringFromIndex:1];
            
            if ([tagString caseInsensitiveCompare:@"Jaago"] == NSOrderedSame) {
                travomi = YES;
            }
            //tagString =[tagString lowercaseStringWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]];
            
        }
    }
    if (travomi == NO && caption.text.length !=0 ) {
        caption.text = [caption.text stringByAppendingString:@" #Travomi"];
    }
    
    
}
- (BOOL)textViewShouldReturn:(UITextView *)textField {
    
    return [textField resignFirstResponder];
}



- (IBAction)postButtonPressed:(UIButton *)sender {
    
    
    
    
    if([TaggedLbl.text isEqualToString:@""])
    {
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Travomi" message:@"Please tag any Hotel to upload this Image" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    if([type isEqual:@"camera"] || [type isEqual: @"existing"]){
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    
    HUD.mode = MBProgressHUDModeDeterminate;
            HUD.delegate = self;
            HUD.labelText = @"Uploading";
            [HUD show:YES];
    
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userdata"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    NSString * UserID;
    
    if (dictionary) {
        
        GTLCustomerLoginCustomerProfile * dict = (GTLCustomerLoginCustomerProfile *) dictionary;
        UserID = [NSString stringWithFormat:@"%@",dict.cUID];
        
        NSLog(@"%@",dict.cUID);
        
    }
    
    NSLog(@"%@",HotelId);
    
    NSString *fileName = [NSString stringWithFormat:@"images/%.0f.png",[[NSDate date] timeIntervalSince1970] * 1000];
    
    NSString *urlString = @"https://travomidev.appspot.com/uploadFile";
    NSURL *url = [NSURL URLWithString:urlString];
    NSDictionary *params = @{@"filename":fileName,@"filetype":@"image/png",@"isVideo":@"1",@"userId":UserID,@"hotelId":HotelId,@"isEdit":@"1"};
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    [params enumerateKeysAndObjectsUsingBlock:^(id _Nonnull key, id _Nonnull obj, BOOL * _Nonnull stop) {
        [request setValue:obj forHTTPHeaderField:key];
    }];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    NSData *photoData = UIImageJPEGRepresentation(receivedPhoto.image, 0.8f);
    NSURLSessionDataTask *uploadTask = [session uploadTaskWithRequest:request fromData:photoData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        
        NSLog(@"Image Uplaod response - %@",responseString);//
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString * twitterKey = [defaults valueForKey:@"twitter"];
            NSString * FacebookKey = [defaults valueForKey:@"facebook"];

            if([twitterKey isEqualToString:@"Yes"])
            {
                self.image = receivedPhoto.image;
                
//                [HUD hide:YES];
//                [HUD removeFromSuperViewOnHide];
//                [self.navigationController popToRootViewControllerAnimated:true];

                [self tweetToTwitter:@"Travomi"];
            }

            else
            {
                if([FacebookKey isEqualToString:@"Yes"])
                {
                    self.image = receivedPhoto.image;
                    [HUD hide:YES];
                    [HUD removeFromSuperViewOnHide];
                    
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Travomi" message:@"Do you want to upload this Image on Facebook also?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
                    [alert show];
                    
                }
                else
                {
                    [HUD hide:YES];
                    [HUD removeFromSuperViewOnHide];
                    [self.navigationController popToRootViewControllerAnimated:true];
                }

                


            }
            
        });

        
    }];
    [uploadTask resume];
    
    
    }
    else
    {
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        
        HUD.mode = MBProgressHUDModeDeterminate;
        HUD.delegate = self;
        HUD.labelText = @"Uploading";
        [HUD show:YES];
        
        NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userdata"];
        NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
        NSString * UserID;
        
        if (dictionary) {
            
            GTLCustomerLoginCustomerProfile * dict = (GTLCustomerLoginCustomerProfile *) dictionary;
            UserID = [NSString stringWithFormat:@"%@",dict.cUID];
            
            NSLog(@"%@",dict.cUID);
            
        }
        
        NSLog(@"%@",HotelId);
        
        NSString *fileName = [NSString stringWithFormat:@"video/%.0f.mp4",[[NSDate date] timeIntervalSince1970] * 1000];
        
        NSString *urlString = @"https://travomidev.appspot.com/uploadFile";
        NSURL *url = [NSURL URLWithString:urlString];
        NSDictionary *params = @{@"filename":fileName,@"filetype":@"video/mp4",@"isVideo":@"0",@"userId":UserID,@"hotelId":HotelId,@"isEdit":@"1"};
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setHTTPMethod:@"POST"];

        
        [params enumerateKeysAndObjectsUsingBlock:^(id _Nonnull key, id _Nonnull obj, BOOL * _Nonnull stop) {
            [request setValue:obj forHTTPHeaderField:key];
        }];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
      //  NSData *photoData = UIImageJPEGRepresentation(receivedPhoto.image, 0.8f);
        
        NSData *videoData=video.videoData;
        
        
        NSURLSessionDataTask *uploadTask = [session uploadTaskWithRequest:request fromData:videoData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            
            NSLog(@"Image Uplaod response - %@",responseString);//
            
            [self PostThumbnailData:responseString];
            

            
            
        }];
        [uploadTask resume];
    }
    
    
    
    
//    NSLog(@"pressed POST");
//    if([type isEqual:@"camera"] || [type isEqual: @"existing"]){
//        //ProgressHUD spot
//        NSString *trimmedComment = [caption.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//        //add files and delegates and variables to the .h file
//        
//        HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        [self.view addSubview:HUD];
//        
//        // Set determinate mode
//        //hud to show uploading to server and so that no user interation will be called to break the app
//        HUD.mode = MBProgressHUDModeDeterminate;
//        HUD.delegate = self;
//        HUD.labelText = @"Uploading";
//        [HUD show:YES];
//        
//        // if logged into twitter also upload to twitter
//        if([[[PFUser currentUser] objectForKey:@"twitter"] isEqual: @YES]) {
//            [self tweetToTwitter:trimmedComment];
//        }
//
//        //make sure the caption deletes blank space before and after caption.
//        NSArray *words = [caption.text componentsSeparatedByString:@" " ];
//        [tags removeAllObjects];
//        for (NSString *word in words) {
//            if([word hasPrefix:@"#"]){
//                NSString *stringTime;
//                stringTime = [word substringFromIndex:1];
//                stringTime =[stringTime lowercaseStringWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]];
//                [tags addObject:stringTime];
//            }
//        }
//        
//        
//        // add the name of the hotel to tags here!
//        NSLog(@"what is the first ones text %@", self.hotel.titleLabel.text);
//        NSLog(@"what is the second ones text %@", self.pickerViewTextField2.text);
//        
//        
//        
//        NSArray *hoteltag = [hotelNames allKeysForObject:self.hotel.titleLabel.text];
//
//        if (hoteltag.count >= 1) {
//            [tags addObject:[hoteltag[0] lowercaseString]];
//        }
//        
//        
//        NSData *photoData = UIImageJPEGRepresentation(receivedPhoto.image, 0.8f);
//        PFFile *photoFile = [PFFile fileWithData:photoData];
//        
//        //catches if cant upload photo.
//        if (!photoFile) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo"
//                                                            message:nil
//                                                           delegate:nil
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"Dismiss",nil];
//            [alert show];
//            return;
//        }
//        
//
//        //parse calls to set up photo as well as upload photo.
//        PFObject *photo = [PFObject objectWithClassName:@"Photo"];
//        [photo setObject:[PFUser currentUser] forKey:@"creator"];
//        [photo setObject:photoFile forKey:@"image"];
//        [photo setObject:tags forKey:@"tags"];
//        [photo setValue:@1 forKey:@"active"];
//        
//        if (![islandLocation.titleLabel.text isEqualToString:@"Select Location"]) {
//            [photo setObject:islandLocation.titleLabel.text forKey:@"tempLocation"];
//        }
//        
//        if (trimmedComment.length != 0) {
//            [photo setObject:trimmedComment forKey:@"caption"];
//        }
//        
//        PFACL *photoACL = [PFACL ACLWithUser:[PFUser currentUser]];
//        [photoACL setPublicReadAccess:YES];
//        photo.ACL = photoACL;
//        
//        [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//            if (!error) {
//                
//                [HUD hide:YES];
//                
//                // show checkmark
//                
//                HUD = [[MBProgressHUD alloc] initWithView:self.view];
//                
//                [self.view addSubview:HUD];
//                
//        
//                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
//                
//                // Set custom view mode
//                HUD.mode = MBProgressHUDModeCustomView;
//                
//                HUD.delegate = self;
//                
//                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Posted Photo" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
//                [alert show];
//                
//            }
//            else {
//                [HUD hide:YES];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
//                [alert show];
//            }
//        
//         
//        }];
//        //adds all the hashtags to an array for the photo object on jaago
//        NSArray *uniquearray = [[NSSet setWithArray:tags] allObjects];
//        
//        NSLog(@"NewArray %@", uniquearray);
//        
//        for (NSString *word in uniquearray) {
//            PFQuery *query = [PFQuery queryWithClassName:@"Hashtag"];
//            [query whereKey:@"title" equalTo:word];
//            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                
//                if ([objects count]) {
//                    NSLog(@"Items");
//                    for(PFObject *object in objects){
//                        [query getObjectInBackgroundWithId:object.objectId block:^(PFObject *hashtag, NSError *error) {
//                            [hashtag[@"list"] addObject:photo];
//                            [hashtag saveInBackground];
//                        }];
//                    }
//                }
//                else {
//                    NSLog(@"No Items");
//                    PFObject *hashtags = [PFObject objectWithClassName:@"Hashtag"];
//                    [hashtags setObject:word forKey:@"title"];
//                    NSMutableArray *list = [[NSMutableArray alloc]init];
//                    [list addObject:photo];
//                    [hashtags setObject:list forKey:@"list"];
//                    [hashtags saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                        
//                        
//                    }];
//                    
//                }
//                
//            }];
//            
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
//
//        
//        //just printed out the tags to make sure it worked on the console.
//        
//        NSLog(@"%@", tags);
//    }else{
//        //ProgressHUD spot
//        NSString *trimmedComment = [caption.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//        //add files and delegates and variables to the .h file
//        
//        HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        [self.view addSubview:HUD];
//        
//        // Set determinate mode
//        //hud to show uploading to server and so that no user interation will be called to break the app
//        HUD.mode = MBProgressHUDModeDeterminate;
//        HUD.delegate = self;
//        HUD.labelText = @"Uploading";
//        [HUD show:YES];
//        
//        // if logged into twitter also upload to twitter
//        if([[[PFUser currentUser] objectForKey:@"twitter"] isEqual: @YES]) {
//            [self tweetToTwitter:trimmedComment];
//        }
//        
//        //make sure the caption deletes blank space before and after caption.
//        NSArray *words = [caption.text componentsSeparatedByString:@" " ];
//        [tags removeAllObjects];
//        for (NSString *word in words) {
//            if([word hasPrefix:@"#"]){
//                NSString *stringTime;
//                stringTime = [word substringFromIndex:1];
//                stringTime =[stringTime lowercaseStringWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]];
//                [tags addObject:stringTime];
//            }
//        }
//        
//        // add tag for hotel here!
//        
//        
//        
//        NSData *thumbData = UIImageJPEGRepresentation(video.videoThumb, 0.8f);
//        PFFile *thumbFile = [PFFile fileWithData:thumbData];
//        NSData *videoData=video.videoData;
//        PFFile *videoFile= [PFFile fileWithData:videoData];
//        
//        //catches if cant upload photo.
//        if (!thumbFile || !videoFile) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your video, because they cannot be parsed to files"
//                                                            message:nil
//                                                           delegate:nil
//                                                  cancelButtonTitle:nil
//                                                  otherButtonTitles:@"Dismiss",nil];
//            [alert show];
//            return;
//        }
//        
//        
//        //parse calls to set up photo as well as upload photo.
//        PFObject *thumb = [PFObject objectWithClassName:@"Photo"];
//        
//        
//        
//        [thumb setObject:[PFUser currentUser] forKey:@"creator"];
//        [thumb setObject:thumbFile forKey:@"image"];
//        [thumb setObject:tags forKey:@"tags"];
//        [thumb setObject:videoFile forKey:@"video"];
//        [thumb setValue:@1 forKey:@"active"];
//        
//        if (![islandLocation.titleLabel.text isEqualToString:@"Select Location"]) {
//            [thumb setObject:islandLocation.titleLabel.text forKey:@"tempLocation"];
//        }
//        
//        if (trimmedComment.length != 0) {
//            [thumb setObject:trimmedComment forKey:@"caption"];
//        }
//        
//        PFACL *thumbACL = [PFACL ACLWithUser:[PFUser currentUser]];
//        [thumbACL setPublicReadAccess:YES];
//        thumb.ACL = thumbACL;
//        
//        [thumb saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//            if (!error) {
//                
//                [HUD hide:YES];
//                
//                // show checkmark
//                
//                HUD = [[MBProgressHUD alloc] initWithView:self.view];
//                
//                [self.view addSubview:HUD];
//                
//                
//                HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
//                
//                // Set custom view mode
//                HUD.mode = MBProgressHUDModeCustomView;
//                
//                HUD.delegate = self;
//                
//                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Posted Photo" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okay", nil];
//                [alert show];
//                
//                
//                
//            }
//            else {
//                [HUD hide:YES];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Couldn't post your photo" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
//                [alert show];
//            }
//            
//            
//        }];
//        //adds all the hashtags to an array for the photo object on jaago
//        NSArray *uniquearray = [[NSSet setWithArray:tags] allObjects];
//        
//        NSLog(@"NewArray %@", uniquearray);
//        
//        for (NSString *word in uniquearray) {
//            PFQuery *query = [PFQuery queryWithClassName:@"Hashtag"];
//            [query whereKey:@"title" equalTo:word];
//            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//                
//                if ([objects count]) {
//                    NSLog(@"Items");
//                    for(PFObject *object in objects){
//                        [query getObjectInBackgroundWithId:object.objectId block:^(PFObject *hashtag, NSError *error) {
//                            [hashtag[@"list"] addObject:thumb];
//                            [hashtag saveInBackground];
//                        }];
//                    }
//                }
//                else {
//                    NSLog(@"No Items");
//                    PFObject *hashtags = [PFObject objectWithClassName:@"Hashtag"];
//                    [hashtags setObject:word forKey:@"title"];
//                    NSMutableArray *list = [[NSMutableArray alloc]init];
//                    [list addObject:thumb];
//                    [hashtags setObject:list forKey:@"list"];
//                    [hashtags saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                        
//                        
//                    }];
//                    
//                }
//                
//            }];
//            
//            [self dismissViewControllerAnimated:YES completion:nil];
//        }
//        
//        
//        //just printed out the tags to make sure it worked on the console.
//        
//        NSLog(@"%@", tags);
//    }
    
    
}

-(void)PostThumbnailData:(NSString *)VideoId
{

    
    NSString *fileName = [NSString stringWithFormat:@"images/%.0f.png",[[NSDate date] timeIntervalSince1970] * 1000];
    
    NSString *urlString = @"https://travomidev.appspot.com/uploadFile";
    NSURL *url = [NSURL URLWithString:urlString];
    NSDictionary *params = @{@"filename":fileName,@"filetype":@"image/png",@"vedioId":VideoId,@"isEdit":@"0"};
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    
    [params enumerateKeysAndObjectsUsingBlock:^(id _Nonnull key, id _Nonnull obj, BOOL * _Nonnull stop) {
        [request setValue:obj forHTTPHeaderField:key];
    }];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:nil delegateQueue:nil];
    //  NSData *photoData = UIImageJPEGRepresentation(receivedPhoto.image, 0.8f);
    
    NSData *thumbData = UIImageJPEGRepresentation(video.videoThumb, 0.8f);
    
    
    
    NSURLSessionDataTask *uploadTask = [session uploadTaskWithRequest:request fromData:thumbData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSLog(@"%@",responseString);
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString * twitterKey = [defaults valueForKey:@"twitter"];
            if([twitterKey isEqualToString:@"Yes"])
            {
                self.image = video.videoThumb;
                [self tweetToTwitter:@"Travomi"];
            }
            else
            {
                NSString * FacebookKey = [defaults valueForKey:@"facebook"];
                if([FacebookKey isEqualToString:@"Yes"])
                {
                    self.image = video.videoThumb;
                    [HUD hide:YES];
                    [HUD removeFromSuperViewOnHide];
                    
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Travomi" message:@"Do you want to upload this Image on Facebook also?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
                    [alert show];
                    
                }
                else
                {
                    [HUD hide:YES];
                    [HUD removeFromSuperViewOnHide];
                    [self.navigationController popToRootViewControllerAnimated:true];
                }
            }
            
            

            

            
            
            [self.navigationController popToRootViewControllerAnimated:true];
        });
    
        
    }];
    [uploadTask resume];
    
    
    
    
    
 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)
    {

        [self fbButton];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:true];

    }
    
    
//    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//    
//    
//    if([title isEqualToString:@"GO"]) {
//        
//    }
//    
//    if([title isEqualToString:@"Okay"])
//    {
//        NSLog(@"Posted Photo was selected.");
//        
//        //[self performSegueWithIdentifier:@"PostToLanding" sender:self];
//
//        [[self.tabBarController.viewControllers objectAtIndex:0] popToRootViewControllerAnimated:NO];
//        [self.tabBarController setSelectedIndex: 0];
//        [[self.tabBarController.viewControllers objectAtIndex:2] popToRootViewControllerAnimated:NO];
//        
//        NSLog(@"Finished Popping");
//        
//    }
}

#pragma mark MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
    // Remove HUD from screen when the HUD hides
    [HUD removeFromSuperview];
	HUD = nil;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
        // Get the new view controller using [segue destinationViewController].
        HomePageViewController *controller = [segue destinationViewController];
        
        controller.islandPicked = islandLocation.titleLabel.text;
    
    
    //after posting the picture goes back to the appropriate island that was chosen to actually see the photo uploaded.
        if ([islandLocation.titleLabel.text isEqualToString: @"Hawaii"]) {
            controller.islandPicked = islandLocation.titleLabel.text;
            controller.islandHotels = hawaiiPicked;
            
            controller.navigationImage = [UIImage imageNamed:@"Hawaii.jpeg"];
        }
        else if ([islandLocation.titleLabel.text isEqualToString: @"Kauai"]) {
            controller.islandPicked = islandLocation.titleLabel.text;
            controller.islandHotels = kauaiPicked;
            
            controller.navigationImage = [UIImage imageNamed:@"Kauai.jpg"];
            
        }
        else if ([islandLocation.titleLabel.text isEqualToString: @"Maui"]) {
            controller.islandPicked = islandLocation.titleLabel.text;
            controller.islandHotels = mauiPicked;
            
            controller.navigationImage = [UIImage imageNamed:@"maui.jpg"];
            
        }
        else if ([islandLocation.titleLabel.text isEqualToString: @"Oahu"]) {
            controller.islandPicked = islandLocation.titleLabel.text;
            controller.islandHotels = oahuPicked;
            
            controller.navigationImage = [UIImage imageNamed:@"Oahu.jpg"];
            
        }
        else {
            
        }
        
    
    

}



- (IBAction)selectLocationButton:(UIButton *)sender {
    [self.pickerViewTextField becomeFirstResponder];
}

//new

- (IBAction)selectHotelButton:(UIButton *)sender {
    [self.pickerViewTextField2 becomeFirstResponder];
}


//bar button methods

- (void)cancelTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [self.pickerViewTextField resignFirstResponder];
    [self.pickerViewTextField2 resignFirstResponder];
}

- (void)doneTouched:(UIBarButtonItem *)sender
{
    
    if([HotelId isEqualToString:@""] || HotelId == nil)
    {
    HotelId = [hotelId objectAtIndex:0];
    
    [TaggedLbl setText:[NSString stringWithFormat:@"Tagged : %@",[islands objectAtIndex:0]]];
    [TagHotelBtn setTitle:@"Change Tag" forState:UIControlStateNormal];
    }
    
    // hide the picker view
    [self.pickerViewTextField resignFirstResponder];
    [self.pickerViewTextField2 resignFirstResponder];
    
    

    
    // perform some action
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    return [islands count];
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [islands objectAtIndex:row];

}


// code to actually tweet to twitter 
- (void)tweetToTwitter:(NSString *)stringToTweet {
    ACAccountType *twitterType =
    [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    SLRequestHandler requestHandler =
    ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData) {
            NSInteger statusCode = urlResponse.statusCode;
            if (statusCode >= 200 && statusCode < 300) {
                NSDictionary *postResponseData =
                [NSJSONSerialization JSONObjectWithData:responseData
                                                options:NSJSONReadingMutableContainers
                                                  error:NULL];
                NSLog(@"[SUCCESS!] Created Tweet with ID: %@", postResponseData[@"id_str"]);
            }
            else {
                NSLog(@"[ERROR] Server responded: status code %ld %@", (long)statusCode,
                      [NSHTTPURLResponse localizedStringForStatusCode:statusCode]);
            }
        }
        else {
            NSLog(@"[ERROR] An error occurred while posting: %@", [error localizedDescription]);
        }
    };
    
    ACAccountStoreRequestAccessCompletionHandler accountStoreHandler =
    ^(BOOL granted, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted) {
                NSArray *accounts = [self.accountStore accountsWithAccountType:twitterType];
                
                
                //added code
                NSLog(@"accounts %@", accounts);
                if ([accounts count] == 0) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Log onto Twitter" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"GO", nil];
                    [alert show];
                    return;
                    
                    
                    
                    //go get oauth shit
                }
                
                //end of added code
                
                NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                              @"/1.1/statuses/update_with_media.json"];
                NSDictionary *params = @{@"status" :stringToTweet};
                SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                        requestMethod:SLRequestMethodPOST
                                                                  URL:url
                                                           parameters:params];
                NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
                [request addMultipartData:imageData
                                 withName:@"media[]"
                                     type:@"image/jpeg"
                                 filename:@"image.jpg"];
                
                NSLog(@"last object in account %@", [accounts lastObject]);
                
                
                [request setAccount:[accounts lastObject]];
                [request performRequestWithHandler:requestHandler];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSString * FacebookKey = [defaults valueForKey:@"facebook"];
                if([FacebookKey isEqualToString:@"Yes"])
                {
                    [HUD hide:YES];
                    [HUD removeFromSuperViewOnHide];
                   
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Travomi" message:@"Do you want to upload this Image on Facebook also?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
                    [alert show];
                }
                else
                {
                    [HUD hide:YES];
                    [HUD removeFromSuperViewOnHide];
                    [self.navigationController popToRootViewControllerAnimated:true];
                }
                
                
                

                
            }
            else {
                NSLog(@"[ERROR] An error occurred while asking for user authorization: %@",
                      [error localizedDescription]);
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Grant Jaago Twitter Info" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"GO", nil];
                [alert show];
                
//                [HUD hide:YES];
//                [HUD removeFromSuperViewOnHide];
//                [self.navigationController popToRootViewControllerAnimated:true];
                //GO GRANT INFO
            }
        });
    };
    
    [self.accountStore requestAccessToAccountsWithType:twitterType
                                               options:NULL
                                            completion:accountStoreHandler];

}

-(void)fbButton {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController * fbSLComposeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [fbSLComposeViewController addImage:self.image];
        [fbSLComposeViewController setInitialText:@"Travomi"];
        [self presentViewController:fbSLComposeViewController animated:YES completion:nil];
        
        fbSLComposeViewController.completionHandler = ^(SLComposeViewControllerResult result) {
            switch(result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"facebook: CANCELLED");
                    [self.navigationController popToRootViewControllerAnimated:true];

                    break;
                case SLComposeViewControllerResultDone:
                    NSLog(@"facebook: SHARED");
                    [self.navigationController popToRootViewControllerAnimated:true];

                    break;
            }
        };
    }
    else {
        UIAlertView *fbError = [[UIAlertView alloc] initWithTitle:@"Facebook Unavailable" message:@"Sorry, we're unable to find a Facebook account on your device.\nPlease setup an account in your devices settings and try again." delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [fbError show];
        [self.navigationController popToRootViewControllerAnimated:true];

    }
}

- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    HotelId = [hotelId objectAtIndex:row];
    
    [TaggedLbl setText:[NSString stringWithFormat:@"Tagged : %@",[islands objectAtIndex:row]]];
    [TagHotelBtn setTitle:@"Change Tag" forState:UIControlStateNormal];
    
}


@end
