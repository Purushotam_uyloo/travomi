//
//  ProfileCell.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/13/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "ProfileCell.h"

@implementation ProfileCell

@synthesize number, mainText, image;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
