//
//  ForgotPasswordViewController.h
//  Travomi
//
//  Created by Kumar, Gopesh on 10/9/16.
//  Copyright © 2016 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ForgotPasswordViewController : UIViewController<MBProgressHUDDelegate>
{
     MBProgressHUD * HUD;
}

@end
