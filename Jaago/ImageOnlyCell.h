//
//  ImageOnlyCell.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 10/8/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageOnlyCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) IBOutlet UILabel *testLabel;

@end
