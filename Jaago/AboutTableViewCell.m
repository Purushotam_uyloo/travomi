//
//  AboutTableViewCell.m
//  Travomi
//
//  Created by admin on 2/4/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "AboutTableViewCell.h"

@implementation AboutTableViewCell
@synthesize HotelNameLbl,HotelDescLbl;
@synthesize backImgview;
@synthesize Morebtn;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    backImgview.backgroundColor = [UIColor whiteColor];
    backImgview.layer.masksToBounds = NO;
    backImgview.layer.shadowColor = [UIColor blackColor].CGColor;
    backImgview.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    backImgview.layer.shadowOpacity = 0.5f;
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
