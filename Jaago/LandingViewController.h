//
//  LandingViewController.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/9/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>



#import "ImageViewCell4.h"
#import "LandingPageResultViewController.h"
#import "ImageReviewViewController.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "YoutubeViewController.h"
#import "VineVideoModel.h"

#import "MGBox.h"
#import "MGScrollView.h"
#import <ParseUI/ParseUI.h>

#import "JSONModelLib.h"
#import "VideoModel.h"

#import "PhotoBox.h"
#import "WebVideoViewController.h"

// instagram clientID
#define KCLIENTID @"745cbb1a78f9401fa4d8cc3a5713a32c"
#import <MobileCoreServices/UTCoreTypes.h>
#import "ImageProperties.h"



@interface LandingViewController : UIViewController < PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationBarDelegate, UIAlertViewDelegate, UISearchBarDelegate>
{
    NSMutableData * responseData;
}

@property NSString *islandPicked;
@property NSMutableArray *islandHotels;
@property UIImage *navigationImage;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;


@property (strong, nonatomic) IBOutlet UITableView *realTableView;

@property (strong, nonatomic) IBOutlet UITableView *searchHotelsTableView;

//@property (strong, nonatomic) IBOutlet UILabel *hotelTitleLabel;

@property (strong, nonatomic) NSMutableArray *images;
@property (strong, nonatomic) IBOutlet UISearchBar *theSearchBar;

@end
