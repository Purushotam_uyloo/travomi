//
//  HotelDetailsViewController.h
//  Jaago
//
//  Created by WangYinghao on 3/9/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface HotelDetailsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,// PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate,
UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate>
{
    NSMutableData *responseData;
}

@property NSString *islandPicked;
@property NSArray *islandHotels;
@property UIImage *navigationImage;


@property (strong, nonatomic) IBOutlet UITableView *realTableView;
//@property (strong, nonatomic) IBOutlet UITableView *searchHotelsTableView;
@property (strong, nonatomic) IBOutlet UILabel *hotelTitleLabel;

@property (strong, nonatomic) NSMutableArray *images;
- (IBAction)logoutButtonPressed:(id)sender;
- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)profileButtonPressed:(id)sender;


@end


