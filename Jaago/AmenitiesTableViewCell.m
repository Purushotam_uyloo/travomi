//
//  AmenitiesTableViewCell.m
//  Travomi
//
//  Created by admin on 2/5/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "AmenitiesTableViewCell.h"
#import "GTLHotelEndpointAmenity.h"

@implementation AmenitiesTableViewCell
@synthesize HotelNameLbl;
@synthesize Morebtn;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    backImgview.backgroundColor = [UIColor whiteColor];
    backImgview.layer.masksToBounds = NO;
    backImgview.layer.shadowColor = [UIColor blackColor].CGColor;
    backImgview.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    backImgview.layer.shadowOpacity = 0.5f;
    
    // Initialization code
}
-(void)initwithAmenitiesArray:(NSArray *)AmenitiesArr
{
    AmenitiesArray = AmenitiesArr;
    
    NSLog(@"%@",AmenitiesArray);
    
  // CGFloat  Screenwidth = [UIScreen mainScreen].bounds.size.width;
  // CGFloat  Screenheight = [UIScreen mainScreen].bounds.size.height;
    
    CGFloat Innerwidth = backImgview.frame.size.width;
    
    CGFloat xcoordinate  = 0;
    CGFloat ycoordinate  = 0;
    
    CGFloat gap  = 38;
    
    CGFloat PropertyWidth   = 28;
    
    gap = (Innerwidth - (PropertyWidth * 5))/4;
    
    int totalcount = 5;
    
    [[InnerView subviews]
     makeObjectsPerformSelector:@selector(removeFromSuperview)];

    
    for(int i =0 ; i < AmenitiesArray.count ; i++ )
    {
        
        GTLHotelEndpointAmenity * wrapper = (GTLHotelEndpointAmenity *)[AmenitiesArray objectAtIndex:i];
        
        UIImageView * ProprtyTypeImg = [[UIImageView alloc]initWithFrame:CGRectMake(xcoordinate, ycoordinate, PropertyWidth, PropertyWidth)];
        
        [ProprtyTypeImg setImage:[UIImage imageNamed:wrapper.name]];
        [ProprtyTypeImg setBackgroundColor:[UIColor clearColor]];
        [ProprtyTypeImg setContentMode:UIViewContentModeScaleAspectFit];
        [InnerView addSubview:ProprtyTypeImg];
        
        
        UILabel * namelbl = [[UILabel alloc]init];
        [namelbl setFrame:CGRectMake(xcoordinate, ycoordinate + 30, PropertyWidth, 14)];
        [namelbl setText:wrapper.name];
        [namelbl setTextAlignment:NSTextAlignmentCenter];
        [namelbl setFont:[UIFont fontWithName:@"Helvetica" size:9.0]];
        [namelbl setNumberOfLines:2];
        [namelbl setTextColor:[UIColor blackColor]];
        [InnerView addSubview:namelbl];

    
        xcoordinate = xcoordinate + PropertyWidth + gap;
    }
    
    
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
