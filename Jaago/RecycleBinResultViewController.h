//
//  RecycleBinResultViewController.h
//  Jaago
//
//  Created by WangYinghao on 2/24/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProperties.h"


@interface RecycleBinResultViewController : UIViewController<UINavigationControllerDelegate, UITextFieldDelegate>

@property UIImage *selectedImage;
@property NSString *selectedImageCaption;
@property NSInteger number;
@property ImageProperties *selectedImageProperties;



@property (strong, nonatomic) IBOutlet UIImageView *picture;
@property (strong, nonatomic) IBOutlet UITextView *caption;
- (IBAction)removePhoto:(UIButton *)sender;
- (IBAction)restorePhoto:(UIButton *)sender;


@end
