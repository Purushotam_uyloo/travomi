//
//  RootNavigationController.m
//  Travomi
//
//  Created by WangYinghao on 8/26/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "RootNavigationController.h"

@interface RootNavigationController ()

@end

@implementation RootNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString * plistFilePath = [[NSBundle mainBundle] pathForResource: @"HotelNamesDictionary" ofType: @"plist"];
    self.hotelNames = [NSDictionary dictionaryWithContentsOfFile:plistFilePath];
    NSLog(@"hotelnames dictionary: %@",[self.hotelNames description]);
}


@end
