//
//  ReportSpam.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 11/15/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSpam : UIViewController
- (IBAction)itemPressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;

@end
