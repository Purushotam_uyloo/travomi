//
//  LandingViewController.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/9/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "LandingViewController.h"
#import "RootNavigationController.h"



#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"

@interface LandingViewController ()

@property UIRefreshControl * rc;

@end

@implementation LandingViewController
{
    //variables used for this controller
    
    NSMutableArray *propertiesArray;
    // tempArray is to allow to add to the full propertiesArray during each cycle
    NSMutableArray *tempArray;
    NSMutableArray *listOfTempArray;
    NSMutableArray *itemOfMainArray;
    NSMutableOrderedSet *locationArray;
    BOOL defaultWord;
    BOOL locationBOOL;
    int numberOfLocations;
    
    BOOL endSearch;
    int ndxCount;
    int arrayCountSub;
    NSString *hashtagWord;
    NSArray* videos;
    NSArray *allHotels;
    NSArray *allEnglishHotels;
    NSDictionary *hotelNames;

}

@synthesize images, realTableView, islandHotels, islandPicked, navigationImage, theSearchBar, searchHotelsTableView, searchButton;

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    /*PFUser *test = [PFUser currentUser];
    NSLog(@"%@", test);
    if (!test) { // No user logged in
        
        [self performSegueWithIdentifier:@"customLogIn" sender:self];
        
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];

        
        
        
        //[self performSegueWithIdentifier:@"NormaltoLoginView" sender:self];
        
        
    }
    else {
        if(![[test objectForKey:@"emailVerified"] boolValue]){
            [test refresh];
            if(![[test objectForKey:@"emailVerified"] boolValue]){
                [PFUser logOut];
                [self viewDidAppear:NO];
                [[[UIAlertView alloc] initWithTitle:@"Please Verify Email" message:@"Check your email to Verify your Email Address before continuing" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show ];
            }
            
        }
        /*if (![[test objectForKey:@"firstTime"] boolValue] && [[test objectForKey:@"testing"]boolValue]) {
            NSLog(@"Nothing here");
            [self performSegueWithIdentifier:@"InitialSocialMedia" sender:self];
            
            
        }*/
       /* else {
            NSLog(@"DO NOTHING");
            
            
        }
    
        
        //[self dismiss:logInViewController];
    }*/
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Montserrat-Regular" size:15.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];

    self.navigationController.navigationBar.titleTextAttributes = size;

    RootNavigationController * rootNVC = (RootNavigationController *)self.navigationController;
    hotelNames = rootNVC.hotelNames;

    UITabBar *tabBar = self.tabBarController.tabBar;
    [self.tabBarController.tabBar setBarTintColor: [UIColor blackColor]];
    
    
    
    UITabBarItem *item0 = [tabBar.items objectAtIndex:0];
//    UITabBarItem *item1 = [tabBar.items objectAtIndex:1];
    UITabBarItem *item2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *item3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *item4 = [tabBar.items objectAtIndex:3];
    

    UIImage* unselectedImage0 = [[UIImage imageNamed:@"home_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage0 = [[UIImage imageNamed:@"home_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIImage* unselectedImage1 = [[UIImage imageNamed:@"islands_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UIImage* selectedImage1 = [[UIImage imageNamed:@"islands_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* unselectedImage2 = [[UIImage imageNamed:@"camera_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage2 = [[UIImage imageNamed:@"camera_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* unselectedImage3 = [[UIImage imageNamed:@"user_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage3 = [[UIImage imageNamed:@"user_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* unselectedImage4 = [[UIImage imageNamed:@"settings_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIImage* selectedImage4 = [[UIImage imageNamed:@"settings_pressed.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item0.selectedImage = selectedImage0;
    item0.image = unselectedImage0;
    item0.imageInsets = UIEdgeInsetsMake(6, -3, -6, 3);
    item0.title = @"";
    

//    item1.selectedImage = selectedImage1;
//    item1.image = unselectedImage1;
//    item1.imageInsets = UIEdgeInsetsMake(6, -8, -6, 8);
//    item1.title = @"";


    item2.selectedImage = selectedImage2;
    item2.image = unselectedImage2;
    item2.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    item2.title = @"";


    item3.selectedImage = selectedImage3;
    item3.image = unselectedImage3;
    item3.imageInsets = UIEdgeInsetsMake(6, 8, -6, -8);
    item3.title = @"";


    item4.selectedImage = selectedImage4;
    item4.image = unselectedImage4;
    item4.imageInsets = UIEdgeInsetsMake(6, 3, -6, -3);
    item4.title = @"";


    self.rc = [[UIRefreshControl alloc]init];
    [self.rc addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    [self.realTableView addSubview:self.rc];
    
    
    
    NSLog(@"current name: %@",self.title);
    islandPicked = @"Maunalanibayhotel";
    self.theSearchBar.hidden=YES;
    searchHotelsTableView.hidden = YES;
    //hotelTitleLabel.hidden=NO;
    NSLog(@"islandHotels: %@, islandPicked = %@", islandHotels, islandPicked);
    
    UIImage *image = [UIImage imageNamed:@"hlogo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    //self.title=[NSString stringWithFormat:islandPicked];
    //hotelTitleLabel.text = [NSString stringWithFormat:@"This is where the description for %@ goes",islandPicked];

    NSCharacterSet *newlineCharSet = [NSCharacterSet newlineCharacterSet];
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"HashtagHotels" ofType:@"txt"];
    NSString* fileContents = [NSString stringWithContentsOfFile:filePath
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    
    
    allHotels = [[NSArray alloc]init];
    allHotels = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    NSString* filePath2 = [[NSBundle mainBundle] pathForResource:@"EnglishHotelTranslations" ofType:@"txt"];
    NSString* fileContents2 = [NSString stringWithContentsOfFile:filePath2
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    
    
    allEnglishHotels = [[NSArray alloc]init];
    allEnglishHotels = [fileContents2 componentsSeparatedByCharactersInSet:newlineCharSet];

    int randomHotelNumber = arc4random_uniform((u_int32_t)[allHotels count]);
    NSLog(@"randomNUMBER %d", randomHotelNumber);
    
    islandPicked = [allHotels objectAtIndex: randomHotelNumber];
    NSLog(@"islandPicked, %@", islandPicked);
    islandHotels = [NSMutableArray arrayWithObject:islandPicked];
    [islandHotels addObjectsFromArray:allHotels];
    

    defaultWord = YES;
    locationArray = NO;
    endSearch = NO;
    propertiesArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableOrderedSet alloc]init];
    if(islandPicked) {
        self.title = [hotelNames objectForKey:islandPicked];
    }
    listOfTempArray = [[NSMutableArray alloc] init]; // array no - 1
    itemOfMainArray = [[NSMutableArray alloc] init];
    [itemOfMainArray addObjectsFromArray:allEnglishHotels];
    
    ndxCount = 0;
    arrayCountSub = 0;
    // adds an IMageproperty to an array to eventually display on the table.
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    
    [photoObjectQuery whereKey:@"tempLocation" equalTo:islandPicked];
    [photoObjectQuery whereKey:@"active" equalTo:@1];
    [photoObjectQuery orderByDescending:@"createdAt"];
    
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                ImageProperties *property = [[ImageProperties alloc] init];
                property.id = [object objectId];
                property.instagramPic = NO;
                //NSLog(@"found a match!");
                property.travomiPost = true;
                [propertiesArray addObject:property];
                
            }
            [self getInstagramFeed];
            [self getTwitterFeed];
            //[self getYoutubeFeed];
            [self getVineFeed];
            
        }
        else {
            [self getInstagramFeed];
            [self getTwitterFeed];
            //[self getYoutubeFeed];
            [self getVineFeed];
            
        }
    }];
    
    item0.title = @"";

    
}

- (void) refreshTableView{
    
    
    
    
    NSLog(@"current name: %@",self.title);
    islandPicked = @"Maunalanibayhotel";
    self.theSearchBar.hidden=YES;
    searchHotelsTableView.hidden = YES;
    //hotelTitleLabel.hidden=NO;
    NSLog(@"islandHotels: %@, islandPicked = %@", islandHotels, islandPicked);
    
    UIImage *image = [UIImage imageNamed:@"hlogo.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    //self.title=[NSString stringWithFormat:islandPicked];
    //hotelTitleLabel.text = [NSString stringWithFormat:@"This is where the description for %@ goes",islandPicked];
    
    NSCharacterSet *newlineCharSet = [NSCharacterSet newlineCharacterSet];
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"HashtagHotels" ofType:@"txt"];
    NSString* fileContents = [NSString stringWithContentsOfFile:filePath
                                                       encoding:NSUTF8StringEncoding
                                                          error:nil];
    
    
    allHotels = [[NSArray alloc]init];
    allHotels = [fileContents componentsSeparatedByCharactersInSet:newlineCharSet];
    
    NSString* filePath2 = [[NSBundle mainBundle] pathForResource:@"EnglishHotelTranslations" ofType:@"txt"];
    NSString* fileContents2 = [NSString stringWithContentsOfFile:filePath2
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];
    
    
    allEnglishHotels = [[NSArray alloc]init];
    allEnglishHotels = [fileContents2 componentsSeparatedByCharactersInSet:newlineCharSet];
    
    int randomHotelNumber = arc4random_uniform((u_int32_t)[allHotels count]);
    NSLog(@"randomNUMBER %d", randomHotelNumber);
    
    islandPicked = [allHotels objectAtIndex: randomHotelNumber];
    NSLog(@"islandPicked, %@", islandPicked);
    islandHotels = [NSMutableArray arrayWithObject:islandPicked];
    [islandHotels addObjectsFromArray:allHotels];
    
    
    defaultWord = YES;
    locationArray = NO;
    endSearch = NO;
    propertiesArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableOrderedSet alloc]init];
    if(islandPicked) {
        self.title = [hotelNames objectForKey:islandPicked];
    }
    listOfTempArray = [[NSMutableArray alloc] init]; // array no - 1
    itemOfMainArray = [[NSMutableArray alloc] init];
    [itemOfMainArray addObjectsFromArray:allEnglishHotels];
    
    ndxCount = 0;
    arrayCountSub = 0;
    // adds an IMageproperty to an array to eventually display on the table.
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    
    [photoObjectQuery whereKey:@"tempLocation" equalTo:islandPicked];
    [photoObjectQuery whereKey:@"active" equalTo:@1];
    [photoObjectQuery orderByDescending:@"createdAt"];
    
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                ImageProperties *property = [[ImageProperties alloc] init];
                property.id = [object objectId];
                property.instagramPic = NO;
                //NSLog(@"found a match!");
                property.travomiPost = true;
                [propertiesArray addObject:property];
                
            }
            [self getInstagramFeed];
            [self getTwitterFeed];
            //[self getYoutubeFeed];
            [self getVineFeed];
            [self.rc endRefreshing];
            [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:@""];
            
        }
        else {
            [self getInstagramFeed];
            [self getTwitterFeed];
            //[self getYoutubeFeed];
            [self getVineFeed];
            [self.rc endRefreshing];
            [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:@""];

        }
    }];

}

- (void) getVineFeed{
    hashtagWord = [islandHotels objectAtIndex:ndxCount];
    if (ndxCount == ([islandHotels count]-1)) {
        endSearch = YES;
    }
    //URL escape the term
    NSString* term = [hashtagWord stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //term=@"Hawaii";
    //make HTTP call
    NSString* searchCall = [NSString stringWithFormat:@"https://api.vineapp.com/timelines/tags/%@", term];
    
    [JSONHTTPClient getJSONFromURLWithString: searchCall
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      
                                      //got JSON back
                                      //NSLog(@"Got JSON from web: %@", json);
                                      
                                      if (err) {
                                          [[[UIAlertView alloc] initWithTitle:@"Error"
                                                                      message:[err localizedDescription]
                                                                     delegate:nil
                                                            cancelButtonTitle:@"Close"
                                                            otherButtonTitles: nil] show];
                                          return;
                                      }
                                      
                                      //initialize the models
                                      videos = json[@"data"][@"records"];
                                      
                                      if (videos) NSLog(@"Loaded successfully vine models");
                                      
                                      //add the thumbnails
                                      ImageProperties *properties;
                                      tempArray = [[NSMutableArray alloc]init];
                                      for (int i=0;i<videos.count;i++) {
                                          
                                          //get the data
                                          NSDictionary* video = videos[i];
                                          VineVideoModel *vinev=[[VineVideoModel alloc] init];
                                          vinev.title=video[@"description"];
                                          vinev.videoURL=[NSURL URLWithString:video[@"videoUrl"]];
                                          vinev.videoThumb=[NSURL URLWithString:video[@"thumbnailUrl"]];
                                          
                                          
                                          //create a property
                                          properties.videoType=@"Vine";
                                          properties = [[ImageProperties alloc] init];
                                          properties.url = vinev.videoThumb;
                                          properties.instagramPic = YES;
                                          properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
                                          properties.user = @"VineUSER";
                                          properties.vineVideo = vinev;
                                          properties.isVideo=YES;
                                          properties.travomiPost = NO;
                                          [tempArray addObject:properties];
                                          
                                      }
                                      //NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                                      [propertiesArray addObjectsFromArray:tempArray];
                                      [realTableView reloadData];
                                  }];
    
}


- (void) getYoutubeFeed{
    hashtagWord = [islandHotels objectAtIndex:ndxCount];
    if (ndxCount == ([islandHotels count]-1)) {
        endSearch = YES;
    }
    [self searchYoutubeVideosForTerm:hashtagWord];
}

-(void)searchYoutubeVideosForTerm:(NSString*)term
{
    //NSLog(@"Searching for '%@' ...", term);
    
    //URL escape the term
    term = [term stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //make HTTP call
    NSString* searchCall = [NSString stringWithFormat:@"http://gdata.youtube.com/feeds/api/videos?q=%@&max-results=50&alt=json", term];
    
    [JSONHTTPClient getJSONFromURLWithString: searchCall
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      
                                      //got JSON back
                                      //NSLog(@"Got JSON from web: %@", json);
                                      
                                      if (err) {
                                          NSLog(@"Network Error!");
                                          return;
                                      }
                                      
                                      //initialize the models
                                      videos = [VideoModel arrayOfModelsFromDictionaries:
                                                json[@"feed"][@"entry"]
                                                ];
                                      
                                      //if (videos) NSLog(@"Loaded successfully models");
                                      
                                      //add the thumbnails
                                      ImageProperties *properties;
                                      tempArray = [[NSMutableArray alloc]init];
                                      for (int i=0;i<videos.count;i++) {
                                          
                                          //get the data
                                          VideoModel* video = videos[i];
                                          MediaThumbnail* thumb = video.thumbnail[0];
                                          
                                          //create a property
                                          properties.videoType=@"Youtube";
                                          properties = [[ImageProperties alloc] init];
                                          properties.url = thumb.url;
                                          properties.instagramPic = YES;
                                          properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
                                          properties.user = @"YoutubeUSER";
                                          properties.videos = video;
                                          properties.isVideo=YES;
                                          properties.travomiPost = NO;

                                          [tempArray addObject:properties];
                                          
                                      }
                                      //NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                                      [propertiesArray addObjectsFromArray:tempArray];
                                      [realTableView reloadData];
                                  }];
}

- (void) getTwitterFeed{
    hashtagWord = [islandHotels objectAtIndex:ndxCount];
    if (ndxCount == ([islandHotels count]-1)) {
        endSearch = YES;
    }
    //handle network requests here
    [[Twitter sharedInstance] logInGuestWithCompletion:^
     (TWTRGuestSession *session, NSError *error) {
         if (session) {
             // make API calls that do not require user auth
             //NSLog(@"TWitter logged in");
             NSString *statusesShowEndpoint = [NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json?q=%@",hashtagWord];
             //NSLog(@"Twitter hashtagWord=%@",hashtagWord);
             NSDictionary *params = @{};
             NSError *clientError;
             NSURLRequest *request = [[[Twitter sharedInstance] APIClient]
                                      URLRequestWithMethod:@"GET"
                                      URL:statusesShowEndpoint
                                      parameters:params
                                      error:&clientError];
             
             if (request) {
                 [[[Twitter sharedInstance] APIClient]
                  sendTwitterRequest:request
                  completion:^(NSURLResponse *response,
                               NSData *data,
                               NSError *connectionError) {
                      if (data) {
                          // handle the response data e.g.
                          NSError *jsonError;
                          NSDictionary *json = [NSJSONSerialization
                                                JSONObjectWithData:data
                                                options:0
                                                error:&jsonError];
                          //NSLog(@"twitter json :::: %@", json);
                          ImageProperties *properties;
                          tempArray = [[NSMutableArray alloc]init];
                          
                          NSArray *statuses=[json objectForKey:@"statuses"];
                          for (NSDictionary *status in statuses) {
                              NSDictionary * entity=[status valueForKey:@"entities"];
                              NSDictionary * medias=[entity valueForKey:@"media"];
                              for (NSDictionary * media in medias){
                                  NSString *type=[media valueForKey:@"type"];
                                  if([type isEqualToString:@"photo"]){
                                      properties = [[ImageProperties alloc] init];
                                      properties.url = [NSURL URLWithString:[media valueForKey:@"media_url"]];
                                      properties.instagramPic = YES;
                                      properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
                                      properties.user = @"TwitterUSER";
                                      properties.isVideo=NO;
                                      properties.travomiPost = NO;

                                      [tempArray addObject:properties];
                                  }
                              }
                          }
                          //NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                          [propertiesArray addObjectsFromArray:tempArray];
                          [realTableView reloadData];
                      }
                      else {
                          NSLog(@"Error: %@", connectionError);
                      }
                  }];
             }
             else {
                 NSLog(@"Error: %@", clientError);
             }
         } else {
             NSLog(@"error: %@", [error localizedDescription]);
         }
     }];
    
    
}

//gets instagram feed based on what was searched.
- (void) getInstagramFeed {
    
    
    //looks for locations based off of the inital hashtag search of instagram
    if ([locationArray count] == 0) {
        locationBOOL = NO;
    }
    
    
    
    if (locationBOOL == NO) {
        hashtagWord = [islandHotels objectAtIndex:ndxCount];
        if (ndxCount == ([islandHotels count]-1)) {
            endSearch = YES;
        }
        
        NSString *requestUrl = [NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?client_id=%@&count=50",hashtagWord, KCLIENTID];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
        (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else {
        NSInteger ndx;
        numberOfLocations = [locationArray count];
        for (ndx = 0; [locationArray count] > ndx;) {
            NSString *requestUrl = [NSString stringWithFormat: @"https://api.instagram.com/v1/locations/%@/media/recent?client_id=%@",[locationArray objectAtIndex:ndx], KCLIENTID];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
            (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
            [locationArray removeObjectAtIndex:ndx];
            
        }
    }
}

- (IBAction)unwindToVC1:(UIStoryboardSegue*)sender
{
    
    NSLog(@"UNWINDTOVC1");
    propertiesArray = nil;
    [self viewDidLoad];
}

- (void)somethingToAvoidNestedPop {
    [self performSegueWithIdentifier:@"ToProfile" sender:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark NSURLConnection Delegate Methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

-(void) connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // loads all the images if there is a location in those images search for location
    if (locationBOOL == NO) {
        NSError *error;
        ImageProperties *properties;
        tempArray = [[NSMutableArray alloc]init];
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //NSLog(@"instagram json :::: %@", json);
        NSArray *firstPic = [[[json objectForKey:@"data"]valueForKey:@"images"]valueForKey:@"standard_resolution"];
        
        for (NSDictionary *testDict in firstPic) {
            
            properties = [[ImageProperties alloc] init];
            
            properties.url = [NSURL URLWithString:[testDict objectForKey:@"url"]];
            properties.instagramPic = YES;
            properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
            properties.user = @"InstagramUSER";
            properties.isVideo=NO;
            properties.travomiPost = NO;

            [tempArray addObject:properties];
            
        }
        
        NSArray *firstLocation = [[json objectForKey:@"data"]valueForKey:@"location"];
        for (NSDictionary *locDict in firstLocation) {
            if (![locDict isEqual:[NSNull null]]) {
                
                if ([locDict objectForKey:@"name"]) {
                    locationBOOL = YES;
                    [locationArray addObject:[locDict objectForKey:@"id" ]];
                }
            }
            
        }
        
        [propertiesArray addObjectsFromArray:tempArray];
        
        if (locationBOOL == YES) {
            [self getInstagramFeed];
        }
        
    }
    else {
        
        
        ImageProperties *properties;
        
        tempArray = [[NSMutableArray alloc]init];
        NSError *error;
        if (responseData == nil) {
            responseData = [[NSMutableData alloc]init];
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        NSArray *firstPic = [[[json objectForKey:@"data"]valueForKey:@"images"]valueForKey:@"standard_resolution"];
        
        for (NSDictionary *testDict in firstPic) {
            
            properties = [[ImageProperties alloc] init];
            
            properties.url = [NSURL URLWithString:[testDict objectForKey:@"url"]]; //1
            properties.instagramPic = YES;
            properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
            properties.user = @"InstagramUSER";
            properties.isVideo=NO;
            properties.travomiPost = NO;

            [tempArray addObject:properties];
            
            
            
        }
        [propertiesArray addObjectsFromArray:tempArray];
        
        
        
        
    }
    numberOfLocations--;
    
    if ([propertiesArray count]%4 != 0) {
        if (++numberOfLocations != 0 ){
            numberOfLocations--;
        }
        else {
            int number = [propertiesArray count]%4;
            int ndx;
            for (ndx = 0; ndx < number; ndx ++) {
                ImageProperties *properties;
                
                properties = [[ImageProperties alloc] init];
                properties.user = @"blank";
                properties.image = [UIImage imageNamed:@"white.png"];
                properties.travomiPost = NO;

                [propertiesArray addObject:properties];
                
            }
        }
    }
    responseData = nil;
    
    
    [realTableView reloadData];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    responseData = nil;
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}


#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == realTableView) {
        
        
    }
    else {
        if ([listOfTempArray count] > 0) {
            theSearchBar.text = [listOfTempArray objectAtIndex:indexPath.row];
            [self searchBarSearchButtonClicked:theSearchBar];
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}


// sets it to have 4 images per section of the table instead of just 1
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == realTableView) {
        if (defaultWord == NO) {
            return [propertiesArray count]/4 +1;
        }
        else if (ndxCount == ([islandHotels count] - 1)) {
            return [propertiesArray count]/4 + 1;
        }
        else {
            return [propertiesArray count]/4;
        }
    }
    else {
        return [listOfTempArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:realTableView]) {
        static NSString *CellIdentifier = @"ImageViewCell";
        ImageViewCell4 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[ImageViewCell4 alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:   CellIdentifier];
        }
        if (indexPath.row == [propertiesArray count]/4 - 1) {
            ndxCount ++;
            if (ndxCount < [islandHotels count] && defaultWord == YES) {
                [self getInstagramFeed];
                [self getTwitterFeed];
                [self getYoutubeFeed];
                [self getVineFeed];
            }
            else {
                NSLog(@"hits this last one ");
                endSearch = YES;
            }
        }
        
        
        if (indexPath.row == [propertiesArray count]/4) {
            cell = [[ImageViewCell4 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hello"];
            cell.imageView.hidden = YES;
            if ([propertiesArray count] >0) {
                if (endSearch == YES) {
                    cell.textLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:15.0];
                    cell.textLabel.text = @"              End of Search";
                    [cell setUserInteractionEnabled:NO];
                }
                
            }
        }
        else {
            cell.textLabel.hidden = TRUE;
            
            // math to have 4 images per row.
            ImageProperties *cellProperties1;
            ImageProperties *cellProperties2;
            ImageProperties *cellProperties3;
            ImageProperties *cellProperties4;
            cellProperties1 = ((ImageProperties * )propertiesArray[indexPath.row*4]);
            cellProperties2 = ((ImageProperties * )propertiesArray[indexPath.row*4+1]);
            cellProperties3 = ((ImageProperties * )propertiesArray[indexPath.row*4+2]);
            cellProperties4 = ((ImageProperties * )propertiesArray[indexPath.row*4+3]);
            
            
            
            if (cellProperties1.image) {
                cell.pictureView1.image = cellProperties1.image;
                if ([cellProperties1.user isEqualToString:@"blank"]) {
                    cell.button1.enabled = NO;
                    
                }
                
            }
            
            // this checks if each image id downloaded or not
            else {
                if (![cellProperties1.user isEqualToString:@"blank"]) {
                    
                    
                    cell.pictureView1.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties1.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties1.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                if(cellProperties1.isVideo || cellProperties1.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView1.image=newimage;
                                    cellProperties1.image=newimage;
                                }else{
                                    cell.pictureView1.image = image;
                                    cellProperties1.image = image;
                                }
                            }
                            
                        }];
                        
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties1) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView1.image=newimage;
                                    cellProperties1.image=newimage;
                                }else{
                                    cell.pictureView1.image = image.image;
                                    cellProperties1.image = image.image;
                                }
                                
                                cellProperties1.user = image.user;
                                cellProperties1.caption = image.caption;
                                cellProperties1.videoData=image.videoData;
                                cellProperties1.isUserVideo=image.isUserVideo;
                                //NSLog(@"Regurgitate %@, %@", cellProperties1.user, cellProperties1.caption);
                            }
                        }];
                    }
                }
                
            }
            if (cellProperties2.image) {
                cell.pictureView2.image = cellProperties2.image;
                
                
                if ([cellProperties2.user isEqualToString:@"blank"]) {
                    cell.button2.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties2.user isEqualToString:@"blank"]) {
                    
                    cell.pictureView2.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties2.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties2.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                if(cellProperties2.isVideo || cellProperties2.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView2.image=newimage;
                                    cellProperties2.image=newimage;
                                }else{
                                    cell.pictureView2.image = image;
                                    cellProperties2.image = image;
                                }
                            }
                            
                        }];
                        
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties2) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                //NSLog(@"lets do it");
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView2.image=newimage;
                                    cellProperties2.image=newimage;
                                }else{
                                    cell.pictureView2.image = image.image;
                                    cellProperties2.image = image.image;
                                }
                                cellProperties2.user = image.user;
                                cellProperties2.caption = image.caption;
                                cellProperties2.videoData=image.videoData;
                                cellProperties2.isUserVideo=image.isUserVideo;
                                //NSLog(@"Regurgitate %@, %@", cellProperties2.user, cellProperties2.caption);
                            }
                        }];
                    }
                }
            }
            if (cellProperties3.image) {
                cell.pictureView3.image = cellProperties3.image;
                if ([cellProperties3.user isEqualToString:@"blank"]) {
                    cell.button3.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties3.user isEqualToString:@"blank"]) {
                    
                    cell.pictureView3.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties3.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties3.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                if(cellProperties3.isVideo || cellProperties3.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView3.image=newimage;
                                    cellProperties3.image=newimage;
                                }else{
                                    cell.pictureView3.image = image;
                                    cellProperties3.image = image;
                                }
                            }
                            
                            
                        }];
                        
                        
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties3) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView3.image=newimage;
                                    cellProperties3.image=newimage;
                                }else{
                                    cell.pictureView3.image = image.image;
                                    cellProperties3.image = image.image;
                                }
                                
                                
                                cellProperties3.user = image.user;
                                cellProperties3.caption = image.caption;
                                cellProperties3.videoData=image.videoData;
                                cellProperties3.isUserVideo=image.isUserVideo;
                            }
                        }];
                    }
                }
            }
            if (cellProperties4.image) {
                cell.pictureView4.image = cellProperties4.image;
                if ([cellProperties4.user isEqualToString:@"blank"]) {
                    cell.button4.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties4.user isEqualToString:@"blank"]) {
                    
                    cell.pictureView4.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties4.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties4.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                
                                if(cellProperties4.isVideo || cellProperties4.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView4.image=newimage;
                                    cellProperties4.image=newimage;
                                }else{
                                    cell.pictureView4.image = image;
                                    cellProperties4.image = image;
                                }
                            }
                            
                        }];
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties4) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView4.image=newimage;
                                    cellProperties4.image=newimage;
                                }else{
                                    cell.pictureView4.image = image.image;
                                    cellProperties4.image = image.image;
                                }
                                
                                
                                cellProperties4.user = image.user;
                                cellProperties4.caption = image.caption;
                                cellProperties4.videoData = image.videoData;
                                cellProperties4.isUserVideo=image.isUserVideo;
                                
                                //NSLog(@"Regurgitate %@, %@", cellProperties4.user, cellProperties4.caption);
                            }
                        }];
                    }
                }
                
            }
        }
        return cell;
        
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text = [listOfTempArray objectAtIndex:indexPath.row];
        
        
        return cell;
    }
}

//code for displaying the player icon on the image
- (UIImage *) overlayPlayerIcon: (UIImage *) image{
    //NSLog(@"doing the overlay...");
    UIImage *backgroundImage = image;
    UIImage *playermarkImage = [UIImage imageNamed:@"PlayButtonImage2.png"];
    
    UIGraphicsBeginImageContext(backgroundImage.size);
    [backgroundImage drawInRect:CGRectMake(0, 0,500,400)];
    [playermarkImage drawInRect:CGRectMake(0,40 , 500,300)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}





//code for the searchbar on the page
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    searchHotelsTableView.hidden = NO;
    NSString *name = @"";
    NSString *firstLetter = @"";
    if (listOfTempArray.count > 0)
        [listOfTempArray removeAllObjects];
    
    if ([searchText length] > 0)
    {
        for (int i = 0; i < [itemOfMainArray count] ; i = i+1)
        {
            name = [itemOfMainArray objectAtIndex:i];
            if (name.length >= searchText.length)
            {
                firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                
                if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame)
                {
                    [listOfTempArray addObject: [itemOfMainArray objectAtIndex:i]];
                }
            }
        }
    }
    else
    {
        
        [listOfTempArray addObjectsFromArray:itemOfMainArray ];
    }
    
    [searchHotelsTableView reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSArray *hoteltag = [hotelNames allKeysForObject:theSearchBar.text];
    
    NSLog(@"what's is the array : %@", hoteltag);
    self.title = theSearchBar.text;
    UITabBar *tabBar = self.tabBarController.tabBar;

    UITabBarItem *item0 = [tabBar.items objectAtIndex:0];
    item0.title = @"";
    
    if ([hoteltag count] >0) {
        hashtagWord = [hoteltag objectAtIndex:0];
    } else {
        hashtagWord = islandPicked;
    }
    
    
    if( [hashtagWord caseInsensitiveCompare:islandPicked] == NSOrderedSame ) {
        defaultWord = YES;
    }
    else {
        defaultWord = NO;
    }
    
    ndxCount = 0;
    arrayCountSub = 0;
    searchHotelsTableView.hidden = YES;
    BOOL match = NO;
    int ndx = 0;
    images = [[NSMutableArray alloc] init];
    propertiesArray = [[NSMutableArray alloc]init];
    //NSLog(@"one");
    [realTableView reloadData];
    
    NSLog(@"what is the hashtagword? : %@", hashtagWord);
    hashtagWord = [hashtagWord lowercaseString];
    NSLog(@"what is the hashtagword? : %@", hashtagWord);

    // when search is pressed it querys the parse database then searches instagram.
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    [photoObjectQuery whereKey:@"tags" equalTo:[hashtagWord lowercaseStringWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]]];
    [photoObjectQuery orderByDescending:@"createdAt"];
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            for(PFObject *hello in objects){
                ImageProperties *property = [[ImageProperties alloc] init];
                property.id = [hello objectId];
                property.instagramPic = NO;
                property.travomiPost = YES;
                NSLog(@"hello?");
                
                [propertiesArray addObject:property];
                NSLog(@"propertiesArray count %lu", (unsigned long)[propertiesArray count] );
                //NSLog(@"two");
                
                [realTableView reloadData];
            }
        }
        else {
        }
    }];
    
    if (defaultWord == NO) {
        for(ndx = 0; ndx < [allHotels count] && match == NO; ndx++){
            if ([[allHotels objectAtIndex:ndx] isEqualToString:[hashtagWord capitalizedString]]){
                match = YES;
            }
        }
        if (match){
            NSString *requestUrl = [NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?client_id=%@&count=50",hashtagWord, KCLIENTID];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
            (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
        
    }
    else {
        [self getInstagramFeed];
        [self getTwitterFeed];
        [self getYoutubeFeed];
        [self getVineFeed];
    }
    theSearchBar.hidden = true;
    searchButton.selected = !searchButton.selected;
    [searchBar resignFirstResponder];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    theSearchBar.text = @"";
    
    searchHotelsTableView.hidden = YES;
    
    [searchBar resignFirstResponder];
}

- (IBAction)logoutButtonPressed:(id)sender
{
    [PFUser logOut];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    //[self viewDidAppear:NO];
}

- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}


- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

-(IBAction) searchButtonPressed:(id)sender {
    if (searchButton.selected == false) {
        theSearchBar.hidden = NO;
        searchButton.selected = !searchButton.selected;
        //searchHotelsTableView.hidden = NO;
    }
    else {
        searchButton.selected = !searchButton.selected;
        searchHotelsTableView.hidden = YES;
        theSearchBar.hidden = YES;
        [self.view endEditing:YES];
    }
}


- (void) downloadImageWithObject:(ImageProperties **)id completionBlock:(void (^)(BOOL succeeded, ImageProperties *image)) completionBlock
{
    //NSLog(@"%@",(*id).id);
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    [photoObjectQuery includeKey:@"creator"];
    [photoObjectQuery getObjectInBackgroundWithId:(*id).id block:^(PFObject *object, NSError *error) {
        if (!error) {
            ImageProperties *imageSpecs = [[ImageProperties alloc] init];
            imageSpecs.user = object[@"creator"][@"username"];
            imageSpecs.caption = object[@"caption"];
            imageSpecs.imageFile = object[@"image"];
            if([object.allKeys containsObject:@"video"]){
                imageSpecs.videoFile = object[@"video"];
                
            }
            
            [object[@"video"] getDataInBackgroundWithBlock:^(NSData *videoData,NSError *error) {
                if(!error){
                    NSLog(@"seccessfully get video: data length=%lu",(unsigned long)videoData.length);
                    imageSpecs.videoData=videoData;
                    if(videoData.length>0){
                        imageSpecs.isUserVideo=YES;
                        imageSpecs.caption=[imageSpecs.caption stringByAppendingString:@""];
                    }
                }else{
                    NSLog(@"ERROR downloading video");
                }
            }];
            [object[@"image"] getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    NSLog(@"successfully get image: data length=%lu",(unsigned long)imageData.length);
                    imageSpecs.image = [UIImage imageWithData:imageData];
                    
                    completionBlock (YES,imageSpecs);
                }
                else {
                    NSLog(@"Error downloading image");
                    completionBlock (NO,nil);
                }
            }];
        }
        else {
            NSLog(@"Error first query");
            completionBlock (NO,nil);
        }
    }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    
    if([title isEqualToString:@"GO"]) {
    }
    
    if([title isEqualToString:@"Okay"])
    {
        NSLog(@"Posted Photo was selected.");
        
    }
}

# pragma mark 


# pragma mark Segues
// segues based on what the user pressed
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LandingPageResultSegue"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        NSInteger path = realTableView.indexPathForSelectedRow.row;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        //NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button1"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button2"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 1;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button3"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 2;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button4"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 3;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        NSLog(@"tableuser :%@",controller.tableUser);
    }
    
}
@end
