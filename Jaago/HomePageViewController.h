//
//  HomePageViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/21/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


// initial starting page view controller past picking the island
@interface HomePageViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationBarDelegate, UIAlertViewDelegate, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
{
    NSMutableData *responseData;
}

@property NSString *islandPicked;
@property NSArray *islandHotels;
@property UIImage *navigationImage;

@property (weak, nonatomic) IBOutlet UISearchBar *theSearchBar;

@property (strong, nonatomic) IBOutlet UITableView *realTableView;

@property (strong, nonatomic) IBOutlet UITableView *searchHotelsTableView;

//@property (strong, nonatomic) IBOutlet UILabel *hotelTitleLabel;

@property (strong, nonatomic) NSMutableArray *images;
@property (strong, nonatomic) IBOutlet UIToolbar *bottomToolBar;

@property (strong, nonatomic) NSString *selectedRow;

- (IBAction)logoutButtonPressed:(id)sender;
- (IBAction)cameraButtonPressed:(id)sender;
- (IBAction)profileButtonPressed:(id)sender;


@end
