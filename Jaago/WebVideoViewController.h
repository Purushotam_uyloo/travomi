//
//  WebVideoViewController.h
//  YTBrowser
//
//  Created by Marin Todorov on 09/01/2013.
//  Copyright (c) 2013 Underplot ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"
@import AVFoundation;
#import "PlayerView.h"
#import "VineVideoModel.h"
@interface WebVideoViewController : UIViewController

@property (weak, nonatomic) VideoModel* youtubevideo;
@property (weak, nonatomic) VineVideoModel* vineVideo;
@property (weak, nonatomic) NSData * uservideodata;
@property (strong, nonatomic) NSString * videotype;
@property (nonatomic) AVPlayer *player;
@property (nonatomic) AVPlayerItem *playerItem;
@property (weak, nonatomic) IBOutlet PlayerView *playerView;

@end
