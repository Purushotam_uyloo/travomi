//
//  UserFeedsViewController.m
//  Travomi
//
//  Created by admin on 3/11/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "UserFeedsViewController.h"
#import "GTLHotelEndpointHotel.h"
#import "GTLServiceHotelEndpoint.h"
#import "GTLQueryCustomerLogin.h"
#import "GTLHotelEndpointImageCollection.h"
#import "ImageProperties.h"
#import "GTLHotelEndpointImage.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "TwitterfeedsTableViewCell.h"
#import "FeedsCollectionViewCell.h"
#import "HotelDetailViewController.h"
#import "ReportPhoto.h"
#import "UIImageView+WebCache.h"
#import "GTLCustomerLoginImageCollection.h"
#import "GTLCustomerLoginImage.h"


@interface UserFeedsViewController ()

@end

@implementation UserFeedsViewController
@synthesize myCollectionView;
@synthesize DataArr;
static GTLServiceHotelEndpoint *service = nil;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!service) {
        service = [[GTLServiceHotelEndpoint alloc] init];
        service.retryEnabled = YES;
        
    }
    
    FeedsArray = [[NSMutableArray alloc]init];

    
    for(int i=0 ; i < DataArr.count ; i++)
    {
        GTLCustomerLoginImage * wrapper = (GTLCustomerLoginImage *)[DataArr objectAtIndex:i];
        
        ImageProperties * prop =[[ImageProperties alloc]init];
        prop.id = prop.id;
        if(wrapper.video == 0)
        {
            prop.isVideo = true;
        }
        else
        {
            prop.isVideo = false;
        }
        
        if(wrapper.video.intValue == 1)
        {
            prop.url = [NSURL URLWithString:wrapper.sURL];

        }
        else
        {
            prop.url = [NSURL URLWithString:wrapper.sURL];

        }

        [FeedsArray addObject:prop];
        
        
    }
    [myCollectionView reloadData];
    
    
    
    // Do any additional setup after loading the view.
}

-(void)DeleteImagesforRow:(int)row
{
    
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        HUD.delegate = self;
        [HUD show:YES];
    
    GTLCustomerLoginImage * wrapper = (GTLCustomerLoginImage *)[DataArr objectAtIndex:row];

    
    GTLQueryCustomerLogin *query = [GTLQueryCustomerLogin queryForDeleteUserImageWithImageid:[wrapper.identifier integerValue]];
    [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLHotelEndpointImageCollection *object, NSError *error) {
        
        if(error == nil)
        {
            

          //  [myCollectionView reloadData];
            
            [FeedsArray removeObjectAtIndex:row];
            [myCollectionView reloadData];
            

            [HUD hide:YES];
            [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
            
        
            
            
        }
        else
        {

                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                        
        }
        
        // Do something with items.
    }];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return FeedsArray.count;
}




- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FeedsCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.projImg.contentMode = UIViewContentModeScaleAspectFit;
   // cell.projImg.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
    
    cell.projImg.backgroundColor = [UIColor blackColor];
    ImageProperties * wrapper = (ImageProperties *)[FeedsArray objectAtIndex:indexPath.row];

    
    NSString *image1 =[NSString stringWithFormat:@"%@",wrapper.url];
    
    if(wrapper.isVideo == true)
    {
        [cell.playImg setHidden:false];
        
    }
    else
    {
        [cell.playImg setHidden:true];

    }
    
    if (![image1 isEqualToString:@""])
    {
        
//        UIActivityIndicatorView *spinnerCell = [[UIActivityIndicatorView alloc]
//                                                initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        spinnerCell.color=[UIColor darkGrayColor];
//        spinnerCell.frame=CGRectMake(cell.projImg.frame.size.width/2-10, cell.projImg.frame.size.height/2-10, 20, 20);
//        [cell.projImg addSubview:spinnerCell];
//        [spinnerCell startAnimating];
        
        [cell.projImg sd_setImageWithURL:wrapper.url
                     placeholderImage:[UIImage imageNamed:@"no_img_sq.png"]];
        
 
    }
    else
    {
        UIImage *imgno = [UIImage imageNamed:@"no_img_sq.png"];
        
        [cell.projImg setImage:imgno];
    }
    
    
   // [cell.projImg setBackgroundColor:[UIColor grayColor]];
    
    [cell.reportbtn setTag:indexPath.row];
    [cell.reportbtn addTarget:self action:@selector(deleteBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;

}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(collectionView.frame)/2)-2,160);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    

}

-(void)deleteBtnClicked:(id)sender
{

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
