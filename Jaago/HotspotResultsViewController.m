//
//  HotspotResultsViewController.m
//  Jaago
//
//  Created by WangYinghao on 3/15/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "HotspotResultsViewController.h"
#import "BookingWebViewViewController.h"

@interface HotspotResultsViewController ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIImageView *hotspotImageView;
@property NSString *bookingEngineName;


@end

@implementation HotspotResultsViewController
    @synthesize HotelName;

- (void)viewDidLoad {
    [super viewDidLoad];
    //set title
    self.navigationItem.title = @"Hotspot Detail";
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    NSLog(@"current hotspot detail:%@", self.property.googleSearchResults);
    self.hotspotImageView.image = self.property.image;
    
//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
//    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//    blurEffectView.frame = self.view.bounds;
//    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    
//    [ self.hotspotImageView addSubview:blurEffectView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDelegate and UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    if (row==0 || row == 4) {
        if (row == 0) {
            //title cell
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell"];
            [cell.textLabel setText:[self.property.googleSearchResults objectForKey:@"name"]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        } else {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell"];
            [cell.textLabel setText:@"Book Now!"];
            [cell.textLabel setTextColor:[UIColor blueColor]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        }
        
        
    } else {
        //detail cell
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
        if (row==1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
            [cell.textLabel setText:@"Address"];
            if([self.property.googleSearchResults objectForKey:@"formatted_address"]){
                NSString * address = [self.property.googleSearchResults objectForKey:@"formatted_address"];
                [cell.detailTextLabel setText:address];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        } else if (row==2) {
            [cell.textLabel setText:@"Rating"];
            if([self.property.googleSearchResults objectForKey:@"rating"]){
                NSString * rating = [self.property.googleSearchResults objectForKey:@"rating"];
                [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ / 5", rating ]];
            } else {
                [cell.detailTextLabel setText:@"N/A"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        } if (row==3) {
            [cell.textLabel setText:@"Now"];
            if([[self.property.googleSearchResults objectForKey:@"opening_hours"] objectForKey:@"open_now"]){
                id opennow = [[self.property.googleSearchResults objectForKey:@"opening_hours"] objectForKey:@"open_now"];
                
                if ([opennow isEqual:@(YES)]) {
                    [cell.detailTextLabel setText:@"Open"];
                } else {
                    [cell.detailTextLabel setText:@"CLosed"];
                }
            } else {
                [cell.detailTextLabel setText:@"Unknown"];
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

            return cell;
        }
        

        
        
        
        return cell;
    }
    
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row !=4) {
        return NO;
    } else {
        return YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 4) {
        //book now action
        UIAlertAction * viatorAction = [UIAlertAction actionWithTitle:@"Viator.com" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
            NSString * URLString = [self.property viatorURL];
            self.bookingEngineName = @"Viator.com";
            [self performSegueWithIdentifier:@"bookPage" sender:URLString];
        }];
        UIAlertAction * opentableAction = [UIAlertAction actionWithTitle:@"Opentable.com" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
            NSString * URLString = [self.property opentableURL];
            self.bookingEngineName = @"Opentable.com";
            [self performSegueWithIdentifier:@"bookPage" sender:URLString];
        }];
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Choose where to book" message:@"currently supported websites:" preferredStyle:UIAlertControllerStyleActionSheet];
        if ([self.property.hotspottype isEqualToString:@"attraction"]) {
            [alert addAction:viatorAction];
        }
        if ([self.property.hotspottype isEqualToString:@"restaurant"]) {
            [alert addAction:opentableAction];
        }
        
        
        
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    BookingWebViewViewController* destinationController = [segue destinationViewController];
    destinationController.aURLStringToOpen = (NSString *)sender;
    destinationController.bookingEngineName = self.bookingEngineName;
    destinationController.searchKeyWord = [self.property.caption substringFromIndex:1];
}

@end
