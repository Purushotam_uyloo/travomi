//
//  RecycleBinResultViewController.m
//  Jaago
//
//  Created by WangYinghao on 2/24/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "RecycleBinResultViewController.h"
#import <Parse/Parse.h>
#import "CommentViewController.h"



// Controller used for the profile.


#define LIKE 1
#define COMMENT 2
#define FOLLOW 3

@interface RecycleBinResultViewController ()
{
    BOOL liked;
    NSString *likedID;
    PFObject *likedObject;
}
@end

@implementation RecycleBinResultViewController

@synthesize selectedImage, selectedImageCaption,picture,caption,selectedImageProperties;

- (void)viewDidLoad
{
    [super viewDidLoad];
    liked = NO;
    PFQuery *likeQuery = [self didUserLike];
    [likeQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (objects) {
                for (PFObject *object in objects) {
                    if (object) {
                        likedObject = object;
                        NSLog(@"there is something here");
                        liked = YES;
                        likedID = [object objectId];
                        NSLog(@"likedID %@", likedID);
                    }
                    
                }
            }
            else {
                NSLog(@"nothing?");
            }
            
        }
        else {
            NSLog(@"error getting initial like query");
        }
    }];
    //liked =
    NSLog(@"selected Image Properties. photoID %@", selectedImageProperties.photoID);
    
    [[self.caption layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.caption layer] setBorderWidth:0.3];
    [[self.caption layer] setCornerRadius:15];
    
    caption.text = selectedImageProperties.caption;
    [picture setImage: selectedImageProperties.image];
    
    
}


//querys what the user liked previously to display it (doesn't do anything at the moment)
- (PFQuery *) didUserLike{
    PFQuery *likeQuery = [PFQuery queryWithClassName:@"Notification"];
    
    [likeQuery whereKey:@"notificationType" equalTo:@"like"];
    [likeQuery whereKey:@"photoID" equalTo:selectedImageProperties.id];
    //[likeQuery whereKey:@"photoID" equalTo:selectedImageProperties.id];
    return likeQuery;
}


//remove photo that user uploaded

- (IBAction)removePhoto:(UIButton *)sender {
    NSLog(@"%@",selectedImageProperties.id);
    
    PFQuery *deletePhotoQuery = [PFQuery queryWithClassName:@"Photo"];
    [deletePhotoQuery whereKey:@"objectId" equalTo:selectedImageProperties.id];
    [deletePhotoQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                NSLog(@"deleting.....");
                [object deleteInBackground];
            }
            //[self.navigationController popViewControllerAnimated:YES];
            
            [self performSegueWithIdentifier:@"unwindAfterDelete" sender:self];
            
            
        }
    }];
    
}

- (IBAction)restorePhoto:(UIButton *)sender {
    NSLog(@"%@",selectedImageProperties.id);
    
    PFQuery *deletePhotoQuery = [PFQuery queryWithClassName:@"Photo"];
    [deletePhotoQuery whereKey:@"objectId" equalTo:selectedImageProperties.id];
    [deletePhotoQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                NSLog(@"restoring.....");
                [object setObject:@1 forKey:@"active"];
                [object save];
            }
            //[self.navigationController popViewControllerAnimated:YES];
            
            [self performSegueWithIdentifier:@"unwindAfterDelete" sender:self];
            
            
        }
    }];

}

@end
