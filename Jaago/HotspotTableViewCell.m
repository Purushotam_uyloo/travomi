//
//  HotspotTableViewCell.m
//  Travomi
//
//  Created by admin on 2/8/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "HotspotTableViewCell.h"

@implementation HotspotTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    backImgview.backgroundColor = [UIColor whiteColor];
    backImgview.layer.masksToBounds = NO;
    backImgview.layer.shadowColor = [UIColor blackColor].CGColor;
    backImgview.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    backImgview.layer.shadowOpacity = 0.5f;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
