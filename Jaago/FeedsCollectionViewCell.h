//
//  FeedsCollectionViewCell.h
//  Travomi
//
//  Created by admin on 3/12/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedsCollectionViewCell : UICollectionViewCell

@property(nonatomic,retain)IBOutlet UIImageView * projImg;
@property(nonatomic,retain)IBOutlet UIImageView * playImg;
@property(nonatomic,retain)IBOutlet UIButton * reportbtn;


@end
