//
//  ImageViewCell4.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 9/15/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewCell4 : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *pictureView1;
@property (strong, nonatomic) IBOutlet UIButton *button4;
@property (strong, nonatomic) IBOutlet UIImageView *pictureView2;
@property (strong, nonatomic) IBOutlet UIButton *button3;
@property (strong, nonatomic) IBOutlet UIImageView *pictureView3;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIImageView *pictureView4;
@property (strong, nonatomic) IBOutlet UIButton *button1;

@end
