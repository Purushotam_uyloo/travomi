//
//  ReportPhoto.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 11/15/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MBProgressHUD;

@interface ReportPhoto : UIViewController
{
    MBProgressHUD * HUD;
}

- (IBAction)itemOnePressed:(id)sender;
- (IBAction)itemTwoPressed:(id)sender;
- (IBAction)itemThreePressed:(id)sender;
- (IBAction)cencelPressed:(id)sender;

@property NSString *name;

@end
