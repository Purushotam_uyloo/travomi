//
//  RecycleBinTableViewController.h
//  Jaago
//
//  Created by WangYinghao on 2/24/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecycleBinTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *realTableView;

@end
