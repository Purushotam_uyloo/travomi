//
//  CameraController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/5/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraController : UIImagePickerController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end
