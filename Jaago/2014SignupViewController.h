//
//  2014SignupViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/16/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface _014SignupViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>


@property (nonatomic, strong) IBOutlet UILabel *welcomeLabel;

- (IBAction)logOutButtonTapAction:(id)sender;


@end
