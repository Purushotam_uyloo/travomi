//
//  UserFeedsViewController.h
//  Travomi
//
//  Created by admin on 3/11/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface UserFeedsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,MBProgressHUDDelegate>
{
    NSMutableArray * FeedsArray;
    MBProgressHUD * HUD;
}
@property(nonatomic,retain)NSArray * DataArr;
@property (nonatomic, retain) IBOutlet UICollectionView *myCollectionView;


@end
