//
//  HeaderCell.h
//  Travomi
//
//  Created by admin on 2/3/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderCell : UITableViewCell
{
    
}
@property(nonatomic,retain)IBOutlet UIImageView * PlayImgview;

@property(nonatomic,retain)IBOutlet UIImageView * Imageview;
@property(nonatomic,retain)IBOutlet UILabel * PhotoCountLbl;

@end
