//
//  SocialMediaSettingsViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 9/5/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>


//controller for the media settings page
@interface SocialMediaSettingsViewController : UIViewController<UIAlertViewDelegate>
{
    IBOutlet UISwitch *facebookSwitch;
    IBOutlet UISwitch *twitterSwitch;
    IBOutlet UISwitch *tumblrSwitch;
    
}
- (IBAction)switchFacebookSwitch:(id)sender;
- (IBAction)switchTwitterSwitch:(id)sender;
- (IBAction)switchTumblrSwitch:(id)sender;


@end
