//
//  ForgotPasswordViewController.m
//  Travomi
//
//  Created by Kumar, Gopesh on 10/9/16.
//  Copyright © 2016 MarkAnthonyCorpuz. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import <Parse/Parse.h>
#import "GTLCustomerLoginCustomerProfile.h"
#import "GTLQueryCustomerLogin.h"
#import "GTLServiceCustomerLogin.h"
#import "GTLCustomerLoginCommonResponse.h"

@interface ForgotPasswordViewController ()
{
    __weak IBOutlet UITextField *txtFieldEmail;
}

@end

static GTLServiceCustomerLogin *service = nil;

static NSString * const emailRegEx =
@"(?:[a-z0-9A-Z!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9A-Z!#$%\\&'*+/=?\\^_`{|}"
@"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
@"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9A-Z](?:[a-"
@"z0-9A-Z-]*[a-z0-9A-Z])?\\.)+[a-z0-9A-Z](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5"
@"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
@"9][0-9]?|[a-z0-9A-Z-]*[a-z0-9A-Z]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
@"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!service) {
        service = [[GTLServiceCustomerLogin alloc] init];
        service.retryEnabled = YES;
        
    }
    
    // Do any additional setup after loading the view.
}
/**
 Is email address valid.
 @return YES if email is valid.
 */
- (BOOL)isEmailValid {
    NSPredicate *regExPredicate =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL validEmail = [regExPredicate evaluateWithObject:txtFieldEmail.text];
    return validEmail;
}

-(IBAction)btnSubmitAction:(id)sender{
    if ([self isEmailValid]) {
        
        [self CallAPI];
        //[PFUser requestPasswordResetForEmail:txtFieldEmail.text];
    }
    else{
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter valid email." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [controller addAction:okAction];
        [self presentViewController:controller animated:true completion:nil];
    }
}

-(void)CallAPI
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    GTLQueryCustomerLogin * query = [GTLQueryCustomerLogin queryForForgotPwdWithCUsername:txtFieldEmail.text];
    
    [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLCustomerLoginCommonResponse *object, NSError *error) {
        
        if(error == nil)
        {
            
            GTLCustomerLoginCommonResponse * resp = object;
            
            if([resp.status.stringValue isEqualToString:@"1"])
            {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Travomi" message:@"Password sent successfully to your email" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
                
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                
                [self dismissViewControllerAnimated:true completion:nil];
            }
            else
            {
                UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Travomi" message:@"Email not registered" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                
                
                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
            }
            
            
        }
        else
        {
            
            
            
        }
        
        // Do something with items.
    }];
    
    
}

-(IBAction)btnBackAction:(id)sender{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
