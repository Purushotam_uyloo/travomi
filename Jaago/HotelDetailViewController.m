//
//  HotelDetailViewController.m
//  Travomi
//
//  Created by admin on 2/3/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "HotelDetailViewController.h"
#import "HeaderCell.h"
#import "AboutTableViewCell.h"
#import "HotelDescTableViewCell.h"
#import "GTLHotelEndpointHotel.h"
#import "GTLHotelEndpointImage.h"
#import "AmenitiesTableViewCell.h"
#import "BookNowTableViewCell.h"
#import "_014AppDelegate.h"
#import "HotspotTableViewCell.h"
#import "HotspotListViewController.h"
#import "GTLHotelEndpointHotelUrl.h"
#import "ImageProperties.h"
#import "JSONModelLib.h"
#import "BookingWebViewViewController.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "TwitterfeedsTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "GTLHotelEndpointHotelUrl.h"

#import "ReportPhoto.h"


//#define KCLIENTID @"745cbb1a78f9401fa4d8cc3a5713a32c"

//db6ba5080beb40fcaa0d9a37d875afd4

#define KCLIENTID @"1230913616.1677ed0.7b8de9f1658149b6b388a346ebcef41d"


#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface HotelDetailViewController ()

@end

@implementation HotelDetailViewController
@synthesize DataArray;
@synthesize ImagesArray;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    _delegate = (_014AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    Photos = [[NSMutableArray alloc]init];
    TwitterArray = [[NSMutableArray alloc]init];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    

    
    
    
    
    NSLog(@"%@",[DataArray valueForKey:@"amenities"]);
    tableview.estimatedRowHeight = 10;
    tableview.rowHeight = UITableViewAutomaticDimension;
    
   // [self getInstagramFeed];
    
    // Do any additional setup after loading the view.
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    currentloc = [location coordinate];
    [locationManager stopUpdatingLocation];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    
    return 6;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 5)
    {
        
        
//        if(TwitterArray.count == 0)
//        {
//            return 0.0;
//        }
//        else{
//            return 180.0;
//        }
        
    }
    
     return UITableViewAutomaticDimension;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0)
    {
        HeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headcell" forIndexPath:indexPath];
        
        
        
      //  NSArray * Images = (NSArray *)[self.DataArray valueForKey:@"gallery"];
        
        ImageProperties * wrapper = (ImageProperties *)[ImagesArray objectAtIndex:_selectedIndex];

        if(wrapper.isVideo == true)
        {
            [cell.PlayImgview setHidden:false];
        }
        else
        {
            [cell.PlayImgview setHidden:true];

        }
        
        
        cell.PhotoCountLbl.text = [NSString stringWithFormat:@"%lu Photos",(unsigned long)ImagesArray.count];
        
      //  GTLHotelEndpointImage * ImageWrapper = (GTLHotelEndpointImage *)[Images objectAtIndex:0];
        
        NSString *image1 =[NSString stringWithFormat:@"%@",wrapper.url];
        
        if (![image1 isEqualToString:@""])
        {
            [cell.Imageview sd_setImageWithURL:wrapper.url
                            placeholderImage:[UIImage imageNamed:@"no_img_sq.png"]];

  
        }
        else
        {
            UIImage *imgno = [UIImage imageNamed:@"no_img_sq.png"];
            
            [cell.Imageview setImage:imgno];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        
        return cell;
    }
    else if (indexPath.row ==1)
    {
        HotelDescTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"desc" forIndexPath:indexPath];
        cell.HotelNameLbl.text = [DataArray valueForKey:@"name"];
        cell.HotelDescLbl.text = [DataArray valueForKey:@"address"];
        cell.HotelDescLbl.numberOfLines = 3;
        
        [cell.MapButton addTarget:self action:@selector(MapbuttonClicked) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if (indexPath.row ==2)
    {
        //about
        
        AboutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"about" forIndexPath:indexPath];
        cell.HotelNameLbl.text = @"About";
        cell.HotelDescLbl.text = [DataArray valueForKey:@"des"];
        
        
        
        [cell.HotelDescLbl setUserInteractionEnabled:true];
        cell.HotelDescLbl.delegate = self;
        
        
        
        //        cell.HotelDescLbl.linkAttributes = [
        //                                                    NSForegroundColorAttributeName: [UIColor redColor]                                                    ]
        //
        // ...Read less
        if(IsAboutMoreClicked==false)
        {
            [cell.Morebtn setTitle:@"More" forState:UIControlStateNormal];
            NSMutableAttributedString *trunc = [[NSMutableAttributedString alloc] initWithString:@" ...Read more"];
            
            [trunc addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(0, 13)];
            [trunc addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"Readmore"] range:NSMakeRange(0, 13)];
            [trunc addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, 13)];
            
            
            
            cell.HotelDescLbl.attributedTruncationToken = trunc;
            cell.HotelDescLbl.linkAttributes =@{ NSFontAttributeName : [UIFont systemFontOfSize:12],NSForegroundColorAttributeName : [UIColor grayColor] };
            
            
            
            cell.HotelDescLbl.numberOfLines = 3;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
        }
        else
        {
            cell.HotelDescLbl.numberOfLines = 0;
            [cell.Morebtn setTitle:@"Less" forState:UIControlStateNormal];
            NSMutableAttributedString *trunc = [[NSMutableAttributedString alloc] initWithString:@" ...Read less"];
            
            [trunc addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12.0] range:NSMakeRange(0, 13)];
            [trunc addAttribute:NSLinkAttributeName value:[NSURL URLWithString:@"Readless"] range:NSMakeRange(0, 13)];
            [trunc addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, 13)];
            
            NSMutableAttributedString * partOne = [[NSMutableAttributedString alloc] initWithString:[DataArray valueForKey:@"des"]];
            
            
            NSMutableAttributedString *combination = [[NSMutableAttributedString alloc] init];
            [combination appendAttributedString:partOne];
            [combination appendAttributedString:trunc];
            
            [cell.HotelDescLbl setText:combination];
            cell.HotelDescLbl.attributedTruncationToken = trunc;
            cell.HotelDescLbl.linkAttributes =@{ NSFontAttributeName : [UIFont systemFontOfSize:12],NSForegroundColorAttributeName : [UIColor grayColor] };
            
            
            
            
        }
        
        [cell.Morebtn addTarget:self action:@selector(ReadMoreAboutSectionClicked) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    else if (indexPath.row == 3)
    {
        AmenitiesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"amenities" forIndexPath:indexPath];
        [cell initwithAmenitiesArray:[DataArray valueForKey:@"amenities"]];
        
        [cell.Morebtn setHidden:YES];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    else if (indexPath.row == 4)
    {
        HotspotTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hotspot" forIndexPath:indexPath];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if (indexPath.row == 5)
    {
//        TwitterfeedsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"twitter" forIndexPath:indexPath];
//      
//        if(TwitterArray.count > 0)
//        {
//        [cell initwithAmenitiesArray:TwitterArray];
//        [cell.Morebtn addTarget:self action:@selector(OpenTwitterFeeds) forControlEvents:UIControlEventTouchUpInside];
//        }
//
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        
//        return cell;
        
        BookNowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"booknow" forIndexPath:indexPath];
        
        [cell.BookNowbtn addTarget:self action:@selector(BookNowBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    else if (indexPath.row == 6)
    {
        BookNowTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"booknow" forIndexPath:indexPath];
        
        [cell.BookNowbtn addTarget:self action:@selector(BookNowBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    return nil;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row==0)
    {
        [Photos removeAllObjects];
        
       // NSArray * Images = (NSArray *)[self.DataArray valueForKey:@"gallery"];
        
        for(int i=0 ; i < ImagesArray.count ; i++)
        {
          //  GTLHotelEndpointImage * ImageWrapper = (GTLHotelEndpointImage *)[Images objectAtIndex:i];
            ImageProperties * wrapper = (ImageProperties *)[ImagesArray objectAtIndex:i];
            
            if(wrapper.isVideo == true)
            {
                MWPhoto *video = [MWPhoto photoWithURL:wrapper.url];
                video.videoURL = wrapper.videourl;
                [Photos addObject:video];
            }
            else
            {
                [Photos addObject:[MWPhoto photoWithURL:wrapper.url]];

            }
            
            
            
            
            
            //  [Photos addObject:ImageWrapper.sURL];
            
        }
        
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        
        // Set options
        browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
        browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
        browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
        browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
        browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
        browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
        browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
        browser.autoPlayOnAppear = YES; // Auto-play first video
        
        // Customise selection images to change colours if required
        browser.customImageSelectedIconName = @"ImageSelected.png";
        browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
        
        // Optionally set the current visible photo before displaying
        [browser setCurrentPhotoIndex:_selectedIndex];
        
        // Present
        [self.navigationController pushViewController:browser animated:YES];
        
        
    }
    else if ( indexPath.row == 4)
    {
        HotspotListViewController * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"hot"];
        NSString* thishotelname = [DataArray valueForKey:@"name"];
        
        // NSString* thishotelname = @"Koloalanding";
        page.islandPicked = thishotelname;
        page.islandHotels = [NSArray arrayWithObject:thishotelname];
        page.title=thishotelname;
        
        //  page.DataArray = [HotelArray objectAtIndex: indexPath.row];
        [self.navigationController pushViewController:page animated:YES];
    }
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return Photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < Photos.count) {
        return [Photos objectAtIndex:index];
    }
    return nil;
}

-(void)ReadMoreAboutSectionClicked
{
    if(IsAboutMoreClicked == false)
    {
        IsAboutMoreClicked = true;
    }
    else
    {
        IsAboutMoreClicked = false;
    }
    
    [tableview beginUpdates];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [tableview reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [tableview endUpdates];
}

-(void)viewWillAppear:(BOOL)animated
{

    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    UIView * backbtnview = [[UIView alloc]init];
    
    backbtnview.frame = CGRectMake(0, 0, 64,44);
    backbtnview.backgroundColor = [UIColor clearColor];
    
    UIImageView * CallImg = [[UIImageView alloc]init];
    CallImg.frame = CGRectMake(0, 6, 32,32);
    [CallImg setImage:[UIImage imageNamed:@"flag"]];
    [backbtnview addSubview:CallImg];
    
    
    UIButton * CallBtn = [UIButton  buttonWithType: UIButtonTypeCustom];
    CallBtn.frame = CGRectMake(0, 10, 34,40);
    [CallBtn addTarget:self action:@selector(CallBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [backbtnview addSubview:CallBtn];
    
    
    UIBarButtonItem * BackBarButton = [[UIBarButtonItem alloc]init];
    [BackBarButton setCustomView:backbtnview];
    
    
    UIBarButtonItem * negativeSpace = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpace.width = -7.0;
    
    UIImageView * ShareImg = [[UIImageView alloc]init];
    ShareImg.frame = CGRectMake(34, 10, 18,20);
    [ShareImg setImage:[UIImage imageNamed:@"shareIconWhite"]];
    [backbtnview addSubview:ShareImg];
    
    
    UIButton * ShareBtn = [UIButton  buttonWithType: UIButtonTypeCustom];
    ShareBtn.frame = CGRectMake(34, 10, 34,40);
    [ShareBtn addTarget:self action:@selector(ShareBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [backbtnview addSubview:ShareBtn];
    
    
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpace,BackBarButton, nil]];
    
    grad = [CAGradientLayer layer];
    grad.frame =  self.navigationController.navigationBar.bounds;
    
    CGFloat  Screenwidth = [UIScreen mainScreen].bounds.size.width;
    
    CGSize size = CGSizeMake(Screenwidth, 44);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    size_t gradientNumberOfLocations = 2;
    CGFloat gradientLocations[2] = { 0.0, 1.0 };
    CGFloat gradientComponents[8] = { 0.0,0.0,0.0,0.60,     // Start color
        255.0, 255.0, 255.0, 0.0, };  // End color
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents (colorspace, gradientComponents, gradientLocations, gradientNumberOfLocations);
    
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, size.height), 0);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorspace);
    UIGraphicsEndImageContext();
    
    
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithPatternImage:image];
    
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    [self ApplytransparentcolortoStatusBar];
}

-(void)ApplytransparentcolortoStatusBar
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        CGFloat  Screenwidth = [UIScreen mainScreen].bounds.size.width;
        
        CGSize size = CGSizeMake(Screenwidth, 20);
        UIGraphicsBeginImageContextWithOptions(size, NO, 0);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        
        size_t gradientNumberOfLocations = 2;
        CGFloat gradientLocations[2] = { 0.0, 1.0 };
        CGFloat gradientComponents[8] = { 0.0,0.0,0.0,0.60,     // Start color
            0.0, 0.0, 0.0, 0.60, };  // End color
        
        CGGradientRef gradient = CGGradientCreateWithColorComponents (colorspace, gradientComponents, gradientLocations, gradientNumberOfLocations);
        
        CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, size.height), 0);
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        CGGradientRelease(gradient);
        CGColorSpaceRelease(colorspace);
        UIGraphicsEndImageContext();
        
        transparentview=[[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
        transparentview.backgroundColor=[UIColor colorWithPatternImage:image];
        [_delegate.window.rootViewController.view addSubview:transparentview];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    // [grad removeFromSuperlayer];
    
    [transparentview removeFromSuperview];
    
    
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:217.0/255.0 green:85.0/255.0 blue:46.0/255.0 alpha:1.0];
    
    self.navigationController.navigationBar.shadowImage = nil;
    // [self.navigationController.navigationBar setTranslucent:false];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}

-(void)MapbuttonClicked
{
    NSString *baseUrl = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%@,%@", currentloc.latitude,currentloc.longitude,[DataArray valueForKey:@"lat"],[DataArray valueForKey:@"lng"]];
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:baseUrl]];
    
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if(IsAboutMoreClicked == false)
    {
        IsAboutMoreClicked = true;
    }
    else
    {
        IsAboutMoreClicked = false;
    }
    
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    
    [tableview beginUpdates];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [tableview reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [tableview endUpdates];
}

-(void)CallBtnClicked
{
    
    ReportPhoto * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"report"];
    //    page.DataArray = self.DataArr;
    //    page.ImagesArray = [[NSMutableArray alloc]initWithArray:FeedsArray];
    //    page.selectedIndex = indexPath.row;
    
    [self.navigationController pushViewController:page animated:YES];
    
    //[DataArray valueForKey:@"primaryPhoneNumber"]
    
//    NSString *phoneNumber = [@"tel://" stringByAppendingString:[DataArray valueForKey:@"primaryPhoneNumber"]];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
}

-(void)ShareBtnClicked
{
    NSLog(@"%@",[DataArray valueForKey:@"bookingUrls"]);
    
    NSArray * BookingArray = [DataArray valueForKey:@"bookingUrls"];
    NSString * UrltoShare =[[NSString alloc]init];
    for(int i = 0 ; i < [BookingArray count];i++)
    {
        GTLHotelEndpointHotelUrl * wrapper = [BookingArray objectAtIndex:i];
        if([wrapper.name isEqualToString:@"Official Website"])
        {
            UrltoShare = wrapper.url;
        }
    }
    
    NSString *textToShare = [NSString stringWithFormat:@"Check out %@ at %@.You can get discount at %@.The promo code is 5%@",[DataArray valueForKey:@"name"],[DataArray valueForKey:@"address"],[DataArray valueForKey:@"name"],@"%d"];
    // NSURL *myWebsite = [NSURL URLWithString:@"http://www.codingexplorer.com/"];
    
    NSArray *objectsToShare = @[textToShare,UrltoShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)BookNowBtnClicked
{

    
    
    NSLog(@"%@",[DataArray valueForKey:@"bookingUrls"]);
    

    
    
    NSArray * Bookingurl = [DataArray valueForKey:@"bookingUrls"];
    NSMutableArray * BookingUrlsArray = [[NSMutableArray alloc]init];
    
    for(int i = 0 ; i < Bookingurl.count ; i++)
    {
        GTLHotelEndpointHotelUrl * wrapper = (GTLHotelEndpointHotelUrl *)[Bookingurl objectAtIndex:i];
        [BookingUrlsArray addObject:wrapper.name];
        
        
    }
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Book Now"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    for (int j =0 ; j<BookingUrlsArray.count; j++){
        NSString *titleString = BookingUrlsArray[j];
        UIAlertAction *action = [UIAlertAction actionWithTitle:titleString style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            NSLog(@"Selected Value: %@",BookingUrlsArray[j]);
            
            BookingWebViewViewController * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"book"];
            page.bookingEngineName = BookingUrlsArray[j];
            
            NSString * pageurl =@"";
            for(int i = 0 ; i < Bookingurl.count ; i++)
            {
                GTLHotelEndpointHotelUrl * wrapper = (GTLHotelEndpointHotelUrl *)[Bookingurl objectAtIndex:i];
                if([BookingUrlsArray[j] isEqualToString:wrapper.name])
                {
                    pageurl = wrapper.url;
                }
                
            }
            
            page.aURLStringToOpen = pageurl;

            [self.navigationController pushViewController:page animated:YES];
            
            
            
        }];
        // [action setValue:[[UIImage imageNamed:@"USA16.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
        [alertController addAction:action];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                         }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

-(void) connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // loads all the images if there is a location in those images search for location
    if (locationBOOL == NO) {
        NSError *error;
        ImageProperties *properties;
        tempArray = [[NSMutableArray alloc]init];
        
        NSString *string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //NSLog(@"instagram json :::: %@", json);
        NSArray *firstPic = [[[json objectForKey:@"data"]valueForKey:@"images"]valueForKey:@"standard_resolution"];
        
        for (NSDictionary *testDict in firstPic) {
            
            properties = [[ImageProperties alloc] init];
            
            properties.url = [NSURL URLWithString:[testDict objectForKey:@"url"]];
            properties.instagramPic = YES;
            properties.caption = [NSString stringWithFormat: @"#%@",[DataArray valueForKey:@"name"]];
            properties.user = @"InstagramUSER";
            properties.isVideo=NO;
            [tempArray addObject:properties];
            
        }
        
        
    }
    else {
        
        
        ImageProperties *properties;
        
        tempArray = [[NSMutableArray alloc]init];
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        NSArray *firstPic = [[[json objectForKey:@"data"]valueForKey:@"images"]valueForKey:@"standard_resolution"];
        
        for (NSDictionary *testDict in firstPic) {
            
            properties = [[ImageProperties alloc] init];
            
            properties.url = [NSURL URLWithString:[testDict objectForKey:@"url"]]; //1
            properties.instagramPic = YES;
            properties.caption = [NSString stringWithFormat: @"#%@",[DataArray valueForKey:@"name"]];
            properties.user = @"InstagramUSER";
            properties.isVideo=NO;
            [tempArray addObject:properties];
            
            
            
        }
        
    }
    
    responseData = nil;
    
    
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    responseData = nil;
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void) getInstagramFeed {
    
    NSString * str = [DataArray valueForKey:@"name"];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *requestUrl = [NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?client_id=%@&count=20",str, KCLIENTID];
    NSString* encodedStrin = [requestUrl stringByAddingPercentEscapesUsingEncoding:
                              NSUTF8StringEncoding];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:encodedStrin]];
    (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void) getYoutubeFeed{

    NSString * str = [DataArray valueForKey:@"name"];
    [self searchYoutubeVideosForTerm:str];
}

-(void)searchYoutubeVideosForTerm:(NSString*)term
{
    //NSLog(@"Searching for '%@' ...", term);
    
    //URL escape the term
    term = [term stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //make HTTP call
    NSString* searchCall = [NSString stringWithFormat:@"http://gdata.youtube.com/feeds/api/videos?q=%@&max-results=50&alt=json", term];
    NSString* encodedStrin = [searchCall stringByAddingPercentEscapesUsingEncoding:
                              NSUTF8StringEncoding];
    
    [JSONHTTPClient getJSONFromURLWithString: encodedStrin
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      
                                      //got JSON back
                                      //NSLog(@"Got JSON from web: %@", json);
                                      
                                      if (err) {
                                          NSLog(@"Network Error!");
                                          return;
                                      }
                                      
                                      //initialize the models
                                    NSArray *  videos = [VideoModel arrayOfModelsFromDictionaries:
                                                json[@"feed"][@"entry"]
                                                ];
                                      
                                   
                                  }];
}

- (void) getTwitterFeed{

    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    
    NSString * str = [DataArray valueForKey:@"name"];
    NSString * encoded = [str stringByReplacingOccurrencesOfString:@" " withString:@""];

    
    //handle network requests here
    [[Twitter sharedInstance] logInGuestWithCompletion:^
     (TWTRGuestSession *session, NSError *error) {
         if (session) {
             // make API calls that do not require user auth
             //NSLog(@"TWitter logged in");
             NSString *statusesShowEndpoint = [NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json?q=%@",encoded];
             NSString* encodedStrin = [statusesShowEndpoint stringByAddingPercentEscapesUsingEncoding:
                                       NSUTF8StringEncoding];
             
             NSDictionary *params = @{};
             NSError *clientError;
             NSURLRequest *request = [[[Twitter sharedInstance] APIClient]
                                      URLRequestWithMethod:@"GET"
                                      URL:encodedStrin
                                      parameters:params
                                      error:&clientError];
             
             if (request) {
                 [[[Twitter sharedInstance] APIClient]
                  sendTwitterRequest:request
                  completion:^(NSURLResponse *response,
                               NSData *data,
                               NSError *connectionError) {
                      if (data) {
                          // handle the response data e.g.
                          NSError *jsonError;
                          NSDictionary *json = [NSJSONSerialization
                                                JSONObjectWithData:data
                                                options:0
                                                error:&jsonError];
                          
                          
                          NSArray *statuses=[json objectForKey:@"statuses"];
                          for (NSDictionary *status in statuses) {
                              NSDictionary * entity=[status valueForKey:@"entities"];
                              NSDictionary * medias=[entity valueForKey:@"media"];
                              for (NSDictionary * media in medias){
                                  NSString *type=[media valueForKey:@"type"];
                                  if([type isEqualToString:@"photo"])
                                  {
                                      NSLog(@"%@",[media valueForKey:@"media_url"]);
                                      
                                      [TwitterArray addObject:[media valueForKey:@"media_url"]];

                                  }
                              }
                          }
                          
                          [tableview reloadData];
                          
                          [self getInstagramFeed];
                          
                          [HUD hide:YES];
                          [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                      
                      }
                      else {
                          NSLog(@"Error: %@", connectionError);
                          [HUD hide:YES];
                          [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                      }
                  }];
             }
             else {
                 NSLog(@"Error: %@", clientError);
                 
                 [HUD hide:YES];
                 [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
             }
         } else {
             NSLog(@"error: %@", [error localizedDescription]);
             [HUD hide:YES];
             [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
         }
     }];
    
    
}
- (void) getVineFeed{

    
    NSString * str = [DataArray valueForKey:@"name"];

    //URL escape the term
    NSString* term = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //term=@"Hawaii";
    //make HTTP call
    NSString* searchCall = [NSString stringWithFormat:@"https://api.vineapp.com/timelines/tags/%@", term];
    
    [JSONHTTPClient getJSONFromURLWithString: searchCall
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      
                                      //got JSON back
                                      //NSLog(@"Got JSON from web: %@", json);
                                      
                                      if (err) {
                                          NSLog(@"Network Error!");
                                          return;
                                      }
                                      
                                      //initialize the models
                                      videos = json[@"data"][@"records"];
                                      
                                      if (videos) NSLog(@"Loaded successfully vine models");
                                      
                                      //add the thumbnails
                                      ImageProperties *properties;
                                      tempArray = [[NSMutableArray alloc]init];
                                      for (int i=0;i<videos.count;i++) {
                                          
                                          //get the data
                                          NSDictionary* video = videos[i];
                                          VineVideoModel *vinev=[[VineVideoModel alloc] init];
                                          vinev.title=video[@"description"];
                                          vinev.videoURL=[NSURL URLWithString:video[@"videoUrl"]];
                                          vinev.videoThumb=[NSURL URLWithString:video[@"thumbnailUrl"]];
                                          
                                          
//                                          //create a property
//                                          properties.videoType=@"Vine";
//                                          properties = [[ImageProperties alloc] init];
//                                          properties.url = vinev.videoThumb;
//                                          properties.instagramPic = YES;
//                                          properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
//                                          properties.user = @"VineUSER";
//                                          properties.vineVideo = vinev;
//                                          properties.isVideo=YES;
//                                          [tempArray addObject:properties];
                                          
                                      }
                                      //NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                                   //   [propertiesArray addObjectsFromArray:tempArray];
                                   //   [realTableView reloadData];
                                  }];
    
}

-(void)OpenTwitterFeeds
{
    [Photos removeAllObjects];
    
    
    for(int i=0 ; i < TwitterArray.count ; i++)
    {
        
        [Photos addObject:[MWPhoto photoWithURL:[NSURL URLWithString:[TwitterArray objectAtIndex:i]]]];
        
    }
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    // Customise selection images to change colours if required
    browser.customImageSelectedIconName = @"ImageSelected.png";
    browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:0];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];

}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
