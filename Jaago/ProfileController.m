//
//  ProfileController.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/16/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "ProfileController.h"
#import <Parse/Parse.h>


#import "CustomLogInViewController.h"
#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
#import "GTLCustomerLoginCustomerProfile.h"
#import "GTLQueryCustomerLogin.h"
#import "GTLServiceCustomerLogin.h"
#import "GTLCustomerLoginImageCollection.h"
#import "GTLCustomerLoginImage.h"
#import "UIImageView+WebCache.h"
#import "UserFeedsViewController.h"

@interface ProfileController ()

@end

@implementation ProfileController

@synthesize nameLabel, profileImage;
@synthesize tableview;
static GTLServiceCustomerLogin *service = nil;
int sel = -1;



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    PFUser *test = [PFUser currentUser];
    
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userdata"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    
    if (dictionary) {
        
        GTLCustomerLoginCustomerProfile * dict = (GTLCustomerLoginCustomerProfile *) dictionary;
        

            NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Montserrat-Regular" size:15.0],NSFontAttributeName, [UIColor whiteColor],NSForegroundColorAttributeName, nil];
            
            self.navigationController.navigationBar.titleTextAttributes = size;
            // Do any additional setup after loading the view.
            nameLabel.text = dict.cUsername.uppercaseString;
            profileImage.image = [UIImage imageNamed:@"user"];

        
        
         [self CallAPI];
    }
    else {
        if (!test) { // No user logged in
            
            [self performSegueWithIdentifier:@"customLogIn" sender:self];
            
            [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];

            
        }
        else {
            
            
        }

    }
}

-(void)CallAPI
{
    [ProfileArray removeAllObjects];
    
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userdata"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    NSString * UserID;
    
    if (dictionary) {
        
        GTLCustomerLoginCustomerProfile * dict = (GTLCustomerLoginCustomerProfile *) dictionary;
        UserID = [NSString stringWithFormat:@"%@",dict.cUID];
        
        
    }
    
    GTLQueryCustomerLogin *query = [GTLQueryCustomerLogin queryForGetImageWithUserId:[UserID longLongValue]];
    [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLCustomerLoginImageCollection *object, NSError *error) {
        
        if(error == nil)
        {
            
            NSArray *items = [object items];
            [ProfileArray addObjectsFromArray:items];
            
            if([ProfileArray count])
            {
            
                [tableview reloadData];
            
            myTimerName = [NSTimer scheduledTimerWithTimeInterval: 5.0
                                                           target:self
                                                         selector:@selector(handleTimer:)
                                                         userInfo:nil
                                                          repeats:YES];
            }
            
        }
        else
        {

            
            
        }
        
        // Do something with items.
    }];
    
    
}

-(void)handleTimer: (id) sender
{
    //Update Values in Label here
    
    if(sel == [ProfileArray count]-1)
    {
        sel = -1 ;
    }
    
    sel++;
    
    GTLCustomerLoginImage * ImageWrapper = (GTLCustomerLoginImage *)[ProfileArray objectAtIndex:sel];
    NSLog(@"%@",ImageWrapper.identifier);
    
    
    NSString *image1 =[NSString stringWithFormat:@"%@",ImageWrapper.identifier];
    
    if (![image1 isEqualToString:@""])
    {
        [profileImage sd_setImageWithURL:[NSURL URLWithString:ImageWrapper.sURL]
                          placeholderImage:[UIImage imageNamed:@"no_img_sq.png"]];

    }
    else
    {
        UIImage *imgno = [UIImage imageNamed:@"no_img_sq.png"];
        [profileImage setImage:imgno];
    }

    
    
    
    
}

-(void)stopTimer: (id) sender
{
    if(myTimerName)
    {
        [myTimerName invalidate];
        myTimerName = nil;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:true];
    
    [self stopTimer:myTimerName];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"ProfileController viewDidLoad");
    
    ProfileArray = [[NSMutableArray alloc]init];
    

    if (!service) {
        service = [[GTLServiceCustomerLogin alloc] init];
        service.retryEnabled = YES;
        
    }
    
   
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* CellIdentifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    switch (indexPath.row) {
        cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

        case (0):
            cell.textLabel.text = [NSString stringWithFormat:@"Photos (%lu)",(unsigned long)[ProfileArray count]];
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"icon_photos.png"];
            break;
        case (1):
            cell.textLabel.text = @"Booked";
            cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

            cell.imageView.image = [UIImage imageNamed:@"Icon_booked.png"];

            break;
        case (2):
            cell.textLabel.text = @"Comments";

        default:
            break;
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
    if (indexPath.row == 0 ) {
        
        if([ProfileArray count])
        {
        UserFeedsViewController * feedspage = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"userfeeds"];
        feedspage.DataArr = [[NSMutableArray alloc]initWithArray:ProfileArray];
        [self.navigationController pushViewController:feedspage animated:YES];
        }
        
        
    }
    else if (indexPath.row == 1) {
        
        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:@"Travomi" message:@"There is no booked history" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}



- (IBAction)logout:(id)sender {
    [PFUser logOut];
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}
         
@end
