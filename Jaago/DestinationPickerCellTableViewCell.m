//
//  DestinationPickerCellTableViewCell.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/20/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "DestinationPickerCellTableViewCell.h"



#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
@implementation DestinationPickerCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
