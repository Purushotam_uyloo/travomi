//
//  ChooseWhereViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/21/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseWhereViewController : UIViewController 

- (IBAction)camera:(UIButton *)sender;
- (IBAction)existing:(UIButton *)sender;
- (IBAction)video:(UIButton *)sender;

@end
