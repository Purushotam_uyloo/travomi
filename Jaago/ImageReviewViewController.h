//
//  ImageReviewViewController.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/4/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//


// controller for adding filters to the photo to upload

#import <UIKit/UIKit.h>

@interface ImageReviewViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate>

@property UIImage *image;
@property (strong, nonatomic)NSString *type;

@property (strong, nonatomic) IBOutlet UIImageView *takenPhoto;
- (IBAction)editPressed:(UIButton *)sender;
- (IBAction)cancelPressed:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UISlider *toneSlider;
@property (weak, nonatomic) IBOutlet UISlider *brightenSlider;
@property (weak, nonatomic) IBOutlet UISlider *vibranceSlider;
@property (weak, nonatomic) IBOutlet UISlider *luminaceSlider;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *useButton;

@property (weak, nonatomic) IBOutlet UIView *sliderView;

@end
