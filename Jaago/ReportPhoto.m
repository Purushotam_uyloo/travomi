//
//  ReportPhoto.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 11/15/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "ReportPhoto.h"
#import "ReportStatus.h"
#import "BlockUser.h"
#import "ReportSpam.h"
#import <Parse/Parse.h>
#import "MBProgressHUD.h"

@interface ReportPhoto ()

@end

@implementation ReportPhoto

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"reportFinished"]){
        ReportStatus* controller = segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"reportSpam"]){
            ReportSpam* controller = segue.destinationViewController;
    }
    else if([segue.identifier isEqualToString:@"blockUser"]){
            BlockUser* controller = segue.destinationViewController;
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
}

- (void) sendReportForType:(NSString *) type WithCompletionBlock:(void (^)(BOOL succeeded, NSError *error)) completionblock{
    PFObject *reportObject = [PFObject objectWithClassName:@"Reports"];
    reportObject[@"name"] = self.name;
    reportObject[@"type"] = type;
    [reportObject saveInBackgroundWithBlock:completionblock];
}



- (IBAction)itemOnePressed:(id)sender {
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    [self performSelector:@selector(RemoveLoader) withObject:nil afterDelay:0.9];

    
//    [self sendReportForType:@"The picture contains inappropriate content" WithCompletionBlock:^(BOOL succeeded, NSError *error){
//        if (succeeded) {
//            [self performSegueWithIdentifier:@"reportFinished" sender:self];
//        } else {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error sending report" message:error.description preferredStyle:UIAlertControllerStyleAlert];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }];
    

}

- (IBAction)itemTwoPressed:(id)sender {
   
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    [self performSelector:@selector(RemoveLoader) withObject:nil afterDelay:0.9];

    
    
//    [self sendReportForType:@"This is a spam" WithCompletionBlock:^(BOOL succeeded, NSError *error){
//        if (succeeded) {
//            [self performSegueWithIdentifier:@"reportSpam" sender:self];
//        } else {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error sending report" message:error.description preferredStyle:UIAlertControllerStyleAlert];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }];
}

- (IBAction)itemThreePressed:(id)sender {
    
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    [self performSelector:@selector(RemoveLoader) withObject:nil afterDelay:0.9];
    
//    [self sendReportForType:@"Block user that uploads this photo" WithCompletionBlock:^(BOOL succeeded, NSError *error){
//        if (succeeded) {
//            [self performSegueWithIdentifier:@"blockUser" sender:self];
//        } else {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error sending report" message:error.description preferredStyle:UIAlertControllerStyleAlert];
//            [self presentViewController:alert animated:YES completion:nil];
//        }
//    }];

}

-(void)RemoveLoader
{
    [HUD hide:YES];
    [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
    
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Travomi" message:@"This photo has been reported by you.Admin will take proper action on this as soon as possible" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [alert show];
      [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)cencelPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController setNavigationBarHidden:NO];
}
@end
