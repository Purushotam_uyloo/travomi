//
//  LandingPageResultViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/1/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "LandingPageResultViewController.h"
#import <QuartzCore/QuartzCore.h> 
#import "WebVideoViewController.h"
//#import "HotelDetailsViewController.h"
#import "HotspotListViewController.h"
#import "HotelPickerViewController.h"
#import "BookingWebViewViewController.h"
#import "CommentsController.h"



#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
#import "ReportPhoto.h"
@interface LandingPageResultViewController ()

@property NSString * bookingEngineName;



@end

Boolean travomiUser;
@implementation LandingPageResultViewController

//@synthesize navigationItem, user,image,caption,likeButton,viewCommentsButton, tableCaption, tableImage,nonUserLabel, tableUser, selectedImageProperties,videoButton, bookButton;
@synthesize  tableImage, tableCaption, tableUser,selectedImageProperties, image,videoButton, segueTable;


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (travomiUser) {
        NSLog(@"before y = %f", segueTable.frame.origin.y);
        CGRect frame = segueTable.frame;
        NSLog(@"before frame y = %f", frame.origin.y);
        
        frame.origin.y -= 45;
        NSLog(@"after frame y = %f", frame.origin.y);
        segueTable.frame =  CGRectMake( 100, 200, segueTable.frame.size.width, segueTable.frame.size.height ) ;


        
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.reportView setHidden:YES];

    travomiUser = false;
    self.title = _controllerTitle;

    [image setImage:[UIImage imageNamed:_selectedImage]];
    image.contentMode = UIViewContentModeScaleAspectFill;
    
    /*
    if([selectedImageProperties.user isEqualToString:@"InstagramUSER"] || [selectedImageProperties.user isEqualToString:@"TwitterUSER"]  ) {
    //    nonUserLabel.text = selectedImageProperties.user;
    //    [user setHidden:TRUE];
   //     [nonUserLabel setHidden:YES];
////[viewCommentsButton setHidden:YES];
   //     [likeButton setHidden:YES];
        [videoButton setHidden:YES];
    }
    if( [selectedImageProperties.user isEqualToString:@"YoutubeUSER"] || [selectedImageProperties.user isEqualToString:@"VineUSER"]){
        nonUserLabel.text = selectedImageProperties.user;
        [user setHidden:TRUE];
        [nonUserLabel setHidden:YES];
        [viewCommentsButton setHidden:YES];
        [likeButton setHidden:YES];
        
    }
    else {
        
        NSLog(@"Parse user: %@",tableUser);

        if(selectedImageProperties.isUserVideo==NO){
            [videoButton setHidden:YES];
        }
        //[nonUserLabel setHidden:TRUE];
        //[user setTitle:selectedImageProperties.user forState:UIControlStateNormal];
        NSLog(@"video data length: %lu",selectedImageProperties.videoData.length);
    }
    
    if (selectedImageProperties.travomiPost) {
        travomiUser = true;
        segueTable.frame =  CGRectMake( 100, 200, segueTable.frame.size.width, segueTable.frame.size.height ) ;
    }

    NSLog(@"what is travomiuser? :%hhu", travomiUser);
     */
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showVideo"]){
        WebVideoViewController* controller = segue.destinationViewController;
        if(!selectedImageProperties.isUserVideo){
            if([selectedImageProperties.videoType isEqualToString:@"Youtube"]){
                NSLog(@"Youtube video received");
                controller.youtubevideo = selectedImageProperties.videos;
                controller.videotype = @"Youtube";
            }else{
                NSLog(@"Vine video received");
                controller.vineVideo = selectedImageProperties.vineVideo;
                controller.videotype = @"Vine";
            }
        }else{
            NSLog(@"User Video received");
            controller.uservideodata=selectedImageProperties.videoData;
            controller.videotype=@"user";
        }
    }else if([segue.identifier isEqualToString: @"hotspots"]){
        HotspotListViewController* controller = segue.destinationViewController;
        
        NSString* thishotelname = @"Koloalanding";
        controller.islandPicked = thishotelname;
        controller.islandHotels = [NSArray arrayWithObject:thishotelname];
        controller.title=thishotelname;
        
    }else if([segue.identifier isEqualToString: @"bookPage"]){
        
        BookingWebViewViewController* destinationController = [segue destinationViewController];
        destinationController.aURLStringToOpen = (NSString *)sender;
        destinationController.bookingEngineName = self.bookingEngineName;
        destinationController.searchKeyWord = @"Grandhyattkauai";
        
    }else if([segue.identifier isEqualToString: @"comments"]){
        CommentsController *destinationController = [segue destinationViewController];
        destinationController.hideDelete = true;
        destinationController.property = self.selectedImageProperties;
        
    }else if([segue.identifier isEqualToString: @"toReport"]){
        ReportPhoto *destinationController = [segue destinationViewController];
        destinationController.hidesBottomBarWhenPushed = YES;
        destinationController.name = [self.selectedImageProperties.caption stringByAppendingFormat:@" %@", self.selectedImageProperties.photoID ];
        [destinationController.navigationController setNavigationBarHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (travomiUser) {
        return 3;
    } else {
        return 2;
    }
    
}

- (NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.font =[UIFont fontWithName:@"Montserrat-Regular" size:15.0];

    if (indexPath.row == 0) {
        cell.textLabel.text = @"Hotspots Nearby";
        cell.imageView.image = [UIImage imageNamed:@"hot_spots_small_icon.png"];
    
    }
    else if (indexPath.row == 1) {
        cell.textLabel.text = @"Book Now";
        cell.imageView.image = [UIImage imageNamed:@"Icon_booked.png"];

    }
    else if (indexPath.row ==  2) {
        cell.textLabel.text = @"Comments";
        cell.imageView.image = [UIImage imageNamed:@"Icon_comment.png"];

        
    }
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; 
    if (indexPath.row == 0){
        [self performSegueWithIdentifier:@"hotspots" sender:self];
    }
    else if (indexPath.row == 1){
        [self openURL:nil];

    }
    else if (indexPath.row == 2){
        [self performSegueWithIdentifier:@"comments" sender:self];
    }
}

- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}

#pragma mark - private methods

- (void) openURL:(id)sender{
    //sample hotel search URL for Expedia: http://www.expedia.com/Hotel-Search?#&destination=Kapalua
    
    NSString * destinationString = @"Grandhyattkauai";
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction * expediaAction = [UIAlertAction actionWithTitle:@"Expedia" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString * URLString = @"https://www.expedia.co.in/San-Luis-Obispo-Hotels-Dolphin-Cove-Motel.h9400798.Hotel-Information";
        if ([_selectedImage containsString:@"Inn"]) {
            URLString = @"https://www.expedia.co.in/San-Simeon-Hotels-Courtesy-Inn-San-Simeon.h7755.Hotel-Information";
        }
        self.bookingEngineName = @"Expedia";
        [self performSegueWithIdentifier:@"bookPage" sender:URLString];
        //        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:URLString]];
    }];
    UIAlertAction * hotelsAction = [UIAlertAction actionWithTitle:@"Hotels.com" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
        NSString * URLString = @"https://in.hotels.com/ho480093/dolphin-cove-motel-pismo-beach-united-states-of-america/";
        if ([_selectedImage containsString:@"Inn"]) {
            URLString = @"https://in.hotels.com/ho218103/courtesy-inn-san-simeon-san-simeon-united-states-of-america/";
        }
        self.bookingEngineName = @"Hotels.com";
        [self performSegueWithIdentifier:@"bookPage" sender:URLString];
        //        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:URLString]];
    }];
    
    //sample URL:http://www.travelocity.com/Hotel-Search?#&destination=Maui
    UIAlertAction * tvAction = [UIAlertAction actionWithTitle:@"Travelocity" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString * URLString = @"https://www.travelocity.com/San-Luis-Obispo-Hotels-Dolphin-Cove-Motel.h9400798.Hotel-Information";
        if ([_selectedImage containsString:@"Inn"]) {
            URLString = @"https://www.travelocity.com/San-Simeon-Hotels-Courtesy-Inn-San-Simeon.h7755.Hotel-Information";
        }
        self.bookingEngineName = @"Travelocity";
        [self performSegueWithIdentifier:@"bookPage" sender:URLString];
        //        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:URLString]];
    }];
    
    UIAlertAction * bookingAction = [UIAlertAction actionWithTitle:@"Booking.com" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSString * URLString = @"http://www.booking.com/hotel/us/dolphin-cove-motel.html";
        if ([_selectedImage containsString:@"Inn"]) {
            URLString = @"http://www.booking.com/hotel/us/courtesy-inn-san-simeon.html";
        }
        self.bookingEngineName = @"Booking.com";
        [self performSegueWithIdentifier:@"bookPage" sender:URLString];
    }];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Choose where to book" message:@"currently supported websites:" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [alert addAction:cancelAction];
    [alert addAction:expediaAction];
    [alert addAction:tvAction];
    [alert addAction:hotelsAction];
    [alert addAction:bookingAction];
    
    //here will be the website for the specified hotel
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"StaticImages"];
    [photoObjectQuery whereKey:@"key" equalTo:destinationString];
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            id hoteldetail = [objects objectAtIndex:0];
            if ([hoteldetail objectForKey:@"website"]){
                NSString * website = [hoteldetail objectForKey:@"website"];
                UIAlertAction * officialwebsiteAction = [UIAlertAction actionWithTitle:@"Official Website" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                    self.bookingEngineName = @"Official Website";
                    [self performSegueWithIdentifier:@"bookPage" sender:website];
                }];
                [alert addAction:officialwebsiteAction];
            } else {
                NSLog(@"%@ doesn't have official website uploaded yet!", destinationString);
            }
        }
        else {
            NSLog(@"Official Website :::error");
        }
    }];

    
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


- (IBAction)reportButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"toReport" sender:self];

}

- (IBAction)flagPressed:(id)sender {
    [self.reportView setHidden:!self.reportView.isHidden];
    
}
@end
