//
//  BlockUser.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 11/15/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockUser : UIViewController
- (IBAction)cancelPressed:(id)sender;
- (IBAction)itemPressed:(id)sender;

@end
