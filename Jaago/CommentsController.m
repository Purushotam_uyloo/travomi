//
//  CommentsController.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/21/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "CommentsController.h"
#import "ImageOnlyCell.h"
#import "CommentViewCell.h"



#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"

@interface CommentsController () {
    UITextField *test;
    __weak IBOutlet UIImageView *imgView;
}

@end

@implementation CommentsController

@synthesize commentTable, textArray, commentView, commentField, optionsView;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [optionsView setHidden:YES];
    [commentView setHidden:YES];

    if (self.hideDelete == true) {
        [self.deleteButton setHidden:YES];
    }
    self.test.image = self.property.image;
    [imgView setImage:self.property.image];
    textArray = [[NSMutableArray alloc] init];
    NSLog(@"self.property.photoId: %@", self.property.id);
    NSLog(@"caption: %@", self.property.caption);
    PFQuery *query = [PFQuery queryWithClassName:@"Notification"];
    [query includeKey:@"creator"];
    [query whereKey:@"notificationType" equalTo:@"comment"];
    [query whereKey:@"photoID" equalTo:self.property.id];
    [query orderByAscending:@"createdAt"];

    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!error && objects.count>0){
                for (PFObject *object in objects) {
                    
                    [textArray addObject:object];
                }
                [imgView setImage:self.property.image];
                [commentTable reloadData];
            }
        });
        
    }];
    //self.commentTable.rowHeight = UITableViewAutomaticDimension;
    //self.commentTable.estimatedRowHeight = 200;
    NSLog(@"textArray: %@", textArray);
    [commentTable reloadData];
    // Do any additional setup after loading the view.
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  
//    if (indexPath.row == 0)
//        return 321;
    if (indexPath.row == 0) {
        NSString * yourText = [NSString stringWithFormat:@"%@", self.property.caption];
        CGFloat bust = [self heightForTextViewRectWithWidth:280 andText:yourText];
        NSLog(@"bust: %f", bust);
        return bust + 30;
    }
    else {
        PFObject *object = [self.textArray objectAtIndex:indexPath.row-1];
        NSString * yourText = [NSString stringWithFormat:@"%@", object[@"comment"] ];
        CGFloat bust = [self heightForTextViewRectWithWidth:280 andText:yourText];
        NSLog(@"bust: %f", bust);
        return bust + 30;
    }

}
-(CGFloat)heightForTextViewRectWithWidth:(CGFloat)width andText:(NSString *)text
{
    UIFont * font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
    
    // this returns us the size of the text for a rect but assumes 0, 0 origin
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName: font}];
    
    // so we calculate the area
    CGFloat area = size.height * size.width;
    
    CGFloat buffer = 30.0f;
    // and then return the new height which is the area divided by the width
    // Basically area = h * w
    // area / width = h
    // for w we use the width of the actual text view

    return floor(area/width) + buffer;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"number of rows %ld",[textArray count]+1);
    return [textArray count] + 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"hello there");
//    if (indexPath.row == 0 ) {
//        NSString *cellIdentifier = @"ImageOnlyCell";
//        ImageOnlyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//
//        if (cell == nil)
//            cell = [[ImageOnlyCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//        [cell.image setImage:self.property.image];
//
//        return cell;
//    }
     if (indexPath.row == 0 ) {
        NSString *cellIdentifier = @"CommentCell";
        
        CommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil) {
            cell = [[CommentViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.user.font = [UIFont fontWithName:@"Montserrat-Bold" size:15];
        cell.user.text = self.property.user;
        cell.comment.font = [UIFont fontWithName:@"Montserrat-Regular" size:15];
        cell.comment.text = [NSString stringWithFormat:@"%@", self.property.caption];
        CGRect frame = cell.comment.frame;
        frame.size.height = [self heightForTextViewRectWithWidth:self.view.frame.size.width-40 andText:cell.comment.text];
        NSLog(@"height: %f", frame.size.height);

        cell.comment.frame = frame;
        //[cell.textLabel setText:[textArray objectAtIndex:indexPath.row]];
        return cell;
    }
    else {
        NSString *cellIdentifier = @"CommentCell";
        
        CommentViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil) {
            cell = [[CommentViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        PFObject *object = [self.textArray objectAtIndex:indexPath.row-1];
        
        cell.user.font = [UIFont fontWithName:@"Montserrat-Bold" size:15];
       // NSLog(@"%@",object[@"creator"][@"username"]);
        
        
        cell.user.text = object[@"creator"][@"username"];
        
        
        cell.comment.font = [UIFont fontWithName:@"Montserrat-Regular" size:14];
        cell.comment.text = [NSString stringWithFormat:@"%@", object[@"comment"]];
        CGRect frame = cell.comment.frame;
        frame.size.height = [self heightForTextViewRectWithWidth:280 andText:cell.comment.text];
        NSLog(@"height: %f", frame.size.height);
        
        cell.comment.frame = frame;
        //[cell.textLabel setText:[textArray objectAtIndex:indexPath.row]];
        return cell;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"gets here");
    if ([textView.text isEqualToString:@"Write Text Here"]) {
        commentField.text = @"";
    }
}


- (IBAction)postPressed:(id)sender {
    NSString *trimmedComment = [commentField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (trimmedComment.length !=0) {
        PFObject *comment = [PFObject objectWithClassName:@"Notification"];
        [comment setValue:trimmedComment forKey:@"comment"]; // Set comment text
        [comment setValue:[PFUser currentUser] forKey:@"creator"];
        [comment setValue:self.property.pfUser forKey:@"recipient"];
        [comment setValue:@"comment" forKey:@"notificationType"];
        [comment setValue:self.property.id forKey:@"photoID"];
        PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
        [ACL setPublicReadAccess:YES];
        comment.ACL = ACL;
        
        [comment saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Comment Posted" message:@"YAY" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
                [alertView show];
                [textArray addObject:comment];
                [commentTable reloadData];
                commentField.text = @"";
                [commentView setHidden:YES];
                [optionsView setHidden:YES];
                [self.view endEditing:TRUE];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Failed to Post" message:@"Please try posting the comment again" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil] ;
                [alertView show];
                NSLog(@"Error in submitting the comment");
            }
        }];
    }
    
}

- (IBAction)deletePressed:(id)sender {
    PFQuery *deletePhotoQuery = [PFQuery queryWithClassName:@"Photo"];
    [deletePhotoQuery whereKey:@"objectId" equalTo:self.property.id];
    [deletePhotoQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                NSLog(@"setting the active key to be 0....");
                [object setObject:@0 forKey:@"active"];
                [object save];
            }
            //[self.navigationController popViewControllerAnimated:YES];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
            
        }
    }];

}

- (IBAction)addCommentPressed:(id)sender {
    if (commentView.isHidden == YES) {
        [commentView setHidden:NO];
    }
    else {
        [commentView setHidden:YES];
    }
}
- (IBAction)cancelPressed:(id)sender {
    [commentView setHidden:YES];
    [optionsView setHidden:YES];
}

- (IBAction)optionsPressed:(id)sender {
    if (optionsView.isHidden == YES) {
        [optionsView setHidden:NO];
    }
    else {
        [optionsView setHidden:YES];
    }
}
@end
