//
//  ImageOnlyCell.m
//  Travomi
//
//  Created by Mark Anthony Corpuz on 10/8/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "ImageOnlyCell.h"

@implementation ImageOnlyCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
