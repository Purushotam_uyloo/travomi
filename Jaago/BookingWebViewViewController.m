//
//  BookingWebViewViewController.m
//  Travomi
//
//  Created by WangYinghao on 8/24/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "BookingWebViewViewController.h"

@interface BookingWebViewViewController ()

@property IBOutlet UIWebView * webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *webViewLoadingIndicator;


@end

@implementation BookingWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.webView setDelegate:self];
    if(self.aURLStringToOpen){
        NSLog(@"URL String: %@", self.aURLStringToOpen);
        NSURL * aURL = [NSURL URLWithString:self.aURLStringToOpen];
        NSURLRequest * request = [NSURLRequest requestWithURL:aURL];
        [self.webView loadRequest:request];

    }
}
- (IBAction)backPressed:(UIButton *)sender {
    [self.webView goBack];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(self.bookingEngineName){
        self.title = self.bookingEngineName;
        if([self.bookingEngineName isEqualToString:@"Hotels.com"]){
            [self.webViewLoadingIndicator setHidden:NO];
            [self.webViewLoadingIndicator startAnimating];
        } else {
            [self.webViewLoadingIndicator setHidden:YES];
        }
    }

}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
//    if([self.bookingEngineName isEqualToString:@"Hotels.com"]){
//        NSLog(@"begin loading js now");
//        NSString * searchFormInput = [NSString stringWithFormat:@"document.getElementById(\"qf-0q-destination\").value = \"%@\";",self.searchKeyWord ];
//        [self.webView stringByEvaluatingJavaScriptFromString:searchFormInput];
//        [self.webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName(\"button\")[3].click();"];
//        [self.webViewLoadingIndicator setHidden:YES];
//    }
    
}




@end
