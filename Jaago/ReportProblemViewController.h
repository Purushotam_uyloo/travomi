//
//  ReportProblemViewController.h
//  Travomi
//
//  Created by WangYinghao on 10/15/15.
//  Copyright © 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ReportProblemViewController : UIViewController<MFMailComposeViewControllerDelegate, UITextViewDelegate>

@end
