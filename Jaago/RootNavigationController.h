//
//  RootNavigationController.h
//  Travomi
//
//  Created by WangYinghao on 8/26/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootNavigationController : UINavigationController

@property NSDictionary * hotelNames;

@end
