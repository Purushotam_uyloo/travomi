//
//  CommentViewCell.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/29/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *user;
@property (strong, nonatomic) IBOutlet UITextView *comment;
@property (strong, nonatomic) IBOutlet UITextView *date;
- (IBAction)deleteButtonPressed:(UIButton *)sender;



@end
