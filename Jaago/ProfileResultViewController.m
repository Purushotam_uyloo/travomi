//
//  ProfileResultViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/11/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import "ProfileResultViewController.h"
#import <Parse/Parse.h>
#import "CommentViewController.h"



// Controller used for the profile.


#define LIKE 1
#define COMMENT 2
#define FOLLOW 3

@interface ProfileResultViewController ()
{
    BOOL liked;
    NSString *likedID;
    PFObject *likedObject;
}
@end

@implementation ProfileResultViewController

@synthesize selectedImage, selectedImageCaption,picture,caption,selectedImageProperties, likeButton, unlikeButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    unlikeButton.hidden = YES;
    liked = NO;
    PFQuery *likeQuery = [self didUserLike];
    [likeQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if (objects) {
                for (PFObject *object in objects) {
                    if (object) {
                        likedObject = object;
                        NSLog(@"there is something here");
                        liked = YES;
                        likedID = [object objectId];
                        unlikeButton.hidden = NO;
                        likeButton.hidden = YES;
                        NSLog(@"likedID %@", likedID);
                    }
                    
                }
            }
            else {
                NSLog(@"nothing?");
            }

        }
        else {
            NSLog(@"error getting initial like query");
        }
    }];
    //liked =
    NSLog(@"selected Image Properties. photoID %@", selectedImageProperties.photoID);

    [[self.caption layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.caption layer] setBorderWidth:0.3];
    [[self.caption layer] setCornerRadius:15];

    caption.text = selectedImageProperties.caption;
    [picture setImage: selectedImageProperties.image];


}


//querys what the user liked previously to display it (doesn't do anything at the moment)
- (PFQuery *) didUserLike{
    PFQuery *likeQuery = [PFQuery queryWithClassName:@"Notification"];
    
	[likeQuery whereKey:@"notificationType" equalTo:@"like"];
	[likeQuery whereKey:@"photoID" equalTo:selectedImageProperties.id];
	//[likeQuery whereKey:@"photoID" equalTo:selectedImageProperties.id];
    return likeQuery;
}
- (IBAction)comments:(UIButton *)sender {
    
}

- (IBAction)likePressed:(UIButton *)sender {
    likeButton.hidden = YES;
    
    
    PFObject *like = [PFObject objectWithClassName:@"Notification"];
    [like setObject:[PFUser currentUser] forKey:@"creator"];

    
    //[like setValue:trimmedComment forKey:@"comment"]; // Set comment text
    [like setValue:[PFUser currentUser] forKey:@"creator"];
    [like setValue:selectedImageProperties.pfUser forKey:@"recipient"];
    [like setValue:@"like" forKey:@"notificationType"];
    [like setValue:selectedImageProperties.id forKey:@"photoID"];
    //[comment setValue:currentUSER forKey:@"creator"];
    
    
    PFACL *ACL = [PFACL ACLWithUser:[PFUser currentUser]];
    [ACL setPublicReadAccess:YES];
    like.ACL = ACL;
    
    [like saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            likedObject = like;
            likedID = [like objectId];
            NSLog(@"likeID after pressing like %@, %@",likedID, [like objectId]);
            
            unlikeButton.hidden = NO;

        }
        else {
            NSLog(@"Error in submitting the like");
        }
    }];

}


//remove photo that user uploaded

- (IBAction)removePhoto:(UIButton *)sender {
    NSLog(@"%@",selectedImageProperties.id);
    
    PFQuery *deletePhotoQuery = [PFQuery queryWithClassName:@"Photo"];
    [deletePhotoQuery whereKey:@"objectId" equalTo:selectedImageProperties.id];
    [deletePhotoQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                NSLog(@"setting the active key to be 0....");
                [object setObject:@0 forKey:@"active"];
                [object save];
            }
            //[self.navigationController popViewControllerAnimated:YES];

            [self performSegueWithIdentifier:@"unwindAfterDelete" sender:self];

            
        }
    }];
    
}

- (IBAction)unlikePressed:(UIButton *)sender {
    
    unlikeButton.hidden = YES;
    [likedObject deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            likeButton.hidden = NO;

        }
    }];
     

}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"CommentSegue"]) {
        
        NSLog(@"selected Image Properties. photoID %@", selectedImageProperties.photoID);
        CommentViewController *controller = [segue destinationViewController];
        controller.photoProperties = self.selectedImageProperties;

    }
}



@end
