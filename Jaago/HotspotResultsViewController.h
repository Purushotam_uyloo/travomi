//
//  HotspotResultsViewController.h
//  Jaago
//
//  Created by WangYinghao on 3/15/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageProperties.h"

@interface HotspotResultsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property ImageProperties * property;
@property NSString * HotelName;


@end
