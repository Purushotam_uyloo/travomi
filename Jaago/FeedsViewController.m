//
//  FeedsViewController.m
//  Travomi
//
//  Created by admin on 3/11/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "FeedsViewController.h"
#import "GTLHotelEndpointHotel.h"
#import "GTLServiceHotelEndpoint.h"
#import "GTLQueryHotelEndpoint.h"
#import "GTLHotelEndpointImageCollection.h"
#import "ImageProperties.h"
#import "GTLHotelEndpointImage.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "TwitterfeedsTableViewCell.h"
#import "FeedsCollectionViewCell.h"
#import "HotelDetailViewController.h"
#import "ReportPhoto.h"
#import "UIImageView+WebCache.h"


@interface FeedsViewController ()

@end

@implementation FeedsViewController
@synthesize myCollectionView;
@synthesize HotelId;
@synthesize DataArr;
static GTLServiceHotelEndpoint *service = nil;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!service) {
        service = [[GTLServiceHotelEndpoint alloc] init];
        service.retryEnabled = YES;
        
    }
    
    FeedsArray = [[NSMutableArray alloc]init];

    
    NSArray * Images = (NSArray *)[self.DataArr valueForKey:@"gallery"];
    
    for(int i=0 ; i < Images.count ; i++)
    {
        GTLHotelEndpointImage * wrapper = (GTLHotelEndpointImage *)[Images objectAtIndex:i];
        
        ImageProperties * prop =[[ImageProperties alloc]init];
        prop.id = prop.id;
        if(wrapper.video == 0)
        {
            prop.isVideo = true;

        }
        else
        {
            prop.isVideo = false;

        }
        
        if(wrapper.video.intValue == 1)
        {
            prop.url = [NSURL URLWithString:wrapper.videoThumbnail];

        }
        else
        {
            prop.url = [NSURL URLWithString:wrapper.sURL];

        }

        
        [FeedsArray addObject:prop];
        
        
        
        
        //  [Photos addObject:ImageWrapper.sURL];
        
    }
    [myCollectionView reloadData];
    
    
    [self CallAPItoFetchImages];
    
    // Do any additional setup after loading the view.
}

-(void)CallAPItoFetchImages
{
    
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        HUD.delegate = self;
        [HUD show:YES];
    
    
    GTLQueryHotelEndpoint *query = [GTLQueryHotelEndpoint queryForGetHotelImagesWithHotelId:HotelId.longLongValue];
    [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLHotelEndpointImageCollection *object, NSError *error) {
        
        if(error == nil)
        {
            
            NSArray *items = [object items];
            
            for(int i = 0 ; i < [items count]; i ++)
            {
                GTLHotelEndpointImage * wrapper =[items objectAtIndex:i];
                
                ImageProperties * prop =[[ImageProperties alloc]init];
                prop.id = wrapper.identifier;
                if(wrapper.video.intValue == 1)
                {
                    prop.isVideo = true;
                    prop.videourl = [NSURL URLWithString:wrapper.sURL];
                }
                else
                {
                    prop.isVideo = false;
                    prop.videourl = [NSURL URLWithString:@""];
                    
                }
                if(wrapper.video.intValue == 1)
                {
                    if(wrapper.videoThumbnail != nil)
                    {
                        prop.url = [NSURL URLWithString:wrapper.videoThumbnail];

                    }
                    else
                    {
                         prop.url = @"";
                    }
                    

                }
                else
                {
                    prop.url = [NSURL URLWithString:wrapper.sURL];

                }
                
                
                [FeedsArray addObject:prop];
                

            }
            
            
          //  [myCollectionView reloadData];

            [HUD hide:YES];
            [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
            
            [self getTwitterFeed];
            
            
        }
        else
        {

                [HUD hide:YES];
                [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                        
        }
        
        // Do something with items.
    }];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    

}

- (void) getTwitterFeed{
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    
    NSString * str = [[DataArr valueForKey:@"name"] lowercaseString];
    NSString * encoded = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    //handle network requests here
    [[Twitter sharedInstance] logInGuestWithCompletion:^
     (TWTRGuestSession *session, NSError *error) {
         if (session) {
             // make API calls that do not require user auth
             //NSLog(@"TWitter logged in");
             NSString *statusesShowEndpoint = [NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json?q=%@",encoded];
             NSString* encodedStrin = [statusesShowEndpoint stringByAddingPercentEscapesUsingEncoding:
                                       NSUTF8StringEncoding];
             
             NSDictionary *params = @{};
             NSError *clientError;
             NSURLRequest *request = [[[Twitter sharedInstance] APIClient]
                                      URLRequestWithMethod:@"GET"
                                      URL:encodedStrin
                                      parameters:params
                                      error:&clientError];
             
             if (request) {
                 [[[Twitter sharedInstance] APIClient]
                  sendTwitterRequest:request
                  completion:^(NSURLResponse *response,
                               NSData *data,
                               NSError *connectionError) {
                      if (data) {
                          // handle the response data e.g.
                          NSError *jsonError;
                          NSDictionary *json = [NSJSONSerialization
                                                JSONObjectWithData:data
                                                options:0
                                                error:&jsonError];
                          
                          
                          NSArray *statuses=[json objectForKey:@"statuses"];
                          for (NSDictionary *status in statuses) {
                              NSDictionary * entity=[status valueForKey:@"entities"];
                              NSDictionary * medias=[entity valueForKey:@"media"];
                              for (NSDictionary * media in medias){
                                  NSString *type=[media valueForKey:@"type"];
                                  if([type isEqualToString:@"photo"])
                                  {
                                      NSLog(@"%@",[media valueForKey:@"media_url"]);
                                      
                                      ImageProperties * prop =[[ImageProperties alloc]init];
                                      prop.url = [NSURL URLWithString:[media valueForKey:@"media_url"]];
                                      prop.id = prop.id;
                                      prop.isVideo = NO;
                                      
                                      [FeedsArray addObject:prop];
                                      
                                  }
                              }
                          }
                          
                          [myCollectionView reloadData];
                          
                          [HUD hide:YES];
                          [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                          
                      }
                      else {
                          NSLog(@"Error: %@", connectionError);
                          [HUD hide:YES];
                          [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                      }
                  }];
             }
             else {
                 NSLog(@"Error: %@", clientError);
                 
                 [HUD hide:YES];
                 [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
             }
         } else {
             NSLog(@"error: %@", [error localizedDescription]);
             [HUD hide:YES];
             [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
         }
     }];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return FeedsArray.count;
}




- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FeedsCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.projImg.contentMode = UIViewContentModeScaleAspectFit;
   // cell.projImg.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0];
    
    cell.projImg.backgroundColor = [UIColor blackColor];
    ImageProperties * wrapper = (ImageProperties *)[FeedsArray objectAtIndex:indexPath.row];

    
    NSString *image1 =[NSString stringWithFormat:@"%@",wrapper.url];
    
    if(wrapper.isVideo == true)
    {
        [cell.playImg setHidden:false];
        
    }
    else
    {
        [cell.playImg setHidden:true];

    }
    
    if (![image1 isEqualToString:@""])
    {
        
//        UIActivityIndicatorView *spinnerCell = [[UIActivityIndicatorView alloc]
//                                                initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        spinnerCell.color=[UIColor darkGrayColor];
//        spinnerCell.frame=CGRectMake(cell.projImg.frame.size.width/2-10, cell.projImg.frame.size.height/2-10, 20, 20);
//        [cell.projImg addSubview:spinnerCell];
//        [spinnerCell startAnimating];
        
        [cell.projImg sd_setImageWithURL:wrapper.url
                     placeholderImage:[UIImage imageNamed:@"no_img_sq.png"]];
        
 
    }
    else
    {
        UIImage *imgno = [UIImage imageNamed:@"no_img_sq.png"];
        
        [cell.projImg setImage:imgno];
    }
    
    
   // [cell.projImg setBackgroundColor:[UIColor grayColor]];
    
   // [cell.reportbtn setTag:indexPath.row];
   // [cell.reportbtn addTarget:self action:@selector(reportBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;

}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(collectionView.frame)/2)-2,100);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    
        HotelDetailViewController * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"detail"];
        page.DataArray = self.DataArr;
        page.ImagesArray = [[NSMutableArray alloc]initWithArray:FeedsArray];
        page.selectedIndex = indexPath.row;
    
        [self.navigationController pushViewController:page animated:YES];
}

-(void)reportBtnClicked:(id)sender
{
    ReportPhoto * page =[self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"report"];
//    page.DataArray = self.DataArr;
//    page.ImagesArray = [[NSMutableArray alloc]initWithArray:FeedsArray];
//    page.selectedIndex = indexPath.row;
    
    [self.navigationController pushViewController:page animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
