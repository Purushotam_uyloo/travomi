//
//  ScratchPadViewController.m
//  Travomi
//
//  Created by Kumar, Gopesh on 10/10/16.
//  Copyright © 2016 MarkAnthonyCorpuz. All rights reserved.
//

#import "ScratchPadViewController.h"

@interface ScratchPadViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSUserDefaults *defaults;
    NSMutableArray *marrHotels;
}
@end

@implementation ScratchPadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    defaults = [NSUserDefaults standardUserDefaults];
    marrHotels = [[NSMutableArray alloc]init];
    [marrHotels addObjectsFromArray:[defaults objectForKey:@"savedHotels"]];
}

#pragma mark - UITABLEVIEW DELEGATE METHODS
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [marrHotels count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    cell.textLabel.text = [marrHotels objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    NSString * hotelName = [marrHotels objectAtIndex:indexPath.row];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:hotelName forKey:@"name"];

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"scratchpad" object:dict];
    
    [[tabBarController.viewControllers objectAtIndex:0] popToRootViewControllerAnimated:YES];

    
    [tabBarController setSelectedIndex:0];

}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [marrHotels removeObjectAtIndex:indexPath.row];
        [defaults setObject:marrHotels forKey:@"savedHotels"];
        [defaults synchronize];
        [tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
