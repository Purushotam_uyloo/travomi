//
//  ImageReviewViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 8/4/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//



// controller for adding filters to the photo to upload
#import "ImageReviewViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "PostImageViewController.h"
#import <Parse/Parse.h>
#import "UserVideo.h"


#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"
@import AVFoundation;

@interface ImageReviewViewController ()

@end

@implementation ImageReviewViewController
{
    BOOL cancelled, set;
    CIContext *context;
    CIFilter *filter;
    CIFilter *filter2;
    CIFilter *filter3;
    CIFilter *filtertest;
    CIImage *beginImage;
    CIImage *changedImage;
    UIImage *previousImage;
    NSData *imageData;
    NSMutableArray *islands;
    UserVideo *video;
}
@synthesize image, takenPhoto,type, editButton, cancelButton, useButton, saveButton, sliderView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    sliderView.hidden = true;
    cancelButton.hidden = true;
    saveButton.hidden = true;

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    
    if (cancelled && !set){
        [self.navigationController popViewControllerAnimated:YES];

    }
    else {
        imageData = UIImagePNGRepresentation(takenPhoto.image);
        beginImage = [CIImage imageWithData:imageData];
        changedImage = beginImage;
        context = [CIContext contextWithOptions:nil];
        filter = [CIFilter filterWithName:@"CISepiaTone"
                            keysAndValues:kCIInputImageKey, changedImage, @"inputIntensity",
                  @0.8, nil];
        
        
        filter2 = [CIFilter filterWithName:@"CIHighlightShadowAdjust" keysAndValues:kCIInputImageKey, changedImage, @"inputShadowAmount", 0.0, nil];
        
        filter3 = [CIFilter filterWithName:@"CIVibrance"
                             keysAndValues:kCIInputImageKey, changedImage,
                   @"inputAmount", 0.0,nil];
        
        filtertest = [CIFilter filterWithName:@"CISharpenLuminance"
         keysAndValues:kCIInputImageKey, beginImage,
         @"inputSharpness", 0 ,nil];
        
    }
}
- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"viewWillAppear:::::: type is now %@",self.type);
    if (!cancelled && !set){
        [self startCameraControllerFromViewController:self usingDelegate:self];
        
    }


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sliderValueChanged:(id)sender {
    
}



- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    NSLog(@"type is %@",self.type);
    if ([self.type  isEqual:@"camera"]) {
    
        NSLog(@"in the startCamera Controller");
        if (([UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypeCamera] == NO)
            || (delegate == nil)
            || (controller == nil)) {
            NSLog(@"exit camera mode");
            [takenPhoto setImage:[UIImage imageNamed:@"JaagoLogin.png"]];
            return NO;
        
        }
    
    
        UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
        cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose picture or
    // movie capture, if both are available:
    
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
    
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
        cameraUI.allowsEditing = YES;
    
        cameraUI.delegate = delegate;
    
        [controller presentViewController: cameraUI animated: YES completion:nil];
        
    }
    else  if([self.type  isEqual:@"video"]){
        NSLog(@"video:::in the startCamera Controller");
        if (([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypeCamera] == NO)
            || (delegate == nil)
            || (controller == nil)) {
            NSLog(@"exit video mode");
            [takenPhoto setImage:[UIImage imageNamed:@"JaagoLogin.png"]];
            return NO;
            
        }
        
        
        UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
        cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        // Displays a control that allows the user to choose picture or
        // movie capture, if both are available:
        
        NSArray *mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
      //  NSArray *videoMediaTypesOnly = [mediaTypes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(SELF contains %@)", @"movie"]];
       // [cameraUI setCameraCaptureMode:UIImagePickerControllerCameraCaptureModeVideo];
        
        cameraUI.mediaTypes =
        [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
        
       // cameraUI.mediaTypes = videoMediaTypesOnly;
        cameraUI.videoQuality = UIImagePickerControllerQualityTypeMedium;
        cameraUI.videoMaximumDuration = 10;
        
        
        // Hides the controls for moving & scaling pictures, or for
        // trimming movies. To instead show the controls, use YES.
        cameraUI.allowsEditing = YES;
        
        cameraUI.delegate = delegate;
        
        [controller presentViewController: cameraUI animated: YES completion:nil];
    
    }
    else{
        if (([UIImagePickerController isSourceTypeAvailable:
              UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
            || (delegate == nil)
            || (controller == nil)) {
            NSLog(@"exit library mode");
            [takenPhoto setImage:[UIImage imageNamed:@"JaagoImage.png"]];
            return NO;
            
        }
        
        
        UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
        cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        // Displays a control that allows the user to choose picture or
        // movie capture, if both are available:
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        // Hides the controls for moving & scaling pictures, or for
        // trimming movies. To instead show the controls, use YES.
        cameraUI.navigationBar.tintColor = [UIColor blackColor];
        cameraUI.allowsEditing = YES;
        cameraUI.delegate = delegate;
        [controller presentViewController: cameraUI animated: YES completion:nil];
    }
    return YES;

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    cancelled = YES;
    [self dismissViewControllerAnimated:YES completion:^{

    }];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    set = YES;
    
    if([self.type isEqual:@"video"]){
        NSLog(@"media type:%@",info[UIImagePickerControllerMediaType]);
        
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:info[UIImagePickerControllerMediaURL] options:nil];
        AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        gen.appliesPreferredTrackTransform = YES;
        CMTime time = CMTimeMakeWithSeconds(0.0, 600);
        NSError *error = nil;
        CMTime actualTime;
        CGImageRef imageref = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
        image= [[UIImage alloc] initWithCGImage:imageref];
        CGImageRelease(imageref);
        [takenPhoto setImage:image];
        NSLog(@"set thumbnail");
        
        video=[[UserVideo alloc] init];
        video.videoURL=info[UIImagePickerControllerMediaURL];
        video.videoThumb=image;
        
        video.videoData=[NSData dataWithContentsOfURL:video.videoURL];
        NSLog(@"AVAsset saved to NSData. length: %lu",(unsigned long)video.videoData.length);

        /*
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            video.videoData = [NSData dataWithContentsOfURL:video.videoURL];
            NSLog(@"AVAsset saved to NSData. length: %d",video.videoData.length);
                  }];
         */
    }else{
        image=[info objectForKey:UIImagePickerControllerEditedImage];
        [takenPhoto setImage:image];
        NSLog(@"set image");
        
    }
    [self dismissViewControllerAnimated:YES completion :nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PostImageViewController *controller = [segue destinationViewController];
    controller.image = takenPhoto.image;
    controller.type=type;
    controller.video=video;
    controller.hidesBottomBarWhenPushed = YES;
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)editPressed:(UIButton *)sender {
    [editButton setSelected: true];
    editButton.enabled = false;
    sliderView.hidden = false;
    useButton.hidden = true;
    saveButton.hidden = false;
    cancelButton.hidden = false;
    previousImage = takenPhoto.image;
    

    
    
}

- (IBAction)cancelPressed:(UIButton *)sender {
    [editButton setSelected: false];
    editButton.enabled = true;
    sliderView.hidden = true;
    useButton.hidden = false;
    saveButton.hidden = true;
    cancelButton.hidden = true;

    takenPhoto.image = previousImage;
}
- (IBAction)savePressed:(UIButton *)sender {
    [editButton setSelected: false];
    editButton.enabled = true;
    sliderView.hidden = true;
    useButton.hidden = false;
    saveButton.hidden = true;
    cancelButton.hidden = true;
}



//CIHighlightShawdowAdjust filter

- (IBAction)undoButtonPressed:(id)sender {
    NSLog(@"undobutton");
    takenPhoto.image = previousImage;
    
}



- (IBAction)toneSliderChanged:(UISlider *)slider {

    float slideValue = _toneSlider.value;
    [filter setValue:@(slideValue)forKey:@"inputIntensity"];
    
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgimg =
    [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    takenPhoto.image = newImage;
    imageData = UIImagePNGRepresentation(newImage);
    CGImageRelease(cgimg);
    
}

- (IBAction)brightenSliderChanged:(UISlider *)slider {
    NSLog(@"in brightenslider changed");
    float slideValue = _brightenSlider.value;
    [filter2 setValue:@(slideValue) forKey:@"inputShadowAmount"];

    CIImage *outputImage = [filter2 outputImage];
    
    CGImageRef cgimg =
    [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    // 3
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    takenPhoto.image = newImage;
    imageData = UIImagePNGRepresentation(newImage);
    CGImageRelease(cgimg);
}

- (IBAction)vibranceSliderChanged:(id)sender {
    
 
    float slideValue = _vibranceSlider.value;
    [filter3 setValue:@(slideValue) forKey:@"inputAmount"];
    CIImage *outputImage = [filter3 outputImage];
    CGImageRef cgimg =
    [context createCGImage:outputImage fromRect:[outputImage extent]];
    // 3
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    changedImage = (__bridge CIImage *)(cgimg);
    takenPhoto.image = newImage;
    imageData = UIImagePNGRepresentation(newImage);
    CGImageRelease(cgimg);
}

- (IBAction)liminanceSliderChanged:(id)sender {
    NSLog(@"fifthbutton");

    
    float slideValue = _luminaceSlider.value;
    [filtertest setValue:@(slideValue)forKey:@"inputSharpness"];
    
    CIImage *outputImage = [filtertest outputImage];
    CGImageRef cgimg =
    [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *newImage = [UIImage imageWithCGImage:cgimg];
    takenPhoto.image = newImage;
    imageData = UIImagePNGRepresentation(newImage);
    CGImageRelease(cgimg);
}
- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}



@end
