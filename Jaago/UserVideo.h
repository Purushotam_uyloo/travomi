//
//  UserVideo.h
//  Jaago
//
//  Created by WangYinghao on 2/12/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserVideo : NSObject

@property (strong, nonatomic) NSData *videoData;
@property (strong, nonatomic) UIImage *videoThumb;
@property (strong, nonatomic) NSURL *videoURL;

@end
