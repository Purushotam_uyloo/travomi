//
//  2014TableViewCell.h
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/21/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface _014TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *pictureView;
@property (nonatomic, weak) IBOutlet UILabel *number;

@end
