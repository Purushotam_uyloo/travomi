//
//  AmenitiesTableViewCell.m
//  Travomi
//
//  Created by admin on 2/5/17.
//  Copyright © 2017 MarkAnthonyCorpuz. All rights reserved.
//

#import "TwitterfeedsTableViewCell.h"
#import "GTLHotelEndpointAmenity.h"

@implementation TwitterfeedsTableViewCell
@synthesize Morebtn;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    backImgview.backgroundColor = [UIColor whiteColor];
    backImgview.layer.masksToBounds = NO;
    backImgview.layer.shadowColor = [UIColor blackColor].CGColor;
    backImgview.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    backImgview.layer.shadowOpacity = 0.5f;
    
    // Initialization code
}
-(void)initwithAmenitiesArray:(NSMutableArray *)AmenitiesArr
{
    AmenitiesArray = AmenitiesArr;
    
    NSLog(@"%@",AmenitiesArray);
    
    CGFloat  Screenwidth = [UIScreen mainScreen].bounds.size.width;
    
    for (UIView *view in [InnerView subviews])
    {
        [view removeFromSuperview];
    }

    
    UIScrollView * Backscroll =[[UIScrollView alloc]init];
    [Backscroll setFrame:CGRectMake(0, 0,Screenwidth-65,110)];
    
    [Backscroll setBackgroundColor:[UIColor clearColor]];
    
    [InnerView addSubview:Backscroll];
    
    CGFloat xcoordinate  = 0;
    CGFloat ycoordinate  = 0;
    CGFloat gap  = 18;
    
    CGFloat PropertyWidth   = 100;
    
     for(int i =0 ; i < AmenitiesArray.count ; i++ )
     {
         
                 UIImageView * ProprtyTypeImg = [[UIImageView alloc]initWithFrame:CGRectMake(xcoordinate, ycoordinate, PropertyWidth, 100)];
                 [ProprtyTypeImg setBackgroundColor:[UIColor lightTextColor]];
                 [ProprtyTypeImg setContentMode:UIViewContentModeScaleAspectFit];
                 [ProprtyTypeImg.layer setBorderColor:[UIColor lightGrayColor].CGColor];
                 [ProprtyTypeImg.layer setBorderWidth:0.5];

         
                 [Backscroll addSubview:ProprtyTypeImg];
         
         NSString * url = [AmenitiesArray objectAtIndex:i];
         NSArray * temp = [url componentsSeparatedByString:@"/"];
         
         NSString *image1 =[NSString stringWithFormat:@"%@",[temp objectAtIndex:[temp count]-1]];
         
         if (![image1 isEqualToString:@""])
         {
             
             UIActivityIndicatorView *spinnerCell = [[UIActivityIndicatorView alloc]
                                                     initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
             spinnerCell.color=[UIColor darkGrayColor];
             spinnerCell.frame=CGRectMake(ProprtyTypeImg.frame.size.width/2-10, ProprtyTypeImg.frame.size.height/2-10, 20, 20);
             [ProprtyTypeImg addSubview:spinnerCell];
             [spinnerCell startAnimating];
             
             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
             NSString *documentsDirectory = [paths objectAtIndex:0];
             NSString *getImagePath;
             
             @try
             {
                 getImagePath = [documentsDirectory stringByAppendingPathComponent:image1];
             }
             @catch (NSException *exception)
             {
                 getImagePath=@"";
             }
             @finally {
                 
             }
             
             UIImage *image = [UIImage imageWithContentsOfFile:getImagePath];
             
             if(image)
             {
                 
                 [ProprtyTypeImg setImage:image];
                 [spinnerCell stopAnimating];
                 
             }
             else
             {
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     
                     NSURL *imageURL = [NSURL URLWithString:url];
                     
                     UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:imageURL]];
                     
                     if(image)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             
                             [ProprtyTypeImg setImage:image];
                             [spinnerCell stopAnimating];
                             
                         });
                         
                         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                         NSString *documentsDirectory = [paths objectAtIndex:0];
                         NSString *appFile = [documentsDirectory stringByAppendingPathComponent:image1];
                         NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
                         [imageData writeToFile:appFile atomically:NO];
                     }
                     else
                     {
                         UIImage *imgno = [UIImage imageNamed:@"no_img_sq.png"];
                         
                         [ProprtyTypeImg setImage:imgno];
                         
                         [spinnerCell stopAnimating];
                         
                     }
                 });
             }
         }
         else
         {
             UIImage *imgno = [UIImage imageNamed:@"no_img_sq.png"];
             
             [ProprtyTypeImg setImage:imgno];
         }
         
         
         xcoordinate = xcoordinate + PropertyWidth + gap;
         
     }
    
    [Backscroll setContentSize:CGSizeMake(xcoordinate, 110)];
    
    
    
//    CGFloat Innerwidth = backImgview.frame.size.width;
//    
//    CGFloat xcoordinate  = 0;
//    CGFloat ycoordinate  = 0;
//    
//    CGFloat gap  = 38;
//    
//    CGFloat PropertyWidth   = 28;
//    
//    gap = (Innerwidth - (PropertyWidth * 5))/4;
//    
//    int totalcount = 5;
//    
//    [[InnerView subviews]
//     makeObjectsPerformSelector:@selector(removeFromSuperview)];
//
//    
//    for(int i =0 ; i < AmenitiesArray.count ; i++ )
//    {
//        
//        GTLHotelEndpointAmenity * wrapper = (GTLHotelEndpointAmenity *)[AmenitiesArray objectAtIndex:i];
//        
//        UIImageView * ProprtyTypeImg = [[UIImageView alloc]initWithFrame:CGRectMake(xcoordinate, ycoordinate, PropertyWidth, PropertyWidth)];
//        
//        [ProprtyTypeImg setImage:[UIImage imageNamed:wrapper.name]];
//        [ProprtyTypeImg setBackgroundColor:[UIColor clearColor]];
//        [ProprtyTypeImg setContentMode:UIViewContentModeScaleAspectFit];
//        [InnerView addSubview:ProprtyTypeImg];
//        
//        
//        UILabel * namelbl = [[UILabel alloc]init];
//        [namelbl setFrame:CGRectMake(xcoordinate, ycoordinate + 30, PropertyWidth, 14)];
//        [namelbl setText:wrapper.name];
//        [namelbl setTextAlignment:NSTextAlignmentCenter];
//        [namelbl setFont:[UIFont fontWithName:@"Helvetica" size:9.0]];
//        [namelbl setNumberOfLines:2];
//        [namelbl setTextColor:[UIColor blackColor]];
//        [InnerView addSubview:namelbl];
//
//    
//        xcoordinate = xcoordinate + PropertyWidth + gap;
//    }
    
    
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
