//
//  HomePageViewController.m
//  Jaago
//
//  Created by Mark Anthony Corpuz on 7/21/14.
//  Copyright (c) 2014 MarkAnthonyCorpuz. All rights reserved.
//






#import "HomePageViewController.h"
#import "ImageViewCell4.h"
#import "LandingPageResultViewController.h"
#import "ImageReviewViewController.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "YoutubeViewController.h"
#import "VineVideoModel.h"

#import "MGBox.h"
#import "MGScrollView.h"

#import "JSONModelLib.h"
#import "VideoModel.h"

#import "PhotoBox.h"
#import "WebVideoViewController.h"

// instagram clientID
#define KCLIENTID @"745cbb1a78f9401fa4d8cc3a5713a32c"
#import <MobileCoreServices/UTCoreTypes.h>
#import "ImageProperties.h"
#import "RootNavigationController.h"

#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"

@interface HomePageViewController ()
{
    NSIndexPath *selectedIndex;
}
@property (strong, nonatomic) UIRefreshControl *rc;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation HomePageViewController
{
    //variables used for this controller
    
    NSMutableArray *propertiesArray;
    // tempArray is to allow to add to the full propertiesArray during each cycle
    NSMutableArray *tempArray;
    NSMutableArray *listOfTempArray;
    NSMutableArray *itemOfMainArray;
    NSMutableOrderedSet *locationArray;
    BOOL defaultWord;
    BOOL locationBOOL;
    int numberOfLocations;
    
    BOOL endSearch;
    int ndxCount;
    int arrayCountSub;
    NSString *hashtagWord;
    NSArray* videos;
    NSDictionary *hotelNames;

}

@synthesize images, realTableView, islandHotels, islandPicked, navigationImage, theSearchBar, searchHotelsTableView, bottomToolBar;


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_bar.png"] forBarMetrics:UIBarMetricsDefault];
    //[self setupBottomToolbarIcons];
    //[self.navigationController.navigationBar setBackgroundImage:navigationImage forBarMetrics:UIBarMetricsDefault];

}




- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    NSLog(@"current name: %@",self.title);
    //[self.navigationItem.backBarButtonItem setTitle:@""];
    //self.theSearchBar.hidden=YES;
    searchHotelsTableView.hidden = YES;
    //hotelTitleLabel.hidden=NO;
    RootNavigationController * rootNVC = (RootNavigationController *)self.navigationController;
    hotelNames = rootNVC.hotelNames;
    
    self.title= @"Courtesy Inn";
    listOfTempArray = [[NSMutableArray alloc]initWithObjects:@"Inn1.png",@"Inn2.png",@"Inn3.png",@"Inn4.png", nil];

    if ([_selectedRow integerValue] == 0) {
        self.title = @"Dolphin Cove Motel";
        listOfTempArray = [[NSMutableArray alloc]initWithObjects:@"Dolphin1.png",@"Dolphin2.png",@"Dolphin3.png",@"Dolphin4.png", nil];
    }
    
    
//    self.title=[hotelNames objectForKey:islandPicked];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_bar.png"] forBarMetrics:UIBarMetricsDefault];
    //hotelTitleLabel.text = [NSString stringWithFormat:@"This is where the description for %@ goes",islandPicked];
    
    /*
    self.rc = [[UIRefreshControl alloc]init];
    [self.rc addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    [self.realTableView addSubview:self.rc];
    
    defaultWord = YES;
    locationArray = nil;
    endSearch = NO;
    propertiesArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableOrderedSet alloc]init];
    if(islandPicked) {
        self.title=[hotelNames objectForKey:islandPicked];
    }
    listOfTempArray = [[NSMutableArray alloc] init]; // array no - 1
    itemOfMainArray = [[NSMutableArray alloc] init];
    [itemOfMainArray addObjectsFromArray:islandHotels];
    
    ndxCount = 0;
    arrayCountSub = 0;
    
    // adds an IMageproperty to an array to eventually display on the table.
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];

        [photoObjectQuery whereKey:@"tempLocation" equalTo:islandPicked];
        [photoObjectQuery whereKey:@"active" equalTo:@1];
        [photoObjectQuery orderByDescending:@"createdAt"];
        [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (PFObject *object in objects) {
                    ImageProperties *property = [[ImageProperties alloc] init];
                    property.id = [object objectId];
                    property.instagramPic = NO;
                    //NSLog(@"found a match!");
                
                    [propertiesArray addObject:property];
                    
                }
                [self getInstagramFeed];
                [self getTwitterFeed];
                //[self getYoutubeFeed];
                [self getVineFeed];
                
            }
            else {
                [self getInstagramFeed];
                [self getTwitterFeed];
                //[self getYoutubeFeed];
                [self getVineFeed];

            }
        }];
     
     */
  
}

- (void) refreshTableView{
    defaultWord = YES;
    locationArray = nil;
    endSearch = NO;
    propertiesArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableOrderedSet alloc]init];
    if(islandPicked) {
        self.title=[hotelNames objectForKey:islandPicked];
    }
    listOfTempArray = [[NSMutableArray alloc] init]; // array no - 1
    itemOfMainArray = [[NSMutableArray alloc] init];
    [itemOfMainArray addObjectsFromArray:islandHotels];
    
    ndxCount = 0;
    arrayCountSub = 0;
    
    // adds an IMageproperty to an array to eventually display on the table.
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    
    [photoObjectQuery whereKey:@"tempLocation" equalTo:islandPicked];
    [photoObjectQuery whereKey:@"active" equalTo:@1];
    [photoObjectQuery orderByDescending:@"createdAt"];
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                ImageProperties *property = [[ImageProperties alloc] init];
                property.id = [object objectId];
                property.instagramPic = NO;
                //NSLog(@"found a match!");
                
                [propertiesArray addObject:property];
                
            }
            [self getInstagramFeed];
            [self getTwitterFeed];
            //[self getYoutubeFeed];
            [self getVineFeed];
            
        }
        else {
            [self getInstagramFeed];
            [self getTwitterFeed];
            //[self getYoutubeFeed];
            [self getVineFeed];
            
        }
        [self.rc endRefreshing];

    }];
}

- (void) getVineFeed{
    hashtagWord = [islandHotels objectAtIndex:ndxCount];
    if (ndxCount == ([islandHotels count]-1)) {
        endSearch = YES;
    }
    //URL escape the term
    NSString* term = [hashtagWord stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //term=@"Hawaii";
    //make HTTP call
    NSString* searchCall = [NSString stringWithFormat:@"https://api.vineapp.com/timelines/tags/%@", term];
    
    [JSONHTTPClient getJSONFromURLWithString: searchCall
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      
                                      //got JSON back
                                      //NSLog(@"Got JSON from web: %@", json);
                                      
                                      if (err) {
                                          NSLog(@"Network Error!");
                                          return;
                                      }
                                      
                                      //initialize the models
                                      videos = json[@"data"][@"records"];
                                      
                                      if (videos) NSLog(@"Loaded successfully vine models");
                                      
                                      //add the thumbnails
                                      ImageProperties *properties;
                                      tempArray = [[NSMutableArray alloc]init];
                                      for (int i=0;i<videos.count;i++) {
                                          
                                          //get the data
                                          NSDictionary* video = videos[i];
                                          VineVideoModel *vinev=[[VineVideoModel alloc] init];
                                          vinev.title=video[@"description"];
                                          vinev.videoURL=[NSURL URLWithString:video[@"videoUrl"]];
                                          vinev.videoThumb=[NSURL URLWithString:video[@"thumbnailUrl"]];
                                          
                                          
                                          //create a property
                                          properties.videoType=@"Vine";
                                          properties = [[ImageProperties alloc] init];
                                          properties.url = vinev.videoThumb;
                                          properties.instagramPic = YES;
                                          properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
                                          properties.user = @"VineUSER";
                                          properties.vineVideo = vinev;
                                          properties.isVideo=YES;
                                          [tempArray addObject:properties];
                                          
                                      }
                                      //NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                                      [propertiesArray addObjectsFromArray:tempArray];
                                      [realTableView reloadData];
                                  }];
    
}


- (void) getYoutubeFeed{
    hashtagWord = [islandHotels objectAtIndex:ndxCount];
    if (ndxCount == ([islandHotels count]-1)) {
        endSearch = YES;
    }
    [self searchYoutubeVideosForTerm:hashtagWord];
}

-(void)searchYoutubeVideosForTerm:(NSString*)term
{
    //NSLog(@"Searching for '%@' ...", term);
    
    //URL escape the term
    term = [term stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //make HTTP call
    NSString* searchCall = [NSString stringWithFormat:@"http://gdata.youtube.com/feeds/api/videos?q=%@&max-results=50&alt=json", term];
    
    [JSONHTTPClient getJSONFromURLWithString: searchCall
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      
                                      //got JSON back
                                      //NSLog(@"Got JSON from web: %@", json);
                                      
                                      if (err) {
                                          NSLog(@"Network Error!");
                                          return;
                                      }
                                      
                                      //initialize the models
                                      videos = [VideoModel arrayOfModelsFromDictionaries:
                                                json[@"feed"][@"entry"]
                                                ];
                                      
                                      //if (videos) NSLog(@"Loaded successfully models");
                                      
                                      //add the thumbnails
                                      ImageProperties *properties;
                                      tempArray = [[NSMutableArray alloc]init];
                                      for (int i=0;i<videos.count;i++) {
                                          
                                          //get the data
                                          VideoModel* video = videos[i];
                                          MediaThumbnail* thumb = video.thumbnail[0];
                                          
                                          //create a property
                                          properties.videoType=@"Youtube";
                                          properties = [[ImageProperties alloc] init];
                                          properties.url = thumb.url;
                                          properties.instagramPic = YES;
                                          properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
                                          properties.user = @"YoutubeUSER";
                                          properties.videos = video;
                                          properties.isVideo=YES;
                                          [tempArray addObject:properties];

                                      }
                                      //NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                                      [propertiesArray addObjectsFromArray:tempArray];
                                      [realTableView reloadData];
                                  }];
}




- (void) getTwitterFeed{
    hashtagWord = [islandHotels objectAtIndex:ndxCount];
    if (ndxCount == ([islandHotels count]-1)) {
        endSearch = YES;
    }
    //handle network requests here
    [[Twitter sharedInstance] logInGuestWithCompletion:^
     (TWTRGuestSession *session, NSError *error) {
         if (session) {
             // make API calls that do not require user auth
             //NSLog(@"TWitter logged in");
             NSString *statusesShowEndpoint = [NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json?q=%@",hashtagWord];
             //NSLog(@"Twitter hashtagWord=%@",hashtagWord);
             NSDictionary *params = @{};
             NSError *clientError;
             NSURLRequest *request = [[[Twitter sharedInstance] APIClient]
                                      URLRequestWithMethod:@"GET"
                                      URL:statusesShowEndpoint
                                      parameters:params
                                      error:&clientError];
             
             if (request) {
                 [[[Twitter sharedInstance] APIClient]
                  sendTwitterRequest:request
                  completion:^(NSURLResponse *response,
                               NSData *data,
                               NSError *connectionError) {
                      if (data) {
                          // handle the response data e.g.
                          NSError *jsonError;
                          NSDictionary *json = [NSJSONSerialization
                                                JSONObjectWithData:data
                                                options:0
                                                error:&jsonError];
                          //NSLog(@"twitter json :::: %@", json);
                          ImageProperties *properties;
                          tempArray = [[NSMutableArray alloc]init];
                          
                          NSArray *statuses=[json objectForKey:@"statuses"];
                          for (NSDictionary *status in statuses) {
                              NSDictionary * entity=[status valueForKey:@"entities"];
                              NSDictionary * medias=[entity valueForKey:@"media"];
                              for (NSDictionary * media in medias){
                                  NSString *type=[media valueForKey:@"type"];
                                  if([type isEqualToString:@"photo"]){
                                      properties = [[ImageProperties alloc] init];
                                      properties.url = [NSURL URLWithString:[media valueForKey:@"media_url"]];
                                      properties.instagramPic = YES;
                                      properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
                                      properties.user = @"TwitterUSER";
                                      properties.isVideo=NO;
                                      [tempArray addObject:properties];
                                  }
                              }
                          }
                          //NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                          [propertiesArray addObjectsFromArray:tempArray];
                          [realTableView reloadData];
                      }
                      else {
                          NSLog(@"Error: %@", connectionError);
                      }
                  }];
             }
             else {
                 NSLog(@"Error: %@", clientError);
             }
         } else {
             NSLog(@"error: %@", [error localizedDescription]);
         }
     }];
    
    
}

//gets instagram feed based on what was searched.
- (void) getInstagramFeed {
    
    
    //looks for locations based off of the inital hashtag search of instagram
    if ([locationArray count] == 0) {
        locationBOOL = NO;
    }
    
    
    
    if (locationBOOL == NO) {
        hashtagWord = [islandHotels objectAtIndex:ndxCount];
        if (ndxCount == ([islandHotels count]-1)) {
            endSearch = YES;
        }

        NSString *requestUrl = [NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?client_id=%@&count=20",hashtagWord, KCLIENTID];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
        (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else {
        NSInteger ndx;
        numberOfLocations = [locationArray count];
        for (ndx = 0; [locationArray count] > ndx;) {
            NSString *requestUrl = [NSString stringWithFormat: @"https://api.instagram.com/v1/locations/%@/media/recent?client_id=%@",[locationArray objectAtIndex:ndx], KCLIENTID];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
            (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
            [locationArray removeObjectAtIndex:ndx];

        }
    }
}

- (IBAction)unwindToVC1:(UIStoryboardSegue*)sender
{
    
    NSLog(@"UNWINDTOVC1");
    propertiesArray = nil;
    [self viewDidLoad];
}

- (void)somethingToAvoidNestedPop {
    [self performSegueWithIdentifier:@"ToProfile" sender:self];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark NSURLConnection Delegate Methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
}

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

-(void) connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // loads all the images if there is a location in those images search for location
    if (locationBOOL == NO) {
        NSError *error;
        ImageProperties *properties;
        tempArray = [[NSMutableArray alloc]init];
    
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //NSLog(@"instagram json :::: %@", json);
        NSArray *firstPic = [[[json objectForKey:@"data"]valueForKey:@"images"]valueForKey:@"standard_resolution"];
    
        for (NSDictionary *testDict in firstPic) {
        
            properties = [[ImageProperties alloc] init];
        
            properties.url = [NSURL URLWithString:[testDict objectForKey:@"url"]];
            properties.instagramPic = YES;
            properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
            properties.user = @"InstagramUSER";
            properties.isVideo=NO;
            [tempArray addObject:properties];
    
        }
        
        NSArray *firstLocation = [[json objectForKey:@"data"]valueForKey:@"location"];
        for (NSDictionary *locDict in firstLocation) {
            if (![locDict isEqual:[NSNull null]]) {

                if ([locDict objectForKey:@"name"]) {
                    locationBOOL = YES;
                    [locationArray addObject:[locDict objectForKey:@"id" ]];
                }
            }
        
        }
        
        [propertiesArray addObjectsFromArray:tempArray];

        if (locationBOOL == YES) {
            [self getInstagramFeed];
        }
        
    }
    else {
        
        
        ImageProperties *properties;

        tempArray = [[NSMutableArray alloc]init];
        NSError *error;
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        NSArray *firstPic = [[[json objectForKey:@"data"]valueForKey:@"images"]valueForKey:@"standard_resolution"];
        
        for (NSDictionary *testDict in firstPic) {
            
            properties = [[ImageProperties alloc] init];
            
            properties.url = [NSURL URLWithString:[testDict objectForKey:@"url"]]; //1
            properties.instagramPic = YES;
            properties.caption = [NSString stringWithFormat: @"#%@",hashtagWord];
            properties.user = @"InstagramUSER";
            properties.isVideo=NO;
            [tempArray addObject:properties];
            

            
        }
        [propertiesArray addObjectsFromArray:tempArray];
        


        
    }
    numberOfLocations--;

    if ([propertiesArray count]%4 != 0) {
        if (++numberOfLocations != 0 ){
            numberOfLocations--;
        }
        else {
            int number = [propertiesArray count]%4;
            int ndx;
            for (ndx = 0; ndx < number; ndx ++) {
                ImageProperties *properties;

                properties = [[ImageProperties alloc] init];
                properties.user = @"blank";
                properties.image = [UIImage imageNamed:@"white.png"];
                [propertiesArray addObject:properties];
            
            }
        }
    }
    responseData = nil;

    
    [realTableView reloadData];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    responseData = nil;
    // inform the user
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 4;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    UIImageView *imgView = (UIImageView*)[cell viewWithTag:1001];
    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Dolphin%ld.png",indexPath.row+1]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = indexPath;
    [self performSegueWithIdentifier:@"ShowLandingPage" sender:nil];
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == realTableView) {
        
     
    }
    else {
        theSearchBar.text = [listOfTempArray objectAtIndex:indexPath.row];
        [self searchBarSearchButtonClicked:theSearchBar];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}


// sets it to have 4 images per section of the table instead of just 1
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == realTableView) {
        if (defaultWord == NO) {
            return [propertiesArray count]/4 +1;
        }
        else if (ndxCount == ([islandHotels count] - 1)) {
            return [propertiesArray count]/4 + 1;
        }
        else {
            return [propertiesArray count]/4;
        }
    }
    else {
        return [listOfTempArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:realTableView]) {
        static NSString *CellIdentifier = @"ImageViewCell";
        ImageViewCell4 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[ImageViewCell4 alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:   CellIdentifier];
        }
        if (indexPath.row == [propertiesArray count]/4 - 1) {
            ndxCount ++;
            if (ndxCount < [islandHotels count] && defaultWord == YES) {
                [self getInstagramFeed];
                [self getTwitterFeed];
                [self getYoutubeFeed];
                [self getVineFeed];
            }
            else {
                NSLog(@"hits this last one ");
                endSearch = YES;
            }
        }

        
        if (indexPath.row == [propertiesArray count]/4) {
            cell = [[ImageViewCell4 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hello"];
            cell.imageView.hidden = YES;
            if ([propertiesArray count] >0) {
                if (endSearch == YES) {
                    cell.textLabel.text = @"                      End of Search";
                    [cell setUserInteractionEnabled:NO];
                }

            }
        }
        else {
            cell.textLabel.hidden = TRUE;
            
            // math to have 4 images per row.
            ImageProperties *cellProperties1;
            ImageProperties *cellProperties2;
            ImageProperties *cellProperties3;
            ImageProperties *cellProperties4;
            cellProperties1 = ((ImageProperties * )propertiesArray[indexPath.row*4]);
            cellProperties2 = ((ImageProperties * )propertiesArray[indexPath.row*4+1]);
            cellProperties3 = ((ImageProperties * )propertiesArray[indexPath.row*4+2]);
            cellProperties4 = ((ImageProperties * )propertiesArray[indexPath.row*4+3]);


            
            if (cellProperties1.image) {
                cell.pictureView1.image = cellProperties1.image;
                if ([cellProperties1.user isEqualToString:@"blank"]) {
                    cell.button1.enabled = NO;
                    
                }
                
            }
            
            // this checks if each image id downloaded or not
            else {
                if (![cellProperties1.user isEqualToString:@"blank"]) {
                    
                
                cell.pictureView1.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                if (cellProperties1.instagramPic == YES) {
                    [self downloadImageWithURL:cellProperties1.url completionBlock:^(BOOL succeeded, UIImage *image) {
                        if (succeeded) {
                            if(cellProperties1.isVideo || cellProperties1.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image];
                                cell.pictureView1.image=newimage;
                                cellProperties1.image=newimage;
                            }else{
                                cell.pictureView1.image = image;
                                cellProperties1.image = image;
                            }
                        }
                        
                    }];
                    
                }
                else {
                    [self downloadImageWithObject:(&cellProperties1) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                        if (succeeded) {
                            if(image.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image.image];
                                cell.pictureView1.image=newimage;
                                cellProperties1.image=newimage;
                            }else{
                                cell.pictureView1.image = image.image;
                                cellProperties1.image = image.image;
                            }

                            cellProperties1.user = image.user;
                            cellProperties1.caption = image.caption;
                            cellProperties1.videoData=image.videoData;
                            cellProperties1.isUserVideo=image.isUserVideo;
                            //NSLog(@"Regurgitate %@, %@", cellProperties1.user, cellProperties1.caption);
                        }
                    }];
                }
                }

            }
            if (cellProperties2.image) {
                cell.pictureView2.image = cellProperties2.image;
                

                if ([cellProperties2.user isEqualToString:@"blank"]) {
                    cell.button2.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties2.user isEqualToString:@"blank"]) {

                cell.pictureView2.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                if (cellProperties2.instagramPic == YES) {
                    [self downloadImageWithURL:cellProperties2.url completionBlock:^(BOOL succeeded, UIImage *image) {
                        if (succeeded) {
                            if(cellProperties2.isVideo || cellProperties2.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image];
                                cell.pictureView2.image=newimage;
                                cellProperties2.image=newimage;
                            }else{
                                cell.pictureView2.image = image;
                                cellProperties2.image = image;
                            }
                        }
                        
                    }];
                    
                }
                else {
                    [self downloadImageWithObject:(&cellProperties2) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                        if (succeeded) {
                            //NSLog(@"lets do it");
                            if(image.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image.image];
                                cell.pictureView2.image=newimage;
                                cellProperties2.image=newimage;
                            }else{
                                cell.pictureView2.image = image.image;
                                cellProperties2.image = image.image;
                            }
                            cellProperties2.user = image.user;
                            cellProperties2.caption = image.caption;
                            cellProperties2.videoData=image.videoData;
                            cellProperties2.isUserVideo=image.isUserVideo;
                            //NSLog(@"Regurgitate %@, %@", cellProperties2.user, cellProperties2.caption);
                        }
                    }];
                }
                }
            }
            if (cellProperties3.image) {
                cell.pictureView3.image = cellProperties3.image;
                if ([cellProperties3.user isEqualToString:@"blank"]) {
                    cell.button3.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties3.user isEqualToString:@"blank"]) {

                cell.pictureView3.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                if (cellProperties3.instagramPic == YES) {
                    [self downloadImageWithURL:cellProperties3.url completionBlock:^(BOOL succeeded, UIImage *image) {
                        if (succeeded) {
                            if(cellProperties3.isVideo || cellProperties3.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image];
                                cell.pictureView3.image=newimage;
                                cellProperties3.image=newimage;
                            }else{
                                cell.pictureView3.image = image;
                                cellProperties3.image = image;
                            }
                        }
                        

                    }];
                    

                }
                else {
                    [self downloadImageWithObject:(&cellProperties3) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                        if (succeeded) {
                            if(image.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image.image];
                                cell.pictureView3.image=newimage;
                                cellProperties3.image=newimage;
                            }else{
                                cell.pictureView3.image = image.image;
                                cellProperties3.image = image.image;
                            }

                            
                            cellProperties3.user = image.user;
                            cellProperties3.caption = image.caption;
                            cellProperties3.videoData=image.videoData;
                            cellProperties3.isUserVideo=image.isUserVideo;
                        }
                    }];
                }
                }
            }
            if (cellProperties4.image) {
                cell.pictureView4.image = cellProperties4.image;
                if ([cellProperties4.user isEqualToString:@"blank"]) {
                    cell.button4.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties4.user isEqualToString:@"blank"]) {

                cell.pictureView4.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                if (cellProperties4.instagramPic == YES) {
                    [self downloadImageWithURL:cellProperties4.url completionBlock:^(BOOL succeeded, UIImage *image) {
                        if (succeeded) {
                            
                            if(cellProperties4.isVideo || cellProperties4.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image];
                                cell.pictureView4.image=newimage;
                                cellProperties4.image=newimage;
                            }else{
                                cell.pictureView4.image = image;
                                cellProperties4.image = image;
                            }
                        }
                        
                    }];
                }
                else {
                    [self downloadImageWithObject:(&cellProperties4) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                        if (succeeded) {
                            if(image.isUserVideo){
                                UIImage* newimage=[self overlayPlayerIcon:image.image];
                                cell.pictureView4.image=newimage;
                                cellProperties4.image=newimage;
                            }else{
                                cell.pictureView4.image = image.image;
                                cellProperties4.image = image.image;
                            }


                            cellProperties4.user = image.user;
                            cellProperties4.caption = image.caption;
                            cellProperties4.videoData = image.videoData;
                            cellProperties4.isUserVideo=image.isUserVideo;
                            
                            //NSLog(@"Regurgitate %@, %@", cellProperties4.user, cellProperties4.caption);
                        }
                    }];
                }
            }
                
            }
        }
        return cell;
        
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text = [listOfTempArray objectAtIndex:indexPath.row];
        
        return cell;
    }
}

//code for displaying the player icon on the image
- (UIImage *) overlayPlayerIcon: (UIImage *) image{
    //NSLog(@"doing the overlay...");
    UIImage *backgroundImage = image;
    UIImage *playermarkImage = [UIImage imageNamed:@"PlayButtonImage2.png"];
    
    UIGraphicsBeginImageContext(backgroundImage.size);
    [backgroundImage drawInRect:CGRectMake(0, 0,500,400)];
    [playermarkImage drawInRect:CGRectMake(0,40 , 500,300)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}





//code for the searchbar on the page
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    searchHotelsTableView.hidden = NO;
    NSString *name = @"";
    NSString *firstLetter = @"";
    if (listOfTempArray.count > 0)
        [listOfTempArray removeAllObjects];
    
    if ([searchText length] > 0)
    {
        for (int i = 0; i < [itemOfMainArray count] ; i = i+1)
        {
            name = [itemOfMainArray objectAtIndex:i];
            if (name.length >= searchText.length)
            {
                firstLetter = [name substringWithRange:NSMakeRange(0, [searchText length])];
                
                if( [firstLetter caseInsensitiveCompare:searchText] == NSOrderedSame)
                {
                    [listOfTempArray addObject: [itemOfMainArray objectAtIndex:i]];
                }
            }
        }
    }
    else
    {
        
        [listOfTempArray addObjectsFromArray:itemOfMainArray ];
    }
    
    [searchHotelsTableView reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    hashtagWord = searchBar.text;
    if( [searchBar.text caseInsensitiveCompare:islandPicked] == NSOrderedSame ) {
        defaultWord = YES;
    }
    else {
        defaultWord = NO;
    }
    
    ndxCount = 0;
    arrayCountSub = 0;
    searchHotelsTableView.hidden = YES;
    BOOL match = NO;
    int ndx = 0;
    images = [[NSMutableArray alloc] init];
    propertiesArray = [[NSMutableArray alloc]init];
    //NSLog(@"one");
    [realTableView reloadData];
    
    
    // when search is pressed it querys the parse database then searches instagram.
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    [photoObjectQuery whereKey:@"tags" equalTo:[searchBar.text lowercaseStringWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-US"]]];
    [photoObjectQuery orderByDescending:@"createdAt"];
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            for(PFObject *hello in objects){
                ImageProperties *property = [[ImageProperties alloc] init];
                property.id = [hello objectId];
                property.instagramPic = NO;
                
                
                [propertiesArray addObject:property];
                //NSLog(@"two");

                [realTableView reloadData];
            }
        }
        else {
        }
    }];
    
    if (defaultWord == NO) {
        for(ndx = 0; ndx < [islandHotels count] && match == NO; ndx++){
            if ([[islandHotels objectAtIndex:ndx] isEqualToString:[searchBar.text capitalizedString]]){
                match = YES;
            }
        }
        if (match){
            NSString *requestUrl = [NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?client_id=%@&count=20",searchBar.text, KCLIENTID];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:requestUrl]];
            (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
        
    }
    else {
        [self getInstagramFeed];
        [self getTwitterFeed];
        [self getYoutubeFeed];
        [self getVineFeed];
    }
    [searchBar resignFirstResponder];
    
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    theSearchBar.text = @"";
    
    searchHotelsTableView.hidden = YES;
    
    [searchBar resignFirstResponder];
}

- (IBAction)logoutButtonPressed:(id)sender
{
    [PFUser logOut];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    //[self viewDidAppear:NO];
}

- (IBAction)cameraButtonPressed:(id)sender
{
    
}

- (IBAction)profileButtonPressed:(id)sender {
    
}


- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (void) downloadImageWithObject:(ImageProperties **)id completionBlock:(void (^)(BOOL succeeded, ImageProperties *image)) completionBlock
{
    //NSLog(@"%@",(*id).id);
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    [photoObjectQuery includeKey:@"creator"];
    [photoObjectQuery getObjectInBackgroundWithId:(*id).id block:^(PFObject *object, NSError *error) {
        if (!error) {
            ImageProperties *imageSpecs = [[ImageProperties alloc] init];
            imageSpecs.user = object[@"creator"][@"username"];
            imageSpecs.caption = object[@"caption"];
            imageSpecs.imageFile = object[@"image"];
            if([object.allKeys containsObject:@"video"]){
                imageSpecs.videoFile = object[@"video"];
                
            }
            
            [object[@"video"] getDataInBackgroundWithBlock:^(NSData *videoData,NSError *error) {
                if(!error){
                    NSLog(@"seccessfully get video: data length=%lu",(unsigned long)videoData.length);
                    imageSpecs.videoData=videoData;
                    if(videoData.length>0){
                        imageSpecs.isUserVideo=YES;
                        imageSpecs.caption=[imageSpecs.caption stringByAppendingString:@""];
                    }
                }else{
                    NSLog(@"ERROR downloading video");
                }
            }];
            [object[@"image"] getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    NSLog(@"successfully get image: data length=%lu",(unsigned long)imageData.length);
                    imageSpecs.image = [UIImage imageWithData:imageData];
                    
                    completionBlock (YES,imageSpecs);
                }
                else {
                    NSLog(@"Error downloading image");
                    completionBlock (NO,nil);
                }
            }];
        }
        else {
            NSLog(@"Error first query");
            completionBlock (NO,nil);
        }
    }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    
    if([title isEqualToString:@"GO"]) {
    }
    
    if([title isEqualToString:@"Okay"])
    {
        NSLog(@"Posted Photo was selected.");
        
    }
}

# pragma mark -
# pragma mark bottom Tool Bar style settings
- (void) setupBottomToolbarIcons{
    NSArray * bottomToolbarItems = self.bottomToolBar.items;
    UIBarButtonItem * homepageItem = [bottomToolbarItems objectAtIndex:0];
    [homepageItem setImage:[UIImage imageNamed:@"home_normal.png"]];
    UIBarButtonItem * cameraItem = [bottomToolbarItems objectAtIndex:1];
    [cameraItem setImage:[UIImage imageNamed:@"camera_normal.png"]];
    UIBarButtonItem * userItem = [bottomToolbarItems objectAtIndex:2];
    [userItem setImage:[UIImage imageNamed:@"user_normal.png"]];
    UIBarButtonItem * settingsItem = [bottomToolbarItems objectAtIndex:3];
    [settingsItem setImage:[UIImage imageNamed:@"settings_normal.png"]];
    
}

- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}


# pragma mark Segues
// segues based on what the user pressed
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    LandingPageResultViewController *controller = [segue destinationViewController];
    controller.selectedImage = [NSString stringWithFormat:@"Inn%ld.png",selectedIndex.row+1];
    if ([_selectedRow integerValue] == 0) {
        controller.selectedImage = [NSString stringWithFormat:@"Dolphin%ld.png",selectedIndex.row+1];
    }
    if ([segue.identifier isEqualToString:@"LandingPageResultSegue"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        NSInteger path = realTableView.indexPathForSelectedRow.row;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        //NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button1"]) {
        NSLog(@"got here !!!!!!!!!!");
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button2"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 1;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button3"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 2;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;

        NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button4"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 3;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        controller.controllerTitle = self.title;
        NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    
}

@end
