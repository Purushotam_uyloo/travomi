//
//  ReportProblemViewController.m
//  Travomi
//
//  Created by WangYinghao on 10/15/15.
//  Copyright © 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "ReportProblemViewController.h"


@interface ReportProblemViewController ()
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) UIToolbar *toolBar;
@end

@implementation ReportProblemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)]; //toolbar is uitoolbar object
    self.toolBar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(submitClicked)];
    [self.toolBar setItems:[NSArray arrayWithObject:btnDone]];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [textView setInputAccessoryView:self.toolBar];
    return YES;
}


- (IBAction)submitClicked {
    [self.textView endEditing:YES];
    // Email Subject
    NSString *emailTitle = @"Bug Report";
    // Email Content
    NSString *messageBody = self.textView.text;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"admin@travomi.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    if ([MFMailComposeViewController canSendMail] ){
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
