//
//  ProfileCell.h
//  Travomi
//
//  Created by Mark Anthony Corpuz on 8/13/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *image;
@property (weak, nonatomic) IBOutlet UIView *mainText;
@property (weak, nonatomic) IBOutlet UILabel *number;

@end
