//
//  HotelPickerViewController.m
//  Jaago
//
//  Created by WangYinghao on 3/2/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//

#import "HotelPickerViewController.h"
#import "HomePageViewController.h"
#import "DestinationPickerCellTableViewCell.h"
#import "RootNavigationController.h"

#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"

@interface HotelPickerViewController ()

@end

@implementation HotelPickerViewController

NSString * hotelName;
bool selectall;
NSMutableArray *newHotelList;
NSDictionary *hotelNames;
NSMutableDictionary *hotelImages;

@synthesize navigationImage, hotelList, islandName, hotelTable,settingsView, type, settingsButton;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_bar.png"] forBarMetrics:UIBarMetricsDefault];
    
    settingsView.hidden = true;
    //[self.navigationController.navigationBar setBackgroundImage:navigationImage forBarMetrics:UIBarMetricsDefault];
    
}

- (void)viewDidLoad {
    
    
    
    [super viewDidLoad];
    settingsView.hidden = true;
    
    if ([type  isEqual: @"hotel"]) {

        NSLog(@"hello");
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor orangeColor]];
        [self.navigationItem.rightBarButtonItem setEnabled:NO];
        self.navigationItem.rightBarButtonItem = nil;
        NSLog(@"in hotel of hotelpicker view controller");
        newHotelList = [NSMutableArray arrayWithArray:hotelList];
        self.title=[NSString stringWithFormat:@"HOTELS"];
        RootNavigationController * rootNVC = (RootNavigationController *)self.navigationController;
        hotelNames = rootNVC.hotelNames;
        
        // added code
        hotelImages = [[NSMutableDictionary alloc]initWithObjectsAndKeys: nil];
        PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"StaticImages"];
        
        [photoObjectQuery whereKey:@"type" equalTo:islandName];
        [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                for (PFObject *object in objects) {
                    
                    PFFile *imageFile = object[@"image"];
                    NSString *key = object[@"key"];
                    NSLog(@"%@", key);
                    [imageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                        if (!error) {
                            NSLog(@"%@", key);
                            [hotelImages setObject:[UIImage imageWithData:imageData] forKey:key];
                            NSLog(@"%@", key);
                            [hotelTable reloadData];
                        }
                        else {
                            NSLog(@"the missing key is %@", key);
                        }
                    }];
                }
                
            }
            else {
                NSLog(@"in here for nothign");
            }
        }];
        
    }
    else if ([type isEqual:@"hotspot"]) {
        NSLog(@"in hotspot of hotelpickerview controller");
        
    }
    [hotelTable reloadData];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    //[newHotelList replaceObjectAtIndex:hotel.tag withObject:hotel.text];
}

#pragma mark - Table view data source

- (NSInteger) numberOfSectionsInTableView:(UITableView*) tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [newHotelList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DestinationPickerCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelCell" forIndexPath:indexPath];
    // Configure the cell...
    
    //NSLog(@"Configure the cell...");
    if ([type  isEqual: @"hotel"]) {
        cell.hotel.tag = indexPath.row;
        cell.hotel.delegate = self;
        
        [cell.hotel setText:[hotelNames objectForKey: [newHotelList objectAtIndex:indexPath.row]]];
        cell.island.text = islandName;
        //figure out how to make these pictures automatic
        cell.hotelImage.image =[hotelImages objectForKey:[newHotelList objectAtIndex:indexPath.row ]];
    }
    else if ([type isEqual:@"hotspot"]) {

        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSInteger selectedRow = indexPath.row;
    
    hotelName = [newHotelList objectAtIndex:selectedRow];
    NSLog(@"touch on %@", hotelName);
        
    
    [self performSegueWithIdentifier:@"hotelPics" sender:self];
}
- (IBAction)settingsPressed:(UIButton *)sender {
    settingsView.hidden = !settingsView.hidden;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"in prepareforSegue");
    NSIndexPath *indexPath = self.hotelTable.indexPathForSelectedRow;
    NSLog(@"row: %ld", (long)indexPath.row);
    NSLog(@"hotelName, %@", hotelName);
    HomePageViewController *controller = [segue destinationViewController];
    controller.islandPicked = hotelName;
    NSLog(@"controller.islandPicked - %@", controller.islandPicked);
    controller.islandHotels = [NSArray arrayWithObject:hotelName];
    //controller.navigationImage = navigationImage;
    //controller.title = hotelName;

}

- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}

@end
