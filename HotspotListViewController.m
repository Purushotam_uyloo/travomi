//
//  HotelDetailsViewController.m
//  Jaago
//
//  Created by WangYinghao on 3/9/15.
//  Copyright (c) 2015 MarkAnthonyCorpuz. All rights reserved.
//


#import "HotspotListViewController.h"
#import "ImageViewCell4.h"
#import "LandingPageResultViewController.h"
#import "ImageReviewViewController.h"
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "YoutubeViewController.h"
#import "HotspotResultsViewController.h"
#import "RootNavigationController.h"

#import "MGBox.h"
#import "MGScrollView.h"

#import "JSONModelLib.h"
#import "VideoModel.h"

#import "PhotoBox.h"
#import "WebVideoViewController.h"

// instagram clientID
#define KCLIENTID @"745cbb1a78f9401fa4d8cc3a5713a32c"
#define GOOGLEAPIKEY @"AIzaSyC5LTp0l2m74UDj7SoKP81t2XGGjAq2X4M"
#import <MobileCoreServices/UTCoreTypes.h>
#import "ImageProperties.h"

#import "LandingViewController.h"
#import "IslandController.h"
#import "ChooseWhereViewController.h"
#import "SettingsViewController.h"
#import "ProfileController.h"

@interface HotspotListViewController ()

@property IBOutlet UISegmentedControl *hotspotTypeSegmentedControl;
@property UIRefreshControl *rc;

@end

@implementation HotspotListViewController
{
    //variables used for this controller
    
    NSMutableArray *propertiesArray;
    // tempArray is to allow to add to the full propertiesArray during each cycle
    NSMutableArray *tempArray;
    NSMutableArray *listOfTempArray;
    NSMutableArray *itemOfMainArray;
    NSMutableOrderedSet *locationArray;
    BOOL defaultWord;
    BOOL locationBOOL;
    int numberOfLocations;
    
    BOOL endSearch;
    int ndxCount;
    int arrayCountSub;
    NSString *hashtagWord;
    NSArray* videos;
    
}



@synthesize images, realTableView, islandHotels, islandPicked, navigationImage, hotelTitleLabel;

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_bar.png"] forBarMetrics:UIBarMetricsDefault];
    self.title = [NSString stringWithFormat:@"Hotspots near %@", islandPicked];

    //[self.navigationController.navigationBar setBackgroundImage:navigationImage forBarMetrics:UIBarMetricsDefault];
    
}

- (void) toggleSegmentedControl{
    [self.hotspotTypeSegmentedControl setHidden:!self.hotspotTypeSegmentedControl.hidden];
    
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self.hotspotTypeSegmentedControl setHidden:YES];
    NSLog(@"current name: %@",self.title);
    
    //self.theSearchBar.hidden=YES;
    //searchHotelsTableView.hidden = YES;
    hotelTitleLabel.hidden=NO;
    hotelTitleLabel.text=[NSString stringWithFormat:@"Hotspots near %@", islandPicked];
    
    self.title = [NSString stringWithFormat:@"Hotspots near %@", islandPicked];
    
    //self.navigationController.title = [NSString stringWithFormat:@"Hotspots near %@", islandPicked];
    
    UIButton *typeToggle  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [typeToggle addTarget:self action:@selector(toggleSegmentedControl) forControlEvents:UIControlEventTouchUpInside];
    [typeToggle.titleLabel setText:@"T"];
    [typeToggle setBackgroundImage:[UIImage imageNamed:@"filter_normal.png"] forState:UIControlStateNormal];
    [typeToggle setBackgroundImage:[UIImage imageNamed:@"filter_pressed.png"] forState:UIControlStateHighlighted];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:typeToggle];
    self.rc = [[UIRefreshControl alloc]init];
    [self.rc addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
    [self.realTableView addSubview:self.rc];
    
    
    
    defaultWord = YES;
    locationArray = nil;
    endSearch = NO;
    propertiesArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableOrderedSet alloc]init];
    if(islandPicked) {
        RootNavigationController * rootNVC = (RootNavigationController *)self.navigationController;
        NSDictionary *hotelNames = rootNVC.hotelNames;
        
        self.title = [hotelNames objectForKey:islandPicked];
    }
    listOfTempArray = [[NSMutableArray alloc] init]; // array no - 1
    itemOfMainArray = [[NSMutableArray alloc] init];
    [itemOfMainArray addObjectsFromArray:islandHotels];
    
    ndxCount = 0;
    arrayCountSub = 0;
    
    // adds an IMageproperty to an array to eventually display on the table.
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    
    [photoObjectQuery whereKey:@"tempLocation" equalTo:islandPicked];
    [photoObjectQuery whereKey:@"active" equalTo:@1];
    [photoObjectQuery orderByDescending:@"createdAt"];
    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                ImageProperties *property = [[ImageProperties alloc] init];
                property.id = [object objectId];
                property.instagramPic = NO;
                //NSLog(@"found a match!");
                
                [propertiesArray addObject:property];
                
            }
            [self getHotspots];
            hashtagWord = [islandHotels objectAtIndex:ndxCount];
//            [self getInstagramFeed];
//            [self getTwitterFeed];
//            [self getYoutubeFeed];
            
        }
        else {
            hashtagWord = [islandHotels objectAtIndex:ndxCount];
//            [self getInstagramFeed];
//            [self getTwitterFeed];
//            [self getYoutubeFeed];
            
        }
    }];
    
    [self.hotspotTypeSegmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self refreshTableView];
    
}

- (void) refreshTableView{
    defaultWord = YES;
    locationArray = nil;
    endSearch = NO;
    propertiesArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableOrderedSet alloc]init];
    if(islandPicked) {
        RootNavigationController * rootNVC = (RootNavigationController *)self.navigationController;
        NSDictionary *hotelNames = rootNVC.hotelNames;
        
        self.title = [hotelNames objectForKey:islandPicked];    }
    listOfTempArray = [[NSMutableArray alloc] init]; // array no - 1
    itemOfMainArray = [[NSMutableArray alloc] init];
    [itemOfMainArray addObjectsFromArray:islandHotels];
    
    ndxCount = 0;
    arrayCountSub = 0;
    
    [self getHotspots];

    
//    // adds an IMageproperty to an array to eventually display on the table.
//    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
//    
//    [photoObjectQuery whereKey:@"tempLocation" equalTo:islandPicked];
//    [photoObjectQuery whereKey:@"active" equalTo:@1];
//    [photoObjectQuery orderByDescending:@"createdAt"];
//    [photoObjectQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        if (!error) {
//            for (PFObject *object in objects) {
//                ImageProperties *property = [[ImageProperties alloc] init];
//                property.id = [object objectId];
//                property.instagramPic = NO;
//                //NSLog(@"found a match!");
//                
//                [propertiesArray addObject:property];
//                
//            }
//            [self getHotspots];
//        }
//        else {
//            [self getHotspots];
//        }
//        [self.rc endRefreshing];
//    }];

}


- (void) segmentedControlValueChanged: (UISegmentedControl *) sender{
    switch (sender.selectedSegmentIndex) {
        case 0:
            [propertiesArray removeAllObjects];
            [self getHotspots];
            [self.realTableView reloadData];
            break;
        case 1:
            [propertiesArray removeAllObjects];
            [self getHotspotsOfType:@"attraction"];
            [self.realTableView reloadData];
            break;
        case 2:
            [propertiesArray removeAllObjects];
            [self getHotspotsOfType:@"restaurant"];
            [self.realTableView reloadData];
            break;

        default:
            break;
    }
    
}


- (void) getHotspots{
    [self getHotspotsOfType:@"restaurant"];
    [self getHotspotsOfType:@"attraction"];
}

- (void) getHotspotsOfType: (NSString *) hotspotType{
   
    if(HUD)
    {
        [HUD hide:YES];
        [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
        
    }
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.delegate = self;
    [HUD show:YES];
    
    NSString* encodedStrin = [islandPicked stringByAddingPercentEscapesUsingEncoding:
                              NSUTF8StringEncoding];
    
    NSString* searchCall = [NSString stringWithFormat: @"https://maps.googleapis.com/maps/api/place/textsearch/json?query=%@+near+%@&key=%@", hotspotType, encodedStrin, GOOGLEAPIKEY];
    NSLog(@"search Call is: %@", searchCall);
    
    [JSONHTTPClient getJSONFromURLWithString: searchCall
                                  completion:^(NSDictionary *json, JSONModelError *err) {
                                      
                                      //got JSON back
                                      NSLog(@"Got JSON from web: %@", json);
                                      //parse result and save it to the results array
                                      
                                      //NSLog(@"error is: %@", err.localizedDescription);
                                      
                                      NSArray * results = json[@"results"];
                                      
                                      ImageProperties *properties;
                                      tempArray = [[NSMutableArray alloc]init];
                                      for (int i=0;i<results.count;i++) {
                                          
                                          //get the data
                                          NSDictionary* placeDetail = results[i];
                                          //create a property
                                          properties = [[ImageProperties alloc] init];
                                          
                                          //sample photo url: https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CmRdAAAAJqHDZ4JZfngouJlv9N8rpR28yi2-8dl8HNThPToZ_pU1wdoRFayI-qVv4Vj-zBTiPDkbBQXOJFrBCpba3rRZZIXGqhzlcoJf8r8Te24F6rVue2cta0AS3h_a_lFtmKd1EhC2lBBXUDAHHV0fV8PbH0eSGhTT-4YB8kvhCNYzRlmxYkm5pkJ9Cg&key=AIzaSyCCyH6djxbYbIziN1w8M0-NHlEXiN6sl5g
                                          
                                          NSString * photoReferenceID = [placeDetail[@"photos"] objectAtIndex:0][@"photo_reference"];
                                          
                                          if(photoReferenceID){
                                              properties.url = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=300&photoreference=%@&key=%@", photoReferenceID, GOOGLEAPIKEY]];
                                          } else {
                                              properties.url = [NSURL URLWithString: placeDetail[@"icon"]];
                                          }
                                          
                                          
                                          properties.googleSearchResults = placeDetail;
                                          properties.instagramPic = YES;
                                          properties.caption = [NSString stringWithFormat: @"#%@",islandPicked];
                                          properties.user = @"hospots";
                                          properties.hotspottype = hotspotType;
                                          [tempArray addObject:properties];
                                          
                                      }
                                      NSLog(@"photo number:%lu",(unsigned long)[tempArray count]);
                                      [propertiesArray addObjectsFromArray:tempArray];
                                      [self.realTableView reloadData];
                                      
                                      [HUD hide:YES];
                                      [HUD performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.0];
                                      
                                  }];
     
    
    
}


- (IBAction)unwindToVC1:(UIStoryboardSegue*)sender
{
    
    NSLog(@"UNWINDTOVC1");
    propertiesArray = nil;
    [self viewDidLoad];
}

- (void)somethingToAvoidNestedPop {
    [self performSegueWithIdentifier:@"ToProfile" sender:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == realTableView) {
        
        
    }
    else {
        //theSearchBar.text = [listOfTempArray objectAtIndex:indexPath.row];
        //[self searchBarSearchButtonClicked:theSearchBar];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}


// sets it to have 4 images per section of the table instead of just 1
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == realTableView) {
        if (defaultWord == NO) {
            return [propertiesArray count]/4 +1;
        }
        else if (ndxCount == ([islandHotels count] - 1)) {
            return [propertiesArray count]/4 + 1;
        }
        else {
            return [propertiesArray count]/4;
        }
    }
    else {
        return [listOfTempArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:realTableView]) {
        static NSString *CellIdentifier = @"ImageViewCell";
        ImageViewCell4 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[ImageViewCell4 alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:   CellIdentifier];
        }
        if (indexPath.row == [propertiesArray count]/4 - 1) {
            ndxCount ++;
            if (ndxCount < [islandHotels count] && defaultWord == YES) {
                //[self getInstagramFeed];
                //[self getTwitterFeed];
                //[self getYoutubeFeed];
            }
            else {
                NSLog(@"hits this last one ");
                endSearch = YES;
            }
        }
        
        
        if (indexPath.row == [propertiesArray count]/4) {
            cell = [[ImageViewCell4 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hello"];
            cell.imageView.hidden = YES;
            if ([propertiesArray count] >0) {
                if (endSearch == YES) {
                    cell.textLabel.text = @"                      End of Search";
                    [cell setUserInteractionEnabled:NO];
                }
                
            }
        }
        else {
            cell.textLabel.hidden = TRUE;
            
            // math to have 4 images per row.
            ImageProperties *cellProperties1;
            ImageProperties *cellProperties2;
            ImageProperties *cellProperties3;
            ImageProperties *cellProperties4;
            cellProperties1 = ((ImageProperties * )propertiesArray[indexPath.row*4]);
            cellProperties2 = ((ImageProperties * )propertiesArray[indexPath.row*4+1]);
            cellProperties3 = ((ImageProperties * )propertiesArray[indexPath.row*4+2]);
            cellProperties4 = ((ImageProperties * )propertiesArray[indexPath.row*4+3]);
            
            
            
            if (cellProperties1.image) {
                cell.pictureView1.image = cellProperties1.image;
                if ([cellProperties1.user isEqualToString:@"blank"]) {
                    cell.button1.enabled = NO;
                    
                }
                
            }
            
            // this checks if each image id downloaded or not
            else {
                if (![cellProperties1.user isEqualToString:@"blank"]) {
                    
                    
                    cell.pictureView1.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties1.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties1.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                if(cellProperties1.isVideo || cellProperties1.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView1.image=newimage;
                                    cellProperties1.image=newimage;
                                }else{
                                    cell.pictureView1.image = image;
                                    cellProperties1.image = image;
                                }
                            }
                            
                        }];
                        
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties1) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView1.image=newimage;
                                    cellProperties1.image=newimage;
                                }else{
                                    cell.pictureView1.image = image.image;
                                    cellProperties1.image = image.image;
                                }
                                
                                cellProperties1.user = image.user;
                                cellProperties1.caption = image.caption;
                                cellProperties1.videoData=image.videoData;
                                cellProperties1.isUserVideo=image.isUserVideo;
                                //NSLog(@"Regurgitate %@, %@", cellProperties1.user, cellProperties1.caption);
                            }
                        }];
                    }
                }
                
            }
            if (cellProperties2.image) {
                cell.pictureView2.image = cellProperties2.image;
                
                
                if ([cellProperties2.user isEqualToString:@"blank"]) {
                    cell.button2.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties2.user isEqualToString:@"blank"]) {
                    
                    cell.pictureView2.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties2.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties2.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                if(cellProperties2.isVideo || cellProperties2.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView2.image=newimage;
                                    cellProperties2.image=newimage;
                                }else{
                                    cell.pictureView2.image = image;
                                    cellProperties2.image = image;
                                }
                            }
                            
                        }];
                        
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties2) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                //NSLog(@"lets do it");
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView2.image=newimage;
                                    cellProperties2.image=newimage;
                                }else{
                                    cell.pictureView2.image = image.image;
                                    cellProperties2.image = image.image;
                                }
                                cellProperties2.user = image.user;
                                cellProperties2.caption = image.caption;
                                cellProperties2.videoData=image.videoData;
                                cellProperties2.isUserVideo=image.isUserVideo;
                                //NSLog(@"Regurgitate %@, %@", cellProperties2.user, cellProperties2.caption);
                            }
                        }];
                    }
                }
            }
            if (cellProperties3.image) {
                cell.pictureView3.image = cellProperties3.image;
                if ([cellProperties3.user isEqualToString:@"blank"]) {
                    cell.button3.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties3.user isEqualToString:@"blank"]) {
                    
                    cell.pictureView3.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties3.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties3.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                if(cellProperties3.isVideo || cellProperties3.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView3.image=newimage;
                                    cellProperties3.image=newimage;
                                }else{
                                    cell.pictureView3.image = image;
                                    cellProperties3.image = image;
                                }
                            }
                            
                            
                        }];
                        
                        
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties3) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView3.image=newimage;
                                    cellProperties3.image=newimage;
                                }else{
                                    cell.pictureView3.image = image.image;
                                    cellProperties3.image = image.image;
                                }
                                
                                
                                cellProperties3.user = image.user;
                                cellProperties3.caption = image.caption;
                                cellProperties3.videoData=image.videoData;
                                cellProperties3.isUserVideo=image.isUserVideo;
                            }
                        }];
                    }
                }
            }
            if (cellProperties4.image) {
                cell.pictureView4.image = cellProperties4.image;
                if ([cellProperties4.user isEqualToString:@"blank"]) {
                    cell.button4.enabled = NO;
                    
                }
            }
            else {
                if (![cellProperties4.user isEqualToString:@"blank"]) {
                    
                    cell.pictureView4.image = [UIImage imageNamed:@"JaagoDownloadingDefault.png"];
                    if (cellProperties4.instagramPic == YES) {
                        [self downloadImageWithURL:cellProperties4.url completionBlock:^(BOOL succeeded, UIImage *image) {
                            if (succeeded) {
                                
                                if(cellProperties4.isVideo || cellProperties4.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image];
                                    cell.pictureView4.image=newimage;
                                    cellProperties4.image=newimage;
                                }else{
                                    cell.pictureView4.image = image;
                                    cellProperties4.image = image;
                                }
                            }
                            
                        }];
                    }
                    else {
                        [self downloadImageWithObject:(&cellProperties4) completionBlock:^(BOOL succeeded, ImageProperties *image) {
                            if (succeeded) {
                                if(image.isUserVideo){
                                    UIImage* newimage=[self overlayPlayerIcon:image.image];
                                    cell.pictureView4.image=newimage;
                                    cellProperties4.image=newimage;
                                }else{
                                    cell.pictureView4.image = image.image;
                                    cellProperties4.image = image.image;
                                }
                                
                                
                                cellProperties4.user = image.user;
                                cellProperties4.caption = image.caption;
                                cellProperties4.videoData = image.videoData;
                                cellProperties4.isUserVideo=image.isUserVideo;
                                
                                //NSLog(@"Regurgitate %@, %@", cellProperties4.user, cellProperties4.caption);
                            }
                        }];
                    }
                }
                
            }
        }
        
        
        return cell;
        
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text = [listOfTempArray objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
}

//code for displaying the player icon on the image
- (UIImage *) overlayPlayerIcon: (UIImage *) image{
    //NSLog(@"doing the overlay...");
    UIImage *backgroundImage = image;
    UIImage *playermarkImage = [UIImage imageNamed:@"PlayButtonImage2.png"];
    
    UIGraphicsBeginImageContext(backgroundImage.size);
    [backgroundImage drawInRect:CGRectMake(0, 0,500,400)];
    [playermarkImage drawInRect:CGRectMake(0,40 , 500,300)];
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

- (IBAction)logoutButtonPressed:(id)sender
{
    [PFUser logOut];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    //[self viewDidAppear:NO];
}

- (IBAction)cameraButtonPressed:(id)sender
{
    
}

- (IBAction)profileButtonPressed:(id)sender {
    
}


- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (void) downloadImageWithObject:(ImageProperties **)id completionBlock:(void (^)(BOOL succeeded, ImageProperties *image)) completionBlock
{
    //NSLog(@"%@",(*id).id);
    PFQuery *photoObjectQuery = [PFQuery queryWithClassName:@"Photo"];
    [photoObjectQuery includeKey:@"creator"];
    [photoObjectQuery getObjectInBackgroundWithId:(*id).id block:^(PFObject *object, NSError *error) {
        if (!error) {
            ImageProperties *imageSpecs = [[ImageProperties alloc] init];
            imageSpecs.user = object[@"creator"][@"username"];
            imageSpecs.caption = object[@"caption"];
            imageSpecs.imageFile = object[@"image"];
            if([object.allKeys containsObject:@"video"]){
                imageSpecs.videoFile = object[@"video"];
                
            }
            
            [object[@"video"] getDataInBackgroundWithBlock:^(NSData *videoData,NSError *error) {
                if(!error){
                    NSLog(@"seccessfully get video: data length=%lu",(unsigned long)videoData.length);
                    imageSpecs.videoData=videoData;
                    if(videoData.length>0){
                        imageSpecs.isUserVideo=YES;
                        imageSpecs.caption=[imageSpecs.caption stringByAppendingString:@" (This is a user video, tap on the video to play it.)"];
                    }
                }else{
                    NSLog(@"ERROR downloading video");
                }
            }];
            [object[@"image"] getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    NSLog(@"successfully get image: data length=%lu",(unsigned long)imageData.length);
                    imageSpecs.image = [UIImage imageWithData:imageData];
                    
                    completionBlock (YES,imageSpecs);
                }
                else {
                    NSLog(@"Error downloading image");
                    completionBlock (NO,nil);
                }
            }];
        }
        else {
            NSLog(@"Error first query");
            completionBlock (NO,nil);
        }
    }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    
    if([title isEqualToString:@"GO"]) {
    }
    
    if([title isEqualToString:@"Okay"])
    {
        NSLog(@"Posted Photo was selected.");
        
    }
}
- (IBAction)homePressed:(id)sender {
    LandingViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LandingPage"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)islandsPressed:(id)sender {
    IslandController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Islands"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)cameraPressed:(id)sender {
    ChooseWhereViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"CameraView"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)profilePressed:(id)sender {
    ProfileController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:landing animated:NO];
}

- (IBAction)settingPressed:(id)sender {
    SettingsViewController *landing = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    [self.navigationController pushViewController:landing animated:NO];
    
}


// segues based on what the user pressed
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LandingPageResultSegue"]) {
        LandingPageResultViewController *controller = [segue destinationViewController];
        NSInteger path = realTableView.indexPathForSelectedRow.row;
        controller.selectedImageProperties = [propertiesArray objectAtIndex:path];
        //NSLog(@"tableuser :%@",controller.tableUser);
        
    }
    if ([segue.identifier isEqualToString:@"button1"]) {
        HotspotResultsViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4;
        controller.property = [propertiesArray objectAtIndex:path];
        controller.HotelName = islandPicked;
        
        
    }
    if ([segue.identifier isEqualToString:@"button2"]) {
        HotspotResultsViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 1;
        controller.property = [propertiesArray objectAtIndex:path];
        controller.HotelName = islandPicked;

        
    }
    if ([segue.identifier isEqualToString:@"button3"]) {
        HotspotResultsViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 2;
        controller.property = [propertiesArray objectAtIndex:path];
        controller.HotelName = islandPicked;

        
        
    }
    if ([segue.identifier isEqualToString:@"button4"]) {
        HotspotResultsViewController *controller = [segue destinationViewController];
        CGPoint hitPoint = [sender convertPoint:CGPointZero toView:self.realTableView];
        NSIndexPath *hitIndex = [self.realTableView indexPathForRowAtPoint:hitPoint];
        NSInteger path = hitIndex.row *4 + 3;
        controller.property = [propertiesArray objectAtIndex:path];
        controller.HotelName = islandPicked;

        
        
    }
    
}

@end
